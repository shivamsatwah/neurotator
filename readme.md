# Neurotator
A hyperdimensional cellular image visualizer and annotation editor.

# INSTALL

If you just want to run the application without rebuilding, there should be a prebuilt installer available for Windows.

## Building

This is all cross-platform, but so far it's only been built and run on Windows using MSVC (Visual Studio) build configuration. Everything should build fine as well using gcc / MinGW. For other OSes you'd have to acquire the appropriate Matlab API library files.

### Windows

- Install Visual Studio

- [optional] If you want to use the debugger, install Windows SDK for your OS (https://developer.microsoft.com/en-us/windows/downloads/sdk-archive)

- Check PATH env variable

- [optional] Install MSYS if you want to use gcc compiler on Windows instead of VS

- Install Qt Creator
	- Run settings, might have to prepend to "Path" the local location of Matlab installation, e.g. on windows it might something like "C:\Program Files\MATLAB\R2018a\bin\win64".

- Install dependencies (see below).

### NOTES

If changing the .pro file, you should re-run qmake (from Qt Creator: Build->Run QMake) to validate changes.

## Creating the installer (after building)

### Windows

	In a command prompt:
	
	Change dir to Qt location, and then in the target configuration, e.g. msvc2017_64, run the windeployqt utility. Replace [destination directory] with the path to an empty folder for it to copy all the necessary binary files to.
	
	C:\Qt\5.11.2\msvc2017_64\bin>windeployqt.exe --release --qmldir "[path to source code]\neurotator\gui\qml"  "[destination directory]"

	Then copy the remaining third-party DLLs from the neurotator source tree and the MSVC distributable dlls.	

# DEPENDENCIES

## Lib Tiff

Example of building for MSVC 2017 64-bit:

cd to `C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools`
(or wherever your local install of MSVC is)

`vsdevcmd -arch=x64`

cd to libtiff directory

To make debug versions, use:
`nmake /f makefile.vc DEBUG=1`

Note that will create library files with the exact same name so you'll have to copy them or rename them. I left them in the same dir and added a "d" to the filenames, e.g. libtiffd.dll.
Release build:
```
	nmake /f Makefile.vc
    cd tools
    nmake /f Makefile.vc
```

## G3Log
Make sure to install a recent version of CMake and add it to the PATH env variable.
These notes are for Visual Studio 2017 (adjust accordingly for different versions.)

As with Lib Tiff, enter the MSVC command prompt, then from the g3log folder:
```
	mkdir build
	cd build
	cmake -G "Visual Studio 15 2017 Win64" -DUSE_G3LOG_UNIT_TEST=ON -DCMAKE_BUILD_TYPE=Release -DDCHANGE_G3LOG_DEBUG_TO_DBUG=ON..
	msbuild g3log.sln /p:Configuration=Release
```

Copy those (.dll, .lib) to the build parent directory.

Repeat for debug mode:
```
	cmake -G "Visual Studio 15 2017 Win64" -DUSE_G3LOG_UNIT_TEST=ON -DCMAKE_BUILD_TYPE=Debug -DDCHANGE_G3LOG_DEBUG_TO_DBUG=ON..
	msbuild g3log.sln /p:Configuration=Debug
```
Rename to have "d" in the name, and then copy those (.dll, .lib, and .pdb) to the build parent directory.

## Mat files

If you want to be able to load .mat files, you'll need Matlab's C API, which is made up of some DLLS and associated headers (and if using MSVC also import libraries). If running from Qt Creator, the Run environment should use Build Environment with the change to prepend to Path "C:\Program Files\MATLAB\R2018a\bin\win64" (or wherever you installed Matlab). 

## OpenCV

--SHK