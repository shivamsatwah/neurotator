/// @author Sam Kenyon <sam@metadevo.com>
#include <string>
#include <QFileInfo>
#include <QDebug>

#include <g3log/g3log.hpp>
#include <tiffio.h>

#include "HyperstackTiff.hpp"

HyperstackTiff::HyperstackTiff()
{
    //m_index.stackId = stackId;
}

HyperstackTiff::~HyperstackTiff()
{
    cleanup();
}

void HyperstackTiff::cleanup()
{
    LOG(DBUG) << "Cleaning up...";
    try
    {
        TIFFClose(m_tif);
        m_tif = nullptr;
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

/// Function for passing in to QImage so we can safely release buffer
/// memory after all the QImage refs are destroyed.
void HyperstackTiff::cleanupBufferHandler(void *info)
{
    try
    {
        uint32* buffer = reinterpret_cast<uint32*>(info);
        if (buffer) {
            //LOG(DBUG) << "Destroying buffer...";
            _TIFFfree(buffer);
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

///@todo is only operating on z, should also nav in t
void HyperstackTiff::loadSlice(const HyperstackIndex index)
{
    if (!m_tif) {
        LOG(DBUG) << "loadSlice: ERROR: TIFF object is null.";
    }
    unsigned int newZ = index.z;
    // each Z slice takes up c directories, one for each channel
    //uint16 currDir = TIFFCurrentDirectory(m_tif);
    //LOG(DBUG) << "currDir:"<< currDir;
    if (newZ >= m_size.z ) {
        return;
    }

    uint16 newDir = static_cast<uint16>(index.z * m_size.c);
    //LOG(DBUG) << "newDir:"<< newDir;

    m_ready = false;
    m_index.z = newZ;
    TIFFSetDirectory(m_tif, newDir);
    loadRasterBuffer(m_tif);  // load all color channels for slice
    m_ready = true;
}

void HyperstackTiff::loadNextZ()
{
    if (!m_tif) {
        LOG(DBUG) << "loadSlice: ERROR: TIFF object is null.";
    }
    unsigned int newZ = m_index.z + 1;
    if (newZ >= m_size.z ) {
        return;
    }
    m_ready = false;
    m_index.z++;
    loadRasterBuffer(m_tif);  // load all color channels for slice
    m_ready = true;
}

void HyperstackTiff::loadPrevZ()
{
    if (!m_tif) {
        LOG(DBUG) << "loadSlice: ERROR: TIFF object is null.";
    }
    int newZ = static_cast<int>(m_index.z) - 1;
    if (newZ < 0 ) {
        return;
    }
    // each Z slice takes up c directories, one for each channel
    uint16 currDir = TIFFCurrentDirectory(m_tif);
    //LOG(DBUG) << "currDir:"<< currDir;
    if (currDir == 0) {
        return;
    }
    uint16 newDir = currDir - m_size.c * 2;
    if (TIFFLastDirectory(m_tif)) {
        newDir++; // correct off-by-one due to stopping at last dir of the file
    }
    //LOG(DBUG) << "newDir:"<< newDir;

    m_ready = false;
    m_index.z--;
    TIFFSetDirectory(m_tif, newDir);
    loadRasterBuffer(m_tif);  // load all color channels for slice
    m_ready = true;
}

unsigned int HyperstackTiff::countPages(TIFF* tif)
{
    unsigned int dirCount = 0;
    if (tif) {
        do {
            dirCount++;
        } while (TIFFReadDirectory(tif));
    } else {
        LOG(WARNING) << "TIFF object is null!";
    }
    TIFFSetDirectory(tif, 0);
    return dirCount;
}

/// Load metadata from tags
bool HyperstackTiff::loadMetadata(TIFF* tif)
{
    if (!tif) {
        LOG(WARNING) << "TIFF object is null!";
        return false;
    }

    unsigned int width, height;
    uint16 bps, spp, photometric, compression;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bps);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &spp);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    TIFFGetField(tif, TIFFTAG_COMPRESSION, &compression);

    m_size.x = width;
    m_size.y = height;
    m_size.slicePixels = width * height;

    LOG(DBUG) << "TIFF width: " << width;
    LOG(DBUG) << "TIFF height: " << height;
    LOG(DBUG) << "TIFF bps: " << bps;
    LOG(DBUG) << "TIFF spp: " << spp;
    LOG(DBUG) << "TIFF photometric: " << photometric;
    LOG(DBUG) << "TIFF compression: " << compression;

    ImageJMetadata metadata = readImageJMetadata(tif);
    if (metadata.imageJ)
    {
        m_numImages = metadata.images;
        m_size.c = metadata.channels;
        m_size.z = metadata.slices;
        m_size.t = metadata.frames;
    }
    else
    {
        qWarning() << "Unknown TIFF format.";
        LOG(DBUG) << "WARNING: Unknown TIFF format.";
    }
    LOG(DBUG) << "numImages:" << m_numImages;
    LOG(DBUG) << "m_size.c:" << m_size.c;
    LOG(DBUG) << "m_size.z:" << m_size.z;
    LOG(DBUG) << "m_size.t:" << m_size.t;
    return true;
}

/// Reads ImageJ custom information (if there is any) out of the TIFF file
ImageJMetadata HyperstackTiff::readImageJMetadata(TIFF* tif)
{
    ImageJMetadata data;

    char* strData;
    int valid = TIFFGetField(tif, TIFFTAG_IMAGEDESCRIPTION, &strData);
    if (valid != 1) {
        LOG(INFO) << "No Image Description tag found.";
        return data;
    }
    QString desc(strData);
    //qDebug() << "TIFFTAG_IMAGEDESCRIPTION: " << desc;

    if (desc.startsWith("ImageJ"))
    {
        data.imageJ = true;
        LOG(DBUG) << "TIFF is ImageJ format.";
        QStringList metatags = desc.split("\n", QString::SkipEmptyParts);

        data.version = getMetadataStr("ImageJ", metatags);
        LOG(DBUG) << "data.version:" << data.version.toStdString();
        data.hyperstack = "true" == getMetadataStr("hyperstack", metatags);
        LOG(DBUG) << "data.hyperstack:" << data.hyperstack;
        data.modeComposite = "composite" == getMetadataStr("mode", metatags);
        LOG(DBUG) << "data.modeComposite:" << data.modeComposite;

        data.images = static_cast<unsigned int>(getMetadataInt("images", metatags));
        data.slices = static_cast<unsigned int>(getMetadataInt("slices", metatags));
        data.frames = static_cast<unsigned int>(getMetadataInt("frames", metatags));
        data.channels = static_cast<unsigned int>(getMetadataInt("channels", metatags));

        LOG(DBUG) << "images:" << data.images;
        LOG(DBUG) << "slices:" << data.slices;
        LOG(DBUG) << "frames:" << data.frames;
        LOG(DBUG) << "channels:" << data.channels;
    }
    return data;
}

/// Extracts an integer out of a string tokenized on an equals symbol, e.g. "foo=42"
int HyperstackTiff::getMetadataInt(QString name, QStringList metatags)
{
    int value = 0;
    QStringList matches = metatags.filter(name);
    if (matches.size() > 0)
    {
        QString chunk = matches[0];
        QStringList tokens = chunk.split('=', QString::SkipEmptyParts);

        if (tokens.size() == 2) {
            bool ok = false;
            value = tokens[1].toInt(&ok);
            if (!ok) {
                LOG(DBUG) << "ERROR: Metadata value not an integer (string: " << tokens[1].toStdString() << ")";
                value = 1;
            }
        }
    }
    return value;
}

/// Extracts a string out of a string tokenized on an equals symbol
QString HyperstackTiff::getMetadataStr(QString name, QStringList metatags)
{
    QString value;
    QStringList matches = metatags.filter(name);
    if (matches.size() > 0)
    {
        QString chunk = matches[0];
        QStringList tokens = chunk.split('=', QString::SkipEmptyParts);
        if (tokens.size() == 2) {
            value = tokens[1];
        }
    }
    return value;
}

/**
 * @brief HyperstackTiff::isolateChannel
 * @param buffer: Image buffer to be modified. All channels except one (and alpha) will be zeroed.
 * @param channel: Which channel to leave. Currently this ignores the alpha channel and leaves it as is.
 * @param width
 * @param height
 */
void HyperstackTiff::isolateChannelRGB(uint32* buffer, const RGBA_Channel channel, const unsigned int width, const unsigned int height)
{
    if (channel == RGBA_Channel::ALPHA) {
        return;
    }

    int i = 0;
    for (unsigned int y = 0; y < height; ++y) {
        for (unsigned int x = 0; x < width; ++x) {
            for (unsigned int c = 0; c < 3; ++c) {
                if (c == channel) {
                    buffer[i] = buffer[i];
                } else {
                    buffer[i] = 0;
                }
                ++i;
            }
        }
    }
}

/// merges three buffers as R, G, B into the first (the Red) buffer in ARGB order
uchar HyperstackTiff::mergeBuffersRGB(uint32** buffers, const unsigned int pixelSize)
{
    uchar maxColorValue = 0;
    int a = 0xFF;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        int r = TIFFGetR(buffers[0][p]);
        int g = TIFFGetG(buffers[1][p]);
        int b = TIFFGetB(buffers[2][p]);
        buffers[0][p] = qRgba(r, g, b, a);
        if (r > maxColorValue) {
            maxColorValue = r;
        }
        if (g > maxColorValue) {
            maxColorValue = g;
        }
        if (b > maxColorValue) {
            maxColorValue = b;
        }
    }
    return maxColorValue;
}

/// merges three buffers as R, G, B into the first (the Red) buffer in ARGB order
void HyperstackTiff::mergeBuffersRGBQuick(uint32** buffers, const unsigned int pixelSize)
{
    int r, g, b, a = 0xFF;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        r = TIFFGetR(buffers[0][p]);
        g = TIFFGetG(buffers[1][p]);
        b = TIFFGetB(buffers[2][p]);
        buffers[0][p] = qRgba(r, g, b, a);
    }
}

/// merges four buffers as R, G, B, A into the first (the Red) buffer in ARGB order
uchar HyperstackTiff::mergeBuffersRGBA(uint32** buffers, unsigned int pixelSize)
{
    uchar maxColorValue = 0;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        int r = TIFFGetR(buffers[0][p]);
        int g = TIFFGetG(buffers[1][p]);
        int b = TIFFGetB(buffers[2][p]);
        int a = TIFFGetA(buffers[3][p]);

        buffers[0][p] = qRgba(r, g, b, a);
        if (r > maxColorValue) {
            maxColorValue = r;
        }
        if (g > maxColorValue) {
            maxColorValue = g;
        }
        if (b > maxColorValue) {
            maxColorValue = b;
        }
    }
    return maxColorValue;
}

/// Loads all channels (if they map to RGBA) for a single slice
bool HyperstackTiff::loadRasterBuffer(TIFF* tif)
{
    if (!tif) {
        LOG(DBUG) << "ERROR: TIFF object is null.";
        return false;
    }
    try
    {
        bool failed = false;
        unsigned int channels = m_size.c;
        if (channels > 4) {
            channels = 4;
        }
        uint32* buffers[4] = {nullptr, nullptr, nullptr, nullptr};
        unsigned int byteSize = m_size.slicePixels * sizeof(uint32);
        for (unsigned int c = 0; c < channels; ++c)
        {
            buffers[c] = static_cast<uint32*>(_TIFFmalloc(byteSize));
            if (!buffers[c]) {
                LOG(DBUG) << "Error: TIFF image buffer memory allocation failed.";
                failed = true;
            }

            if (!TIFFReadRGBAImageOriented(tif, m_size.x, m_size.y, buffers[c], ORIENTATION_TOPLEFT, 0)) {
                LOG(DBUG) << "Error: TIFF read image failed.";
                failed = true;
            }

            if (!TIFFReadDirectory(m_tif))  // advance to next dir in this TIF file and read it
            {
                if (c != channels - 1) {
                    LOG(DBUG) << "ERROR: HyperstackTiff: Unexpected end of pages.";
                    break;
                }
            }
        }        

        if (failed) {
            for (unsigned int c = 0; c < channels; ++c) {
                _TIFFfree(buffers[c]);
            }
            return false;
        }

        if (m_initted && channels == 3) {
            mergeBuffersRGBQuick(buffers, m_size.slicePixels);
        } else {
            if (channels == 3) {
                // combine all channel buffers into one RGBA32 buffer
                uchar maxColor = mergeBuffersRGB(buffers, m_size.slicePixels);
                if (maxColor > m_maxColor) {
                    m_maxColor = maxColor;
                    m_maxColorValue = qRgba(maxColor, maxColor, maxColor, 0xFF);
                }
            } else {
                m_maxColorValue = qRgba(0xFF, 0xFF, 0xFF, 0xFF);
            }
        }

        uchar* byteBuffer = reinterpret_cast<uchar*>(buffers[0]);
        m_currImage = QImage(byteBuffer,
                             static_cast<int>(m_size.x),
                             static_cast<int>(m_size.y),
                             QImage::QImage::Format_ARGB32,
                             (QImageCleanupFunction) &HyperstackTiff::cleanupBufferHandler,
                             (void*) byteBuffer);

        // free memory from buffers we don't need anymore
        for (unsigned int c = 1; c < channels; ++c)
        {
            //LOG(DBUG) << "Destroying temp buffers.";
            _TIFFfree(buffers[c]);
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
    return true;
}

/**
 * @brief SliceStackTiff::load
 * @param filePath
 * @return true on success
 */
bool HyperstackTiff::load(const QString filePath)
{
    m_ready = false;
    QFileInfo fileInfo(filePath);
    if (!fileInfo.exists()) {
        LOG(DBUG) << "ERROR: TIFF file does not exist!";
        return false;
    }
    m_tif = TIFFOpen(filePath.toStdString().c_str(), "r");
    if (!m_tif)
    {
        LOG(DBUG) << "ERROR: TIFF object null!";
        return false;
    }
    m_filePath = filePath;
    m_name = fileInfo.baseName();
    LOG(INFO) << "Trying to load file " << m_name.toStdString();

    loadMetadata(m_tif);
    if (m_numImages == 0) {
        m_numImages = countPages(m_tif);
    }
    LOG(DBUG) << "Image count for \"" << fileInfo.fileName().toStdString() << "\":" << m_numImages;

    if (m_size.z == 0 && m_size.t == 0) {
        // assume this is a z stack unless user tells us otherwise
        m_size.z = m_numImages;
    }
    if (m_size.c == 0) {
        m_size.c = 1;
    }

    LOG(DBUG) << "m_size.z: " << m_size.z;
    if (m_size.z > 0) {
        // load a few images across the z dimension to get a better max color value
        HyperstackIndex index;
        index.z = m_size.z / 2;
        loadSlice(index);
        index.z = m_size.z - 1;
        loadSlice(index);
        index.z = 0;
        loadSlice(index);
    } else {
        LOG(INFO) << "Z size == 0";
        loadRasterBuffer(m_tif); // try loading first image
        m_index.z = 1;
    }
    m_initted = true;
    m_ready = true;
    // leave the tif file open until user closes it when the class is destroyed
    return true;
}

bool HyperstackTiff::loadCache()
{
    if (!m_tif)
    {
        LOG(DBUG) << "ERROR: TIFF object null!";
        return false;
    }

    unsigned int channel_count = 0;
    unsigned int color_channel = RGBA_Channel::RED;
    do
    {
        loadRasterBuffer(m_tif);
        //isolateChannelRGB(uint32* buffer, const RGBA_Channel channel, const unsigned int width, const unsigned int height);
        ///@todo need to keep track of cache size
        if (++channel_count > m_size.c) {  // if is faster than modulo
            channel_count = 0;
        }
        if (++color_channel > RGBA_Channel::BLUE) {  // if is faster than modulo
            color_channel = 0;
        }

    } while (TIFFReadDirectory(m_tif));
    return true;
}


/// @return QImage object. QImage uses implicit data sharing, so ok to pass by value.
QImage HyperstackTiff::imageBuffer()
{
    return m_currImage;
}
