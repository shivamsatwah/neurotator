/// @author Sam Kenyon <sam@metadevo.com>
#include <g3log/g3log.hpp>
#include <QDebug>
#include <QPainter>

#include "AppSettings.hpp"
#include "RoiData.hpp"
#include "SearchResults.hpp"

RoiData::RoiData()
{
    if (AppSettings::instance().find("roi_origin")) {
        QString origin = AppSettings::instance().get("roi_origin").toString();
        if (origin == "bottomleft") {
            m_originTopLeft = false;
        } else {
            m_originTopLeft = true;
        }
    } else {
        AppSettings::instance().set("roi_origin", "topleft");
    }
    LOG(INFO) << "roi_origin: " << m_originTopLeft;
}

RoiData::~RoiData()
{
    LOG(DBUG) << "dtor";
}

bool RoiData::addNewSession(session_key_t name)
{
    if (!m_sessions.contains(name)) {
        m_sessions[name];
        m_currSession = name;
    } else {
        LOG(WARNING) << "Session key ("<< name.toStdString() << ") already inserted.";
        return false;
    }
    return true;
}

bool RoiData::removeSession(session_key_t name)
{
    if (m_sessions.contains(name)) {
        m_sessions.remove(name);
        LOG(DBUG) << "Session \""<< name.toStdString() << "\" removed.";
    } else {
        LOG(WARNING) << "Session key ("<< name.toStdString() << ") does not exist.";
        return false;
    }
    return true;
}

bool RoiData::addNewRoi(session_key_t session, roi_key_t name, roi_t fields)
{
    m_currSession = session;
    return addNewRoi(name, fields);
}

/// Can call this directly if adding all the rois right after adding a new session
/// because the class keeps state of current session.
bool RoiData::addNewRoi(roi_key_t name, roi_t fields)
{
    if (m_currSession != "") {
        auto it = m_sessions.find(m_currSession);
        if(it != m_sessions.end()){
            refreshRoi(m_currSession, fields);
            if (it.value().contains(name)) {
                LOG(INFO) << "Duplicate ROI: " << name.toStdString();
            } else {
                it.value()[name] = fields;
            }
        } else {
            LOG(WARNING) << "Session key ("<< m_currSession.toStdString() << ") unknown.";
            return false;
        }
    } else {
        LOG(WARNING) << "Current session key is empty";
        return false;
    }
    return true;
}

/// This doesn't check for duplicate names, that should be done first
bool RoiData::renameRoi(session_key_t session, roi_key_t name, roi_key_t newName)
{
    if (m_sessions.contains(session)) {
        session_t& sess = m_sessions[session];
        if (sess.contains(name))
        {
            sess[newName] = sess[name];
            sess[name]->name = newName;
            sess.remove(name);
            return true;
        } else {
            LOG(WARNING) << "ROI key ("<< name.toStdString() << ") unknown.";
            return false;
        }
    } else {
        LOG(WARNING) << "Session key ("<< session.toStdString() << ") unknown.";
        return false;
    }
}

bool RoiData::deleteRoi(session_key_t session, roi_key_t roiKey)
{
    if (m_sessions.contains(session)) {
        session_t& sess = m_sessions[session];
        if (sess.contains(roiKey))
        {
            removeRoiFromMaskTables(session, sess[roiKey]);
            int removedCount = sess.remove(roiKey); // delete ROI object
            LOG(INFO) << "Removed: " << removedCount << " " << roiKey.toStdString() << " rois";

            refreshRefLinks();
            return true;
        } else {
            LOG(WARNING) << "ROI key ("<< roiKey.toStdString() << ") unknown.";
            return false;
        }
    } else {
        LOG(WARNING) << "Session key ("<< session.toStdString() << ") unknown.";
        return false;
    }
}

/// Deletes flagged ROIS for the specified session
unsigned int RoiData::deleteFlagged(const QString& sessionKey)
{
    int delCount = 0;
    if (m_sessions.contains(sessionKey)) {
        session_t& sess = m_sessions[sessionKey];
        for (auto roiKey : sess.keys())
        {
            if (sess[roiKey]->flagged) {
                removeRoiFromMaskTables(sessionKey, sess[roiKey]);
                delCount += sess.remove(roiKey);
            }
        }
    } else {
        LOG(WARNING) << "Session key ("<< sessionKey.toStdString() << ") unknown.";
    }
    if (delCount > 0) {
        refreshRefLinks();
    }
    return static_cast<unsigned int>(delCount);
}

/// Deletes all flagged ROIS (across all sessions)
unsigned int RoiData::deleteFlagged()
{
    unsigned int delCount = 0;
    for (auto sessionKey : m_sessions.keys()) {
        delCount += deleteFlagged(sessionKey);
    }
    return delCount;
}

void RoiData::setRefSession(const session_key_t& sessionKey)
{
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        m_refSession = sessionKey;
        LOG(INFO) << "Set reference session to: " << m_refSession.toStdString();
    } else {
        LOG(WARNING) << "Session key ("<< sessionKey.toStdString() << ") unknown.";
    }

}

void RoiData::clearRefSession()
{
    LOG(INFO) << "Clearing reference session.";

    if (m_refSession != "") {
        session_t& refSess = m_sessions[m_refSession];

        // clear all existing linked-in entries in ref session
        for (auto roi : refSess) {
            roi->linkedIn.clear();
            roi->confirmed = false;
        }
    }

    m_refSession = "";
}

///@return 1 if this was an update, 0 if this was a first-time link
int RoiData::modifyRoiLink(RoiAddress& src, RoiAddress& dest)
{
    int ret = 0;

    if (src.sessionKey == m_refSession)
    {
        // swap src and dest
        RoiAddress temp = src;
        src = dest;
        dest = temp;
    }
    if (dest.sessionKey != m_refSession) {
        LOG(WARNING) << "Attempt to link to a non-reference session.";
        ret = -1;
        return ret;
    }

    // this ROI had a link previously
    if (m_sessions[src.sessionKey][src.roiKey]->linkedName != "") {
        ret = 1;
    }

    // Remove existing link to this ref ROI from source session (if there are any)
    removeOldLink(dest.roiKey, src.sessionKey);

    // Modify source ROI to link to ref
    roi_t srcRoi = m_sessions[src.sessionKey][src.roiKey];
    srcRoi->linkedName = dest.roiKey;
    srcRoi->linkedSession = dest.sessionKey;
    srcRoi->confirmed = true;

    // Update ref's linked-in information
    m_sessions[dest.sessionKey][dest.roiKey]->linkedIn[src.sessionKey] = srcRoi;   
    m_sessions[dest.sessionKey][dest.roiKey]->confirmed = true;
    refreshFullyLinked(m_sessions[dest.sessionKey][dest.roiKey]);

    LOG(INFO) << "Link info updated for " << src.sessionKey.toStdString() << " : " << src.roiKey.toStdString()
              << " -> " << m_sessions[src.sessionKey][src.roiKey]->linkedSession.toStdString() << " : " << m_sessions[src.sessionKey][src.roiKey]->linkedName.toStdString();
    return ret;
}

/// This will clear the previous filter results
unsigned int RoiData::filterHighPassRadius(const QString& sessionKey, const int minRadius)
{
    unsigned int filteredOutCount = 0;
    const int minDiameter = minRadius * 2;
    if (m_sessions.contains(sessionKey)) {
        for (auto roi : m_sessions.value(sessionKey))
        {
            roi->filtered = false;
            // first find the largest slice in the mask volume
            int maxDiameter = 0;
            int tempDiameter = 0;
            for (auto mask : roi->masks) {
                tempDiameter = maxDim(mask.extent);
                if (tempDiameter > maxDiameter) {
                    maxDiameter = tempDiameter;
                }
            }
            // is largest slice below the threshold?
            if (maxDiameter < minDiameter) {
                roi->filtered = true;
                ++filteredOutCount;
            }
        }
    }
    return filteredOutCount;
}

unsigned int RoiData::filterHighPassVolume(const QString& sessionKey, const int minVolume)
{
    unsigned int filteredOutCount = 0;

    ///@todo

    return filteredOutCount;
}

/// This does not clear existing flags
unsigned int RoiData::flagFiltered(const QString& sessionKey)
{
    unsigned int flaggedCount = 0;
    if (m_sessions.contains(sessionKey)) {
        for (auto roi : m_sessions.value(sessionKey))
        {
            if (roi->filtered) {
                roi->flagged = true;
                ++flaggedCount;
            }
        }
    }
    return flaggedCount;
}

/// Clears the flagged field for all ROIs in the given session
unsigned int RoiData::unflagAll(const QString& sessionKey)
{
    for (auto roi : m_sessions.value(sessionKey))
    {
        roi->flagged = false;
    }
    return static_cast<unsigned int>(m_sessions.value(sessionKey).size());
}

/// Sets/clears the flagged field for the given ROI
void RoiData::setFlag(const QString& sessionKey, const QString& roiKey, bool flag)
{
    m_sessions.value(sessionKey).value(roiKey)->flagged = flag;
}

/// Sets/clears the flagged field for the given ROI
bool RoiData::toggleFlag(const QString& sessionKey, const QString& roiKey)
{
    bool flag = m_sessions[sessionKey][roiKey]->flagged;
    m_sessions[sessionKey][roiKey]->flagged = !flag;
    return !flag;
}

bool RoiData::removeRoiLink(RoiAddress& src)
{
    //LOG(DBUG) << "Removing link from " << src.sessionKey.toStdString() << " : " << src.roiKey.toStdString();

    // Update ref's linked-in information
    QString refRoiKey = m_sessions[src.sessionKey][src.roiKey]->linkedName;
    if (refRoiKey != "") {
        m_sessions[m_refSession][refRoiKey]->linkedIn.remove(src.sessionKey);
        if (m_sessions[m_refSession][refRoiKey]->linkedIn.empty()) {
            m_sessions[m_refSession][refRoiKey]->confirmed = false;
        }

        // remove the link
        m_sessions[src.sessionKey][src.roiKey]->linkedName.clear();
        m_sessions[src.sessionKey][src.roiKey]->linkedSession.clear();
        m_sessions[src.sessionKey][src.roiKey]->confirmed = false;
        LOG(INFO) << "Link info removed for " << src.sessionKey.toStdString() << " : " << src.roiKey.toStdString();
        return true;
    } else {
        return false;
    }
}

void RoiData::setSelected(roi_t roi)
{
    roi->selected = true;
    if (m_prevSelection && roi != m_prevSelection) {
        m_prevSelection->selected = false;
    }
    m_prevSelection = roi;
}

/// Returns the number of rows in the reference session
int RoiData::refSessionSize() const
{
    if (m_refSession == "") {
        return 0;
    } else {
        return m_sessions[m_refSession].size();
    }
}

/// If an ROI changes its ref link, this method is called to get rid of the previous link
/// @return: true if a link was removed, false otherwise
bool RoiData::removeOldLink(const roi_key_t refRoiName, const session_key_t sessionName)
{
    QStringList strlistTemp;
    if (m_sessions.size() > 0 && m_refSession != "")
    {
        roi_t roi = m_sessions[m_refSession][refRoiName];

        // Does the session have a link in?
        auto it = roi->linkedIn.find(sessionName);
        if(it != roi->linkedIn.end()) {
            // Get that old ROI linking in (lock() is to convert weak_ptr to shared_ptr)
            if (auto roiPtr = it.value().lock()) {
                // Unlink it
                roiPtr->linkedName.clear();
                roiPtr->linkedSession.clear();
                roiPtr->confirmed = false;
                LOG(INFO) << "Link removed from " << sessionName.toStdString() << " ["<< roiPtr->name.toStdString() << "]";

                // remove from ref roi's data
                roi->linkedIn.remove(sessionName);
                return true;
            }
            else {
                LOG(WARNING) << "Weak pointer expired.";
            }
        }
    }
    else
    {
        LOG(WARNING) << "No ref session yet.";
    }
    return false;
}

/**
 * @param rowIndex: The desired row (index must be in range of ref sessions's rows)
 * @param colNames: What columns to use (this also indicates the order)
 * @returns a row into a matrix showing a ref ROI and linked ROIs of other sessions
 */
QStringList RoiData::traverseRefNames(const int rowIndex, const QStringList colNames) const
{
    QStringList strlistTemp;
    if (m_sessions.size() > 0)
    {
        if (m_refSession == "")
        {
            // ref not set yet, so just use whatever's the first added column for now
            return strlistTemp;
        }

        auto names = m_sessions[m_refSession].keys();
        //qDebug() << "names[rowIndex]:" << names[rowIndex];

        if (rowIndex >= names.size()) {
            LOG(WARNING) << "Row index " << rowIndex << " invalid (size: " << names.size() << ")";
            return strlistTemp;
        }

        // now to see if there are any linked sessions for this ROI
        for (QString col : colNames) {
            if (col != m_refSession) {
                QString roiName = "?";
                for (auto it : m_sessions[col].values()) {
                    //qDebug() << "name:" << it.value()->name << " linkedname: " << it.value()->linkedName << " linkedSess:" << it.value()->linkedSession;
                    if ((names[rowIndex] == it->linkedName) && (m_refSession == it->linkedSession)) {
                        roiName = it->name;
                    }
                }
                strlistTemp.push_back(roiName);
            }
        }

        if (!strlistTemp.empty()) {
            // arrange first column to be the ref
            strlistTemp.push_front(names[rowIndex]);
        } else {
            //qDebug() << "No links found for " << names[rowIndex];
            //LOG(WARNING) << "No links found for " << names[rowIndex].toStdString();
        }
    }
    else
    {
        LOG(WARNING) << "No sessions added yet.";
    }
    return strlistTemp;
}

std::vector<LinkCount> RoiData::refLinkCounts()
{
    std::vector<LinkCount> linkCounts;
    int i = 0;
    for (auto roi : m_sessions[m_refSession]) {
        linkCounts.push_back(LinkCount(i++, roi->linkedIn.size()));
    }
    return linkCounts;
}

std::vector<RoiData::IndexedRoi> RoiData::indexedRoisAtSession(session_key_t sessionKey) const
{
    std::vector<IndexedRoi> rois;
    int i = 0;
    for (auto roi : m_sessions[sessionKey]) {
        IndexedRoi indexedRoi;
        indexedRoi.index = i++;
        indexedRoi.roi = roi;
        rois.push_back(indexedRoi);
    }
    return rois;
}

std::vector<std::shared_ptr<SearchResult>> RoiData::searchRois(const QString& sessionKey, QString namePattern)
{
    std::vector<std::shared_ptr<SearchResult>> results;
    bool rightWild = namePattern.right(1) == "*";
    if (rightWild) {
        namePattern.chop(1);
    }

    //QList<session_t> searchSessions;
    QHash<session_key_t, session_t> searchSessions;
    if (sessionKey == "All") {
        searchSessions = m_sessions;
    } else {
        searchSessions[sessionKey] = m_sessions[sessionKey];
    }

    for(auto session = searchSessions.begin(); session != searchSessions.end(); ++session)
    {
        if (rightWild)
        {
            for (session_t::iterator it = session.value().begin(); it != session.value().end(); ++it)
            {
                if ((*it)->name.startsWith(namePattern)) {
                    results.push_back(std::make_shared<SearchResult>((*it)->name, session.key(), false));
                }
            }
        }
        else
        {
            auto rit = session.value().find(namePattern);
            if (rit != session.value().end())
            {
                results.push_back(std::make_shared<SearchResult>(rit.value()->name, session.key(), false));
            }
        }
    }

    return results;
}

/// @returns roi struct smart ptr or nullptr if session key or roi key are not found
RoiData::roi_t RoiData::fieldsAt(session_key_t sessionKey, roi_key_t roiKey) const
{
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        auto session = it.value();
        auto rit = session.find(roiKey);
        if (rit != session.end()) {
            return rit.value();
        } else {
            //LOG(WARNING) << "ROI key ("<< roiKey.toStdString() << ") unknown.";
            return nullptr;
        }
    } else {
        LOG(WARNING) << "Session key ("<< sessionKey.toStdString() << ") unknown.";
        return nullptr;
    }
}

int RoiData::roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const
{
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        auto session = it.value();
        int i = 0;
        for (auto rit = session.begin(); rit != session.end(); ++rit, ++i) {
            if (rit.key() == roiKey) {
                return i;
            }
        }
    } else {
        LOG(WARNING) << "Session key ("<< sessionKey.toStdString() << ") unknown.";
        return -1;
    }
    LOG(WARNING) << "ROI key ("<< roiKey.toStdString() << ") unknown.";
    return -1;
}

/**
 * @param sessionKey: a non-ref session
 * @param roi: ROI in question
 * @return ref session ROI index linked to from the argument or -1 if nothing found
 */
int RoiData::linkedRefIndexAt(const session_key_t sessionKey, RoiData::roi_t roi) const
{
    if (sessionKey == m_refSession) {
        // this is the ref session so ignore
        return -1;
    }
    int i = 0;
    for (auto it : m_sessions[m_refSession].values()) {
        if (it->linkedIn.contains(sessionKey)) {
            // is this the exact roi?
            if (it->linkedIn[sessionKey].lock() == roi) {
                return i;
            }
        }
        ++i;
    }
    return -1;
}

QList<RoiData::roi_t> RoiData::roisAtSession(session_key_t sessionKey) const
{
    QList<roi_t> ret;
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        return it->values();
    } else {
        LOG(WARNING) << "Session key ("<< sessionKey.toStdString() << ") unknown.";
    }
    return ret;
}

///@returns list of roi smart pointers (list may be empty)
QList<RoiData::roi_t> RoiData::roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const
{
    auto it = m_maskTables.find(sessionKey);
    if (it != m_maskTables.end()) {
        return it.value()[z];
    } else {
        return QList<RoiData::roi_t>();
    }
}

QList<QString> RoiData::columnNamesUnordered()
{
    return m_sessions.keys();
}

/// Populate/update the linkedIn structures in the ref session
void RoiData::refreshRefLinks()
{
    LOG(INFO) << "Refreshing ref link data";
    if (m_refSession != "") {
        session_t& refSess = m_sessions[m_refSession];

        // clear all existing linked-in entries in ref session
        for (auto roi : refSess) {
            roi->linkedIn.clear();
        }

        // rebuild: go through all non-ref ROIs, adding linked ones into ref's linked-in structures
        auto keys = m_sessions.keys();
        keys.removeOne(m_refSession);
        for (auto key : keys) {
            for (auto roi : m_sessions[key]) {
                if (roi->linkedSession == m_refSession) {
                    refSess[roi->linkedName]->linkedIn[key] = roi;
                    refSess[roi->linkedName]->confirmed = true;
                    refreshFullyLinked(refSess[roi->linkedName]);
                }
            }
        }
    }
}

// Update the fullyLinked flag in a ref session roi data structures
void RoiData::refreshFullyLinked(roi_t roi)
{
    bool flag = roi->linkedIn.size() >= (m_sessions.size() - 1);
    roi->fullyLinked = flag;
}

/// Initialize / construct extra ROI data we'll need based on the raw data
void RoiData::refreshRoi(session_key_t session, roi_t roi)
{
    if (roi->linkedSession != "" && roi->linkedName != "") {
        roi->confirmed = true;
    }

    for (auto v : roi->mask) {
        // look at all the mask coords and find the extents
        if (roi->masks[v.z].extent.isNull()) {
            roi->masks[v.z].extent.setCoords(v.x, v.y, 1, 1);
        }
        if (v.x < roi->masks[v.z].extent.x()) {
            roi->masks[v.z].extent.setLeft(v.x);
        }
        if (v.x > roi->masks[v.z].extent.right()) {
            roi->masks[v.z].extent.setRight(v.x);
        }
        if (v.y < roi->masks[v.z].extent.top()) {
            roi->masks[v.z].extent.setTop(v.y);
        }
        if (v.y > roi->masks[v.z].extent.bottom()) {
            roi->masks[v.z].extent.setBottom(v.y);
        }

        // add this to the z tables
        if (!m_maskTables[session][v.z].contains(roi)) {
            m_maskTables[session][v.z].append(roi);
        }
    }

    createMaskImages(roi->mask, roi->masks);
    createMaskOutlines(roi->masks);

    roi->mask.clear(); // don't need to waste this memory anymore
}

void RoiData::removeRoiFromMaskTables(session_key_t session, roi_t roi)
{
    LOG(INFO) << "Removing " << roi->name.toStdString() << " from mask tables.";
    for (z_key_t z : roi->masks.keys()) {
        // remove from z tables
        m_maskTables[session][z].removeAll(roi);
    }
}

/// Converts a stack of 2d images representing a 3d mask into a flat list of 3d coordinates
void RoiData::maskImageToVector(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks, bool append)
{
    QHash<z_key_t, RoiMask>::const_iterator it;
    if (!append) {
        coords.clear();
    }
    for (it = masks.constBegin(); it != masks.constEnd(); ++it)
    {
        int z = it.key();
        int x_offset = it.value().extent.x();
        int y_offset = it.value().extent.y();

        const QImage& img = it.value().mask;
        unsigned int maxY = static_cast<unsigned int>(img.height());
        unsigned int maxX = static_cast<unsigned int>(img.width());
        for (int y = 0; y < maxY; ++y) {
            for (int x = 0; x < maxX; ++x) {
                int p = img.pixelIndex(x, y);
                if (p == 1) {
                    VoxelCoordinate v(x_offset + x, y_offset + y, z);
                    coords.push_back(v);
                }
            }
        }
    }
}

/// Converts a flat list of 3d coordinates into a stack of 2d images
void RoiData::createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks)
{
    QVector<QRgb> colors{QColor(0, 0, 0, 0).rgba(), QColor(255, 0, 0).rgba()};
    for (auto v : coords) {
        if (masks[v.z].mask.isNull()) {
            QImage img(masks[v.z].extent.width(), masks[v.z].extent.height(), QImage::Format_Mono);
            img.fill(0);
            img.setColorTable(colors);
            masks[v.z].mask = img;
        }

        masks[v.z].mask.setPixel(v.x - masks[v.z].extent.x(), v.y - masks[v.z].extent.y(), 1);
    }
}


void RoiData::createMaskOutlines(QHash<z_key_t, RoiMask>& masks)
{
    float scale = 0.82f;

    for (auto k : masks.keys()) {
        int sx = static_cast<int>(static_cast<float>(masks[k].mask.width()) * scale);
        int sy = static_cast<int>(static_cast<float>(masks[k].mask.height()) * scale);
        QImage middle = masks[k].mask.scaled(sx, sy, Qt::KeepAspectRatio);
        QImage outline = masks[k].mask.copy();
        QPainter p(&outline);
        p.setCompositionMode(QPainter::CompositionMode_DestinationOut);
        p.drawImage(masks[k].mask.width() / 2 - middle.width() / 2, masks[k].mask.height() / 2 - middle.height() / 2, middle); ///todo coords
        p.end();
        masks[k].maskOutline = outline;
    }
}

/**
 * @param mask: RoiMask member of an Roi object
 * @param x: relative to the mask's coordinates
 * @param y: relative to the mask's coordinates
 * @param set: true to set pixel, false to clear pixel
 * @return
 */
bool RoiData::modifyMask(RoiMask& mask, const int x, const int y)
{
    int mx = x;
    int my = y;
    if (x >= mask.mask.width() || y >= mask.mask.height()) {
        int newWidth = (x >= mask.mask.width()) ? (x + 1) : mask.mask.width();
        int newHeight = (y >= mask.mask.height()) ? (y + 1) : mask.mask.height();
        mask.mask = mask.mask.copy(0, 0, newWidth, newHeight);
        mask.extent.setWidth(mask.mask.width());
        mask.extent.setHeight(mask.mask.height());
    } else if (x < 0 || y < 0) {
        int newWidth = mask.mask.width();
        int newHeight = mask.mask.height();
        int tx = 0;
        int ty = 0;
        if (x < 0) {
            newWidth = newWidth - x;
            tx = x;
            mx = 0;
        }
        if (y < 0) {
            newHeight = newHeight - y;
            ty = y;
            my = 0;
        }
        mask.mask = mask.mask.copy(tx, ty, newWidth, newHeight);
        mask.extent.adjust(tx, ty, 0, 0);
    }
    unsigned int newColor = (mask.mask.pixel(mx, my) == 0) ? 1 : 0;
    mask.mask.setPixel(mx, my, newColor);
    return true;
}

/// Merge ROI data to the first ROI in the given list
void RoiData::mergeToFirst(session_key_t session, const QList<RoiData::roi_t>& rois)
{
    LOG(INFO) << "Merging " << rois.size() << " ROIs";

    // create one big flat list of mask coords from all ROIS
    QVector<VoxelCoordinate> coords;
    for (auto roi : rois) {
        maskImageToVector(coords, roi->masks, true);
    }
    rois[0]->mask = coords;
    rois[0]->masks.clear();

    // update extents and turn that into a new set of mask images
    refreshRoi(session, rois[0]);
}

void RoiData::split(session_key_t session, roi_t src, roi_t dest)
{
//    mask.mask = mask.mask.copy(tx, ty, newWidth, newHeight);
//    mask.extent.adjust(tx, ty, 0, 0);
}
