/// @author Sam Kenyon <sam@metadevo.com>
#include <memory>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QUrl>
#include <QThread>
#include <g3log/g3log.hpp>

#ifdef MATLAB_SUPPORT
#include "MatFile.hpp"
#endif

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "Project.hpp"
#include "HyperstackTiff.hpp"

Project::Project() :
    m_name("blank"),
    m_confirmedModel(&m_roiData, true)
{
    m_filePath = QUrl::fromLocalFile("blank.json");
    LOG(INFO) << "new: " << m_filePath.toString().toStdString();
    saveConfig();
}

Project::Project(const QUrl fileUrl) :
    m_confirmedModel(&m_roiData, true)
{
    m_filePath = fileUrl;
    LOG(INFO) << "new: " << m_filePath.toString().toStdString();
    try
    {
        loadConfig();
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

Project::~Project()
{
    LOG(DBUG) << "Destroying Project";
    for (auto i : m_stack) {
        delete i;
    }
}

void Project::setName(const QString name)
{
    m_name = name;
    saveConfig();
    AppSettings::instance().updateRecentProjectName(name, m_filePath.toLocalFile());
}

void Project::saveProjectAs(const QUrl fileUrl)
{
    try
    {
        m_filePath = fileUrl;
        saveConfig();
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

void Project::loadStack(const QUrl fileUrl)
{
    QString pathStr = fileUrl.toLocalFile();
    loadStack(pathStr);
}

void Project::loadStack(const QString& pathStr)
{
    try
    {
        stack_ptr_t stack;
        if (findStack(pathStr))
        {
            LOG(INFO) << "Stack " << pathStr.toStdString() << "already loaded.";
            m_currentStack = m_findResult;
            stack = m_stack[m_currentStack];
        }
        else
        {
            LOG(INFO) << "Creating a stack...";
            stack = new HyperstackTiff();
            stack->load(pathStr);
            m_stack.push_back(stack);
            m_currentStack = m_stack.size() - 1;
        }

        if (!m_openingProject) {
            // we always show a slice window after first loading
            StackWindow windowConfig;
            windowConfig.stackIndex = m_currentStack;
            windowConfig.width = stack->size().x;
            m_stackWindows.push_back(windowConfig);
            m_currentStackWindow = m_stackWindows.size() - 1;

            saveConfig();
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

/// Removes the stack and associated ROIs from memory.
void Project::unloadStack(const QString& name)
{
    m_roiData.removeSession(name);

    m_confirmedModel.removeColumn(name);
    m_confirmedModel.regen();

    //remove from m_unconfirmedModels
    int index = 0;
    for (auto model : m_unconfirmedModels) {
        if (model->columnNameAt(0) == name) {
            break;
        }
        ++index;
    }
    m_unconfirmedModels.removeAt(index);
    for (auto model : m_unconfirmedModels) {
        model->regen();
    }
    LOG(INFO) << "Removed " << name.toStdString() << " from unconfirmed models.";

    // remove stack pointer(s)
    std::vector<stack_ptr_t>::iterator it;
    stack_index_t id = 0;
    bool found = false;
    for (it = m_stack.begin(); it != m_stack.end(); ++id)
    {
        if ((*it)->name() == name) {
            it = m_stack.erase(it);
            found = true;
            break;
        } else {
            ++it;
        }
    }

    if (found) {
        // remove associated slice window(s)
        std::vector<StackWindow>::iterator it;
        for (it = m_stackWindows.begin(); it != m_stackWindows.end();)
        {
            if (it->stackIndex == id) {
                it = m_stackWindows.erase(it);
                found = true;
            } else {
                ++it;
            }
        }

        saveConfig();
        LOG(INFO) << "Removed stack: " << name.toStdString();
    } else {
        LOG(WARNING) << "Stack not found: " << name.toStdString();
    }
}

void Project::loadRoiData(const QUrl fileUrl)
{
    QString pathStr = fileUrl.toLocalFile();
    loadRoiData(pathStr);
}

void Project::loadRoiData(const QString& pathStr)
{
    try
    {
        QFileInfo info(pathStr);
        QString suffix = info.suffix().toLower();
        QString keyName = info.baseName();
        RoiFile rf;
        rf.savePath = pathStr;

        rf.sessionKey = keyName;
        m_roiFiles.append(rf);

        bool loaded = true;
        if (suffix == "mat") {
            LOG(INFO) << "Attempting to load as Matlab file...";

            loaded = loadMatlabRoiFile(pathStr, keyName);
            if (!loaded) {
                LOG(WARNING) << "Loading Matlab file failed.";
            }
        } else if (suffix == "csv") {
            LOG(INFO) << "Attempting to load as CSV file...";

            loaded = loadCsvRoiFile(pathStr, keyName);
            if (!loaded) {
                LOG(WARNING) << "Loading CSV file failed.";
            }
        } else {
             LOG(WARNING) << "Unrecognized file type.";
        }

        if (loaded && !m_openingProject) {
            saveConfig();
        }

        // post processing
        updateUnconfirmedNameMap();
        m_roiData.refreshRefLinks();
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

bool Project::loadMatlabRoiFile(QString pathStr, QString keyName)
{
    #ifdef MATLAB_SUPPORT
    if (m_roiData.addNewSession(keyName))
    {
        MatFile m(m_roiData);
        if (!m.load(pathStr.toStdString())) {
            LOG(INFO) << "Load matlab file failed.";
            return false;
        }

        auto model = std::make_shared<RoiDataModel>(&m_roiData);
        model->addColumn(keyName);
        m_unconfirmedModels.push_back(model);
        m_confirmedModel.addColumn(keyName);

        model->setSorted(SortChoice::NAME); // sort unconfirmed models by name
        //model->regen(); //will be done in resetModels below
        resetModels();

        return true;
    }
    else
    {
        LOG(WARNING) << "addNewSession failed.";
        return false;
    }
    #else
    return false;
    #endif
}

bool Project::loadCsvRoiFile(QString pathStr, QString keyName)
{
    if (m_roiData.addNewSession(keyName))
    {
        QFile file(pathStr);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << file.errorString();
            file.close();
            return false;
        }

        auto model = std::make_shared<RoiDataModel>(&m_roiData);
        model->addColumn(keyName);
        m_unconfirmedModels.push_back(model);
        m_confirmedModel.addColumn(keyName);

        //unsigned int n = 0;
        QStringList wordList;
        std::shared_ptr<RoiFields> fields(nullptr);
        while (!file.atEnd())
        {
            QString line = file.readLine();
            QStringList row = line.split(",");
            if (row[0] == "\"roi\"") {
                line = file.readLine();
                row = line.split(",");
                if (fields) { // done with last ROI
                    LOG(DBUG) << "Adding new ROI " << fields->name.toStdString();
                    m_roiData.addNewRoi(fields->name, fields);
//                    for (auto v : fields->mask) {
//                        LOG(DBUG) << "mask: " << v.x << ", " << v.y << ", " << v.z;
//                    }
                }
                fields = std::make_shared<RoiFields>();
                QString keyName = row[3].remove('\"');
                fields->name = keyName;
                /// For some reason it seems x and y are swapped in the data
                fields->centroid = VoxelCoordinate(row[5].toInt(), row[4].toInt(), row[6].toInt());
                VoxelCoordinate v(row[1].toInt(), row[0].toInt(), row[2].toInt());
                fields->mask.push_back(v);

                if (row.size() >= 10) {
                    fields->linkedName = row[9].remove('\"');
                    fields->linkedSession = row[10].remove('\"').remove('\n');
                }
            }
            else
            {
                if (fields) {
                    if (row.size() > 2) {
                        /// For some reason it seems x and y are swapped in the data
                        VoxelCoordinate v(row[1].toInt(), row[0].toInt(), row[2].toInt());
                        fields->mask.push_back(v);
                    }
                } else {
                    LOG(WARNING) << "Mask data for unknown ROI";
                }
            }
        }

        model->setSorted(SortChoice::NAME); // sort unconfirmed models by name
        model->regen();
        file.close();
    }
    else
    {
        LOG(WARNING) << "addNewSession failed.";
        return false;
    }
    return true;
}

void Project::saveAllRoiFiles()
{
    LOG(INFO) << "Attempting to save " << m_roiFiles.size() << " files";
    for (auto roifile : m_roiFiles) {
        #ifdef MATLAB_SUPPORT
            LOG(INFO) << "Saving " << roifile.savePath.toStdString();
            MatFile m(m_roiData);
            m.save(roifile.sessionKey, roifile.savePath.toStdString());
        #else
            LOG(INFO) << "Did not save! Matlab support disabled.";
        #endif
    }
    LOG(INFO) << "Done saving." ;
}

/// Move the ref session to index 0
void Project::resortModels(const QString refSessionKey)
{
    LOG(DBUG) << "ReSORTing models...";
    std::shared_ptr<RoiDataModel> it;
    foreach(it, m_unconfirmedModels) {
        if (it->columnNameAt(0) == refSessionKey) {
            m_unconfirmedModels.removeAll(it);
            m_unconfirmedModels.push_front(it);
        }
    }

    updateUnconfirmedNameMap();

    LOG(DBUG) << "Resorted models.";
}

void Project::updateUnconfirmedNameMap()
{
    // update map for quick access via session name
    std::shared_ptr<RoiDataModel> it;
    foreach(it, m_unconfirmedModels) {
        m_unconfirmedModelNameMap[it->columnNameAt(0)] = it;
    }
}

/// A reset is a quicker operation to notify model users,
/// whereas a regen will in addition recreate whatever data/dimensions are in the models.
/// If ROI data has been modified, you should regenerate.
void Project::resetModels(bool regenConfirmed)
{
    if (regenConfirmed) {
        LOG(DBUG) << "REGENERATING models.";

        m_confirmedModel.regen();

        std::shared_ptr<RoiDataModel> it;
        foreach(it, m_unconfirmedModels) {
            it->regen();
        }
    } else {
        LOG(DBUG) << "Resetting models.";

        m_confirmedModel.signalReset();

        std::shared_ptr<RoiDataModel> it;
        foreach(it, m_unconfirmedModels) {
            it->signalReset();
        }
    }
}

void Project::resortConfirmedModel(const SortChoice sortChoice)
{
    m_confirmedModel.setSorted(sortChoice);
    m_confirmedModel.regen();
}

void Project::resortConfirmedModelByName(const int column)
{
    m_confirmedModel.setSorted(SortChoice::NAME);
    m_confirmedModel.setSortColumn(column);
    m_confirmedModel.regen();
}

bool Project::findStack(const QString& filePath)
{
    for (stack_index_t i = 0; i < m_stack.size(); ++i)
    {
        if (m_stack[i]->filePath() == filePath) {
            // Right now we only have one context == 1 client per stack
            // but we might change that later to optimize multiple
            // views using the same HyperStack object.
            if (m_stack[i]->clients() < m_maxClientsPerStack) {
                m_findResult = i;
                return true;
            }
        }
    }
    return false;
}

stack_ptr_t Project::currentStack()
{
    return (m_stack.size() > 0) ? m_stack.at(m_currentStack) : nullptr;
}

bool Project::loadConfig()
{
    try
    {
        LOG(INFO) << "Loading project file...";
        QFile loadFile(m_filePath.toLocalFile());
        if (!loadFile.open(QIODevice::ReadOnly)) {
            LOG(WARNING) << "Couldn't open config file: " << loadFile.errorString().toStdString();
            return false;
        }
        m_openingProject = true;
        QByteArray saveData = loadFile.readAll();

        QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

        readMisc(loadDoc.object());
        readRoiFiles(loadDoc.object());
        readStacks(loadDoc.object());
        readWindowConfigs(loadDoc.object());

        m_openingProject = false;
        LOG(INFO) << "Loaded project config from " << m_filePath.toLocalFile().toStdString();

        return true;
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
}

void Project::readMisc(const QJsonObject &json)
{
    LOG(INFO) << "Reading JSON file...";
    if (json.contains("project_name") && json["project_name"].isString()) {
        m_name = json["project_name"].toString();
    }
}

void Project::readStacks(const QJsonObject &json)
{
    if (json.contains("stacks") && json["stacks"].isArray())
    {
        m_stack.clear();
        QJsonArray imagesArray = json["stacks"].toArray();
        for (int i = 0; i < imagesArray.size(); ++i) {
            QUrl url = QUrl::fromLocalFile(imagesArray[i].toString());
            LOG(DBUG) << "stack/image file: " << url.toString().toStdString();
            loadStack(url);
        }
    }
}

void Project::readWindowConfigs(const QJsonObject &json)
{    
    if (json.contains("stack_windows") && json["stacks"].isArray())
    {
        m_stackWindows.clear();
        QJsonArray windowsArray = json["stack_windows"].toArray();
        unsigned int arraySize = static_cast<unsigned int>(windowsArray.size());
        for (unsigned int i = 0; i < arraySize; ++i) {
            StackWindow windowConfig;
            QJsonObject obj = windowsArray[i].toObject();
            windowConfig.stackIndex = obj["stackId"].toInt();
            windowConfig.coords.z = obj["z"].toInt();
            windowConfig.coords.time = obj["t"].toInt();
            windowConfig.zoomFactor = obj["zoomFactor"].toInt();
            windowConfig.offsetX = obj["offsetX"].toDouble();
            windowConfig.offsetY = obj["offsetY"].toDouble();
            windowConfig.width = obj["width"].toDouble();
            windowConfig.windowX = obj["windowX"].toInt();
            windowConfig.windowY = obj["windowY"].toInt();
            m_stackWindows.push_back(windowConfig);
        }
    }
}

void Project::readRoiFiles(const QJsonObject &json)
{
    m_referenceName = "";
    if (json.contains("roi_files") && json["roi_files"].isArray())
    {
        QJsonArray roiFilesArray = json["roi_files"].toArray();
        for (int i = 0; i < roiFilesArray.size(); ++i) {
            QUrl url = QUrl::fromLocalFile(roiFilesArray[i].toString());
            qDebug() << "roi file: " << url.toString();
            loadRoiData(url);
        }
    }
    if (json.contains("ref"))
    {
        QString refName = json["ref"].toString();
        if (refName != "") {
            qDebug() << "set ref session";
            m_referenceName = refName;
            //ApplicationData::instance().setRefSession(refName);
        }
    }
}

void Project::saveStackZ(stack_index_t stack, unsigned int z)
{
    m_stackWindows.at(stack).coords.z = z;
    saveConfig();
}

void Project::saveStackZoom(stack_index_t stack, const int zoomFactor)
{
    m_stackWindows.at(stack).zoomFactor = zoomFactor;
    saveConfig();
}

void Project::saveStackOffset(stack_index_t stack, const float offsetX, const float offsetY)
{
    m_stackWindows.at(stack).offsetX = offsetX;
    m_stackWindows.at(stack).offsetY = offsetY;
    saveConfig();
}

void Project::saveStackWidth(stack_index_t stack, const float width)
{
    m_stackWindows.at(stack).width = width;
    saveConfig();
}

void Project::saveStackWindowPosition(stack_index_t stack, const int x, const int y)
{
    m_stackWindows.at(stack).windowX = x;
    m_stackWindows.at(stack).windowY = y;
    saveConfig();
}

//void Project::saveStackWindowHidden(const stack_index_t windowIndex)
//{
//    ///@todo set a visible flag?
//    saveConfig();
//}

// rewrites entire json object
bool Project::saveConfig() const
{
    try
    {
        QFile saveFile(m_filePath.toLocalFile());

        if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            LOG(WARNING) << "Couldn't open file: " << saveFile.errorString().toStdString();
            return false;
        }

        // create the json object
        m_jsonObject = QJsonObject(); // clear
        write();

        // save to file
        QJsonDocument saveDoc(m_jsonObject);
        saveFile.write(saveDoc.toJson());

        // in case it hasn't been added yet to recents list in app settings
        AppSettings::instance().setRecentProject(m_name, m_filePath.toLocalFile());

        return true;
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
}

/// just save only what JSON items have been modified instead of rewriting the whole file
//void Project::updateConfig() const
//{
//    if (!m_saveFile) {
//        saveConfig();
//    } else {
//        QJsonDocument saveDoc(m_jsonObject);
//        m_saveFile->seek(0);
//        m_saveFile->write(saveDoc.toJson());
//    }
//}

//void Project::updateStacksConfig() const
//{
//    writeStacks();
//    writeWindowConfig();
//    saveConfig();
//}

void Project::write() const
{
    m_jsonObject["neurotator_ver"] = QCoreApplication::applicationVersion();
    m_jsonObject["project_name"] = m_name;

    writeStacks();
    writeWindowConfig();

    QJsonArray roiFilesArray;
    for (auto roiFile : m_roiFiles) {
         roiFilesArray.append(roiFile.savePath);
    }
    m_jsonObject["roi_files"] = roiFilesArray;
}

void Project::writeStacks() const
{
    QJsonArray imagesArray;
    for (auto stack : m_stack) {
         imagesArray.append(stack->filePath());
    }
    m_jsonObject["stacks"] = imagesArray;

    m_jsonObject["ref"] = m_roiData.refSessionName();
}

void Project::writeWindowConfig() const
{
    QJsonArray windowsArray;
    for (auto w : m_stackWindows) {
        QJsonObject windowConfig;
        windowConfig["stackId"] = static_cast<int>(w.stackIndex);
        windowConfig["z"] = static_cast<int>(w.coords.z);
        windowConfig["t"] = static_cast<int>(w.coords.time);
        windowConfig["zoomFactor"] = w.zoomFactor;
        windowConfig["offsetX"] = w.offsetX;
        windowConfig["offsetY"] = w.offsetY;
        windowConfig["width"] = w.width;
        windowConfig["windowX"] = w.windowX;
        windowConfig["windowY"] = w.windowY;
        windowsArray.append(windowConfig);
    }
    m_jsonObject["stack_windows"] = windowsArray;
}

RoiDataModel* Project::unconfirmedModel(const int index) {
    //qDebug() << "Project: request for model at " << index;
    //qDebug() << "m_unconfirmedModels.size(): " << m_unconfirmedModels.size();
    if (index < m_unconfirmedModels.size()) {
        RoiDataModel* obj = m_unconfirmedModels[index].get();
        QQmlEngine::setObjectOwnership(obj, QQmlEngine::CppOwnership);
        return obj;
    } else {
        LOG(WARNING) << "Index out of bounds.";
        return nullptr;
    }
}


/// sets both the current stack window and the current stack to the associated stack index
void Project::setCurrentStackWindow(const stack_index_t index)
{
    m_currentStackWindow = index;
    m_currentStack = m_stackWindows[index].stackIndex;
}

QStringList Project::stackNames()
{
    QStringList ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append((*it)->name());
    }
    return ret;
}

QList<QUrl> Project::stackUrls()
{
    QList<QUrl> ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append(QUrl::fromLocalFile((*it)->filePath()));
    }
    return ret;
}

QList<QUrl> Project::stackWindowUrls()
{
    QList<QUrl> ret;
    std::vector<StackWindow>::iterator it;
    for (it = m_stackWindows.begin(); it != m_stackWindows.end(); ++it) {
        ret.append(QUrl::fromLocalFile(m_stack[it->stackIndex]->filePath()));
    }
    return ret;
}
