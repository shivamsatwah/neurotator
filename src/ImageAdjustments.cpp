// @author Sam Kenyon <sam@metadevo.com>
#include <vector>
#include <g3log/g3log.hpp>
#include <QDebug>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "ImageAdjustments.hpp"

using namespace cv;

QImage ImageAdjustments::equalizeHistogram(QImage input)
{
    input = input.convertToFormat(QImage::Format_RGB888);
    Mat image(input.height(), input.width(), CV_8UC3, input.bits(), input.bytesPerLine());
    if (!image.data)
    {
        qDebug() << "equalizeHistogram(): ERROR: Image is null!";
        return input;
    }

    // Create a new matrix to hold the HSV image
    Mat HSV;

    // convert RGB image to HSV
    cv::cvtColor(image, HSV, COLOR_RGB2HSV);
    std::vector<Mat> hsv_planes;
    split(HSV, hsv_planes);
    Mat h = hsv_planes[0];
    Mat s = hsv_planes[1];
    Mat v = hsv_planes[2];

    Mat equalizedV;
    equalizeHist(v, equalizedV);
    hsv_planes[2] = equalizedV;

    Mat mrg;
    merge(hsv_planes, mrg);
    Mat dst;
    cvtColor(mrg, dst, COLOR_HSV2RGB);

    QImage output(dst.data, dst.cols, dst.rows, dst.step, QImage::Format_RGB888);
    return output.convertToFormat(QImage::Format_ARGB32);
}
