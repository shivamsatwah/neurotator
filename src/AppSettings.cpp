/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QStandardPaths>

#include <g3log/g3log.hpp>

#include "AppSettings.hpp"

AppSettings::AppSettings()
{
    LOG(INFO) << "Application settings path: " << m_settings.fileName().toStdString();
}

void AppSettings::set(const QString name, const QVariant value)
{
    m_settings.setValue(name, value);
}

void AppSettings::setRecentProject(const QString projectName, const QString projectPath)
{
    if (!recentProjectsContains(projectPath)) {
        int recentCount = numRecentProjects();
        recentCount++;
        if (recentCount >= maxRecentProjects()) {
            recentCount = 0;
        }
        m_settings.setValue("recentproject/" + QString::number(recentCount - 1), projectPath);
        m_settings.setValue("recentprojectname/" + QString::number(recentCount - 1), projectName);
        m_settings.setValue("recentcount", recentCount);
    }
}

void AppSettings::updateRecentProjectName(const QString projectName, const QString projectPath)
{
    for (int i = 0; i < numRecentProjects(); ++i) {
        if (m_settings.value("recentproject/" + QString::number(i)) == projectPath) {
            m_settings.setValue("recentprojectname/" + QString::number(i), projectName);
        }
    }
}

void AppSettings::clearRecentProjects()
{
    m_settings.remove("recentproject");
    m_settings.remove("recentprojectname");
    m_settings.setValue("recentcount", 0);
}


bool AppSettings::recentProjectsContains(const QString projectPath)
{
    for (int i = 0; i < numRecentProjects(); ++i) {
        if (m_settings.value("recentproject/" + QString::number(i)) == projectPath) {
            return true;
        }
    }
    return false;
}

int AppSettings::maxRecentProjects() const
{
    return m_settings.value("maxrecents", 100).toInt();
}

int AppSettings::numRecentProjects() const
{
    return m_settings.value("recentcount", 0).toInt();
}

QList<QUrl> AppSettings::recentProjects() const
{
    QList<QUrl> recents;
    for (int i = numRecentProjects() - 1; i >= 0 ; --i) {
        recents.append(QUrl::fromLocalFile(m_settings.value("recentproject/" + QString::number(i)).toString()));
    }
    return recents;
}

QStringList AppSettings::recentProjectNames() const
{
    QStringList recents;
    for (int i = numRecentProjects() - 1; i >= 0 ; --i) {
        recents.append(m_settings.value("recentprojectname/" + QString::number(i)).toString());
    }
    return recents;
}

QUrl AppSettings::defaultProjectDirectory()
{
    if (!m_settings.contains("defaultprojectdir")) {
        set("defaultprojectdir", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    }
    return m_settings.value("defaultprojectdir").toString();
}
