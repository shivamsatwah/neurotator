/// @author Sam Kenyon <sam@metadevo.com>
#include <algorithm>
#include <functional>
#include <vector>
#include <QDebug>
#include <g3log/g3log.hpp>

#include "ApplicationData.hpp"
#include "RoiDataModel.hpp"

RoiColumns::RoiColumns(QStringList linkedRois)
    : m_linkedRois(linkedRois)
{
}

QString RoiColumns::atColumn(const int col) const
{
    if (col >= m_linkedRois.size()) {
        LOG(WARNING) << "column " <<  col << " out of bounds";
        return "";
    } else {
        return m_linkedRois[col];
    }
}

QStringList RoiColumns::linkedRois() const
{
    return m_linkedRois;
}

void RoiColumns::setLinkedRois(QStringList roiList)
{
    m_linkedRois = roiList;
}

///////////////////////////////////////////////////////////////////////
RoiDataModel::RoiDataModel(RoiData* roiData, bool linkedColumns, QObject *parent)
    : QAbstractListModel(parent),
      m_roiData(roiData),
      m_linkedColumns(linkedColumns)
{
}

RoiDataModel::~RoiDataModel()
{
    LOG(DBUG) << "RoiDataModel destroyed.";
}

void RoiDataModel::addColumn(QString keyname)
{
    beginInsertColumns(QModelIndex(), columnCount(), columnCount());
    m_colNames.push_back(keyname);
    endInsertColumns();
}

void RoiDataModel::removeColumn(QString keyname)
{
    int index = m_colNames.indexOf(keyname);
    if (index != -1) {
        LOG(INFO) << "Removing column " << keyname.toStdString();
        beginRemoveColumns(QModelIndex(), index, index);
        m_colNames.removeAt(index);
        endRemoveColumns();
    }
}

void RoiDataModel::signalReset()
{
    beginResetModel();
    endResetModel();
}

void RoiDataModel::regen()
{
    beginResetModel();
    regenerateColumns();
    if (m_sorted == SortChoice::UNSORTED) {
        regenerateRows();
    } else {
        sortRows();
    }
    endResetModel();
}

/// [Re]generate the model's list
void RoiDataModel::regenerateRows()
{
    if (m_linkedColumns)
    {
        if (m_sorted == SortChoice::UNSORTED) {
            clearAllRows();
            LOG(DBUG) << "Regenerating " << m_roiData->refSessionSize() << " rows...";
            for (int rowIndex = 0; rowIndex < m_roiData->refSessionSize(); ++rowIndex)
            {
                auto roilist = m_roiData->traverseRefNames(rowIndex, m_colNames);
                addRow(RoiColumns(roilist));
            }
        } else {
            sortRows();
        }
    }
    else
    {
        LOG(INFO) << "Unconfirmed model--regenerating blank rows...";
        if (m_colNames.size() > 0) {
            regenerateBlankRows(m_colNames[0]);
        } else {
            LOG(WARNING) << "colnames is empty.";
        }
    }
    LOG(DBUG) << "Regenerating done.";
}

void RoiDataModel::regenerateBlankRows(QString keyname)
{
    beginInsertRows(QModelIndex(), 0, rowCountAtCol(keyname).toInt() - 1);
    endInsertRows();
}

/// Move the ref session to index 0
void RoiDataModel::resortColumnNames(const QString refSessionKey)
{
    qDebug() << "Trying to resort col names...";

    //emit layoutAboutToBeChanged();
    //beginResetModel();

    QString it;
    foreach(it, m_colNames) {
        if (it == refSessionKey) {
            m_colNames.removeAll(it);
            m_colNames.push_front(it);
            qDebug() << "Resorted col names: " << m_colNames;
            break;
        }
    }
    //changePersistentIndex(index(0, 0), index(m_rows.count() - 1, m_colNames.size() - 1));
    //regen();
    //emit layoutChanged();
    //endResetModel();
    //emit headerDataChanged(Qt::Horizontal, 0, m_colNames.size() - 1);
}

void RoiDataModel::clearAllRows()
{
    LOG(INFO) << "Start removing rows";
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    LOG(INFO) << "Clearing all rows.";
    m_rows.clear();
    LOG(INFO) << "End removing rows";
    endRemoveRows();
}

void RoiDataModel::clearAllColumns()
{
    LOG(INFO) << "Start removing columns";
    beginRemoveColumns(QModelIndex(), 0, columnCount() - 1);
    LOG(INFO) << "Clearing all columns.";
    m_colNames.clear();
    LOG(INFO) << "End removing columns";
    endRemoveColumns();
}

void RoiDataModel::addRow(const RoiColumns &row)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_rows << row;
    endInsertRows();
}

/// updates linked column rows (i.e. the confirmed data model)
//void RoiDataModel::insertRow(const int refIndex)
//{
//    if (!m_linkedColumns){
//        return;
//    }

//    addRow(RoiColumns(m_roiData->traverseRefNames(refIndex, m_colNames)));
//}

/// updates linked column rows (i.e. the confirmed data model)
void RoiDataModel::refreshRow(const int refIndex)
{
    if (!m_linkedColumns){
        return;
    }
    if (refIndex >= m_rows.size()) {
        LOG(WARNING) << "Ref index out of bounds.";
        return;
    }
    m_rows[refIndex].setLinkedRois(m_roiData->traverseRefNames(refIndex, m_colNames));
    dataChanged(createIndex(refIndex, 0), createIndex(refIndex, 0));
}

void RoiDataModel::setSorted(const SortChoice sortChoice)
{
    if (m_sorted == sortChoice) {
        m_sortReverse = !m_sortReverse; // toggle it
    } else {
        m_sorted = sortChoice;
    }
}

/// [Re]generate the model's list
void RoiDataModel::sortRows()
{
    LOG(DBUG) << "Sorting...";

    clearAllRows();

    if (m_sorted == SortChoice::LINKS)
    {
        std::vector<LinkCount> linkCounts = m_roiData->refLinkCounts();

        if (m_sortReverse) {
           std::sort(linkCounts.begin(), linkCounts.end(), [](LinkCount a, LinkCount b) {
               return a.count < b.count;
           });
        } else {
            std::sort(linkCounts.begin(), linkCounts.end(), [](LinkCount a, LinkCount b) {
                return a.count > b.count;
            });
        }

        int i = 0;
        for (auto sortedRow : linkCounts)
        {
            auto roilist = m_roiData->traverseRefNames(sortedRow.index, m_colNames);
            addRow(RoiColumns(roilist));
            m_sortedIndexMap[sortedRow.index] = i;
            i++;
        }
    }
    else if (m_sorted == SortChoice::NAME)
    {
        // This is just for unconfirmed models for now

        std::vector<RoiData::IndexedRoi> rawRows = m_roiData->indexedRoisAtSession(m_colNames[0]);
        std::sort(rawRows.begin(), rawRows.end(), [](RoiData::IndexedRoi a, RoiData::IndexedRoi b) {
            return a.roi->name.toStdString() < b.roi->name.toStdString();
        });
        int i = 0;
        for (auto sortedRow : rawRows)
        {
            m_sortedIndexMap[sortedRow.index] = i;
            m_sortedInverseMap[i] = sortedRow.index;
            i++;
        }
    }
}

/// wrapper to map from sorted to the raw index
int RoiDataModel::roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const
{
    int rawIndex = m_roiData->roiIndexAt(sessionKey, roiKey);
    if (m_sorted != SortChoice::UNSORTED && m_sortedIndexMap.contains(rawIndex)) {
        return m_sortedIndexMap[rawIndex];
    } else {
        return rawIndex;
    }
}

/// wrapper to map from sorted to the raw index
int RoiDataModel::linkedRefIndexAt(const session_key_t sessionKey, RoiData::roi_t roi) const
{
    int rawIndex = m_roiData->linkedRefIndexAt(sessionKey, roi);
    if (m_sorted != SortChoice::UNSORTED) {
        return m_sortedIndexMap[rawIndex];
    } else {
        return rawIndex;
    }
}

void RoiDataModel::regenerateColumns()
{
//    LOG(DBUG) << "Regenerating...";

//    if (m_linkedColumns)
//    {
////        clearAllColumns();

////        beginInsertColumns(QModelIndex(), 0, m_roiData->columnNames().size() - 1);

////        // RoiData uses hash containers which have arbitrary ordering, so make sure our
////        // list of columns matches that ordering.
////        m_colNames = m_roiData->columnNames();

//        qDebug() << "col names:" << m_colNames;
////        endInsertColumns();
//    } else {
//        beginRemoveColumns(QModelIndex(), 0, m_colNames.size() - 1);
//        endRemoveColumns();
//        beginInsertColumns(QModelIndex(), 0, 0);
//        endInsertColumns();
//        qDebug() <<  "regenerateColumns() m_colNames[0]=" << m_colNames[0];
//    }
}

int RoiDataModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    if (m_linkedColumns) {
        return m_rows.count();
    } else {
        if (m_colNames.size() > 0) {
            return rowCountAtCol(m_colNames[0]).toInt();
        } else {
            LOG(WARNING) << "colnames is empty.";
            return 0;
        }
    }
}

int RoiDataModel::columnCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    if (m_linkedColumns) {
        return m_colNames.size();
    } else {
        return 1;
    }
}

QString RoiDataModel::columnNameAt(const int index) const
{
    if (index >= 0 && index < m_colNames.size()) {
        return m_colNames[index];
    } else {
        LOG(WARNING) << "Index (" << index << ") out of bounds (" << m_colNames.size() << ").";
        return "???";
    }
}

/// Custom data accessor (not an override)
QVariant RoiDataModel::dataAt(const int row, int col) const
{
    if (row >= 0 && row < m_rows.size()) {
        return m_rows[row].atColumn(col);
    } else {
        LOG(WARNING) << "row " << row << " is out of bounds of " << m_rows.size();
        return QString();
    }
}

/// @return: number of rows in particular column or 0 if there's no column with that nam
QVariant RoiDataModel::rowCountAtCol(QString keyname) const
{
    return m_roiData->roisAtSession(keyname).size();
}

QVariant RoiDataModel::dataAtSingleCol(QString keyname, const int row) const
{
    //qDebug() << "dataAtSingleCol for keyname " << keyname;
    try
    {
        if (row >= 0 && row < rowCount()) {
            //qDebug() << "dataAtSingleCol value:" << m_roiData->roisAtSession(keyname)[row]->name;
            if (m_sorted == SortChoice::NAME) {
                //qDebug() << "dataAtSingleCol SORTED row:" << row;
                return m_roiData->roisAtSession(keyname)[m_sortedInverseMap[row]]->name;
            } else {
                return m_roiData->roisAtSession(keyname)[row]->name;
            }
        } else {
            return -1;
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return -1;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return -1;
    }
}

//bool RoiDataModel::isSelected(QString sessionKey, const int row) const
//{
//    try
//    {
//        if (row >= 0 && row < rowCount()) {
//            auto rois = m_roiData->roisAtSession(sessionKey);
//            bool flag = false;

//            // Very important to validate row since it could be a row index from a larger column
//            if (row < rois.size()) {
//                flag =  rois[row]->selected;
//            }
//            return flag;
//        } else {
//            return false;
//        }
//    }
//    catch (const std::exception &e)
//    {
//        LOG(WARNING) << "ERROR: " << e.what();
//        return false;
//    }
//    catch (...)
//    {
//        LOG(WARNING) << "Unknown exception";
//        return false;
//    }
//}

/// @todo not actually used
QVariant RoiDataModel::data(const QModelIndex & index, int role) const
{
    Q_UNUSED(role)
    LOG(DBUG) << "This shouldn't be called.";
//    qDebug() << "data row: " << index.row();
//    qDebug() << "data column: " << index.column();
    if (index.row() < 0 || index.row() >= m_rows.count())
        return QVariant();

    //if (role == NormalRole)  // one session is a key and the others are linked
    return dataAt(index.row(), index.column());
    //return QVariant();
}

QVariant RoiDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)
    Q_UNUSED(role)
    return (section < m_colNames.size()) ? m_colNames.at(section) : "?";
}

QHash<int, QByteArray> RoiDataModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NormalRole] = "confirmed";
    roles[SpecialRole] = "unconfirmed";
    return roles;
}

int RoiDataModel::refLinkCount(const int row) const
{
    //return m_rows[row].linkedRois().size();
    if (row >= 0 && row < m_rows.size()) {
        QString roiKey = m_rows[row].atColumn(0);
        if (roiKey != "") {
            return m_roiData->fieldsAt(m_colNames[0], roiKey)->linkedIn.size();
        }
    } else {
        LOG(WARNING) << "row " << row << " is out of bounds of " << m_rows.size();
    }
    return 0;
}
