// @author Sam Kenyon <sam@metadevo.com>
#include <vector>
#include <g3log/g3log.hpp>
#include <QDebug>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "WatershedImagePipeline.hpp"

using namespace cv;

WatershedImagePipeline::WatershedImagePipeline()
{
}

WatershedImagePipeline::~WatershedImagePipeline()
{
}

/// Watershedding based on the OpenCV sample
/// @returns a vector of new segments in coordinate form
ImagePipeline::segments_t WatershedImagePipeline::run(const QImage& input)
{
    // temporarily save to disk so we can use OpenCV on it
    QString tempFile = "imgproc-input.png";
    input.save(tempFile);

    // now open that file with OpenCV API
    Mat image;
    image = imread(tempFile.toStdString(), IMREAD_COLOR);
    if(image.empty())
    {
        qDebug() << "Could not open or find image file.";
    }

    // Create binary image from source image
    Mat bw;
    cvtColor(image, bw, COLOR_BGR2GRAY);
    threshold(bw, bw, 40, 255, THRESH_BINARY | THRESH_OTSU);

    imwrite("imgproc-bw.png", bw);

    // distance transform algorithm
    Mat dist;
    distanceTransform(bw, dist, DIST_C, DIST_MASK_PRECISE);

    // normalize the distance image for range = {0.0, 1.0} so we can threshold it
    normalize(dist, dist, 0, 1.0, NORM_MINMAX);

    // threshold to obtain the peaks: this will be the markers for the foreground objects
    threshold(dist, dist, 0.4, 1.0, THRESH_BINARY);

    // dilate the dist image
    Mat kernel1 = Mat::ones(3, 3, CV_8U);
    dilate(dist, dist, kernel1);

    imwrite("imgproc-dilate.png", dist);

    // Create the CV_8U version of the distance image
    // It is needed for findContours()
    Mat dist_8u;
    dist.convertTo(dist_8u, CV_8U);

    // Find total markers
    std::vector<std::vector<Point> > contours;
    findContours(dist_8u, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

    qDebug() << "contours size: " << contours.size();

    // create the marker image for the watershed algorithm
    Mat markers = Mat::zeros(dist.size(), CV_32S);

    // draw the foreground markers
    for (size_t i = 0; i < contours.size(); i++)
    {
        drawContours(markers, contours, static_cast<int>(i), Scalar(static_cast<int>(i)+1), -1);
    }

    imwrite("imgproc-markers.png", markers);

    // draw the background marker
    circle(markers, Point(5,5), 3, Scalar(255), -1);

    imwrite("imgproc-markersbg.png", markers);

    // perform the watershed algorithm
    watershed(image, markers);

    Mat mark;
    markers.convertTo(mark, CV_8U);
    bitwise_not(mark, mark);
    // how the mark image looks like at that point
    imwrite("imgproc-watershed.png", mark);

    // generate random colors for a diagnostic image
    std::vector<Vec3b> colors;
    for (size_t i = 0; i < contours.size(); i++)
    {
        int b = theRNG().uniform(0, 256);
        int g = theRNG().uniform(0, 256);
        int r = theRNG().uniform(0, 256);
        colors.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }

    QVector<QVector<VoxelCoordinate>> segments; // the return data

    // this image is just for diagnostics
    Mat dst = Mat::zeros(markers.size(), CV_8UC3);

    std::vector<Mat> segs;
    for (size_t segIndex = 0; segIndex < contours.size(); ++segIndex)
    {
        segs.push_back(Mat::zeros(markers.size(), CV_8UC3));
        segments.push_back(QVector<VoxelCoordinate>());
    }
    LOG(INFO) << "Num segments found: " << segs.size();

    for (int i = 0; i < markers.rows; i++)
    {
        for (int j = 0; j < markers.cols; j++)
        {
            int index = markers.at<int>(i,j);
            if (index > 0 && index <= static_cast<int>(contours.size()))
            {
                dst.at<Vec3b>(i,j) = colors[index-1];
                segs[index-1].at<Vec3b>(i,j) = Vec3b(0, 255, 0); // colors the segment as green
                segments[index-1].push_back(VoxelCoordinate(j, i, 0)); // leave z as 0 since this method doesn't have z data
            }
        }
    }

    imwrite("imgproc-dst.png", dst);

    // save segment images for diagnostics
    for (unsigned int segIndex = 0; segIndex < segs.size(); ++segIndex)
    {
        QString tempFilename = "imgproc-result" + QString::number(segIndex) + ".png";
        imwrite(tempFilename.toStdString().c_str(), segs.at(segIndex));
    }


//    for (unsigned int i = 0; i < contours.size(); ++i) {
//        QString tempFilename = "imgproc-result" + QString::number(i);
//        result.push_back(QImage(tempFilename));
//    }

//    Intermediate inter;
//    inter.image = input;
//    inter.label = "input";
//    m_intermediates.append(inter);

    return segments;
}

QVector<Intermediate> WatershedImagePipeline::getIntermediates()
{
    return m_intermediates;
}


/// Mainly for testing/debugging
ImagePipeline::segments_t WatershedImagePipeline::runWithFile(const QString filepath)
{
    QImage input(filepath);
    return run(input);
}
