/// @author Sam Kenyon <sam@metadevo.com>
#include <string>
#include <vector>
#include <QDebug>
#include <QString>
#include <g3log/g3log.hpp>
#include "mat.h"

#include "MatFile.hpp"

MatFile::MatFile(RoiData& roiData) :
    m_roiData(roiData)
{
}

MatFile::~MatFile()
{
    LOG(DBUG) << "Destroying...";
    close();
}

bool MatFile::save(const QString& sessionKey, const std::string& filepath)
{
    try
    {
        m_sessionKey = sessionKey;
        m_filepath = filepath;
        qDebug() << "matlab Saving!";
        LOG(INFO) << "Saving file " << m_filepath;
        m_mf = matOpen(m_filepath.c_str(), "w");
        if (m_mf == nullptr) {
            LOG(WARNING) << "Error opening file " << m_filepath;
            return false;
        }
        return saveRois();
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
}

bool MatFile::load(const std::string& filepath)
{
    try
    {
        m_filepath = filepath;
        if (open()) {
            if(examine()) {
                return loadRois();
            }
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
    return false;
}

bool MatFile::open()
{
    try
    {
        LOG(INFO) << "Opening file " << m_filepath;
        m_mf = matOpen(m_filepath.c_str(), "r");
        if (m_mf == nullptr) {
            LOG(WARNING) << "Error opening file " << m_filepath;
            return false;
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
    return true;
}

bool MatFile::close()
{
    try
    {
        if (m_mf) {
            if (matClose(m_mf) != 0) {
                LOG(WARNING) << "Error closing file " << m_filepath;
                return false;
            }
            m_mf = nullptr;
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
    return true;
}

bool MatFile::examine()
{
    bool foundToplevel = false;
    int num;
    char** nameList = matGetDir(m_mf, &num);
    if (nameList != nullptr) {
        if (num > 0) {
            m_topArrayName = nameList[0];
            for (int i = 0; i < num; ++i) {
                LOG(INFO) << "Array found: " << nameList[i];
            }
        }
        else {
            LOG(WARNING) << "Zero arrays found.";
        }
        mxFree(nameList);
    } else {
        LOG(WARNING) << "Error finding arrays.";
    }
    if (m_topArrayName != m_defaultToplevel) {
        LOG(WARNING) << "First array is not named \"" << m_defaultToplevel << "\". Attempting to load anyway.";
    } else {
        foundToplevel = true;
    }

    // Matlab API requires closing and reopening between some of these functions
    if (!close()) {
        return false;
    }
    if (!open()) {
        return false;
    }
    LOG(INFO) << "Examining headers for " << m_topArrayName;
    mxArray *topArray;
    topArray = matGetVariableInfo(m_mf, m_topArrayName.c_str());
    if (topArray == nullptr) {
        LOG(WARNING) << "Error reading array.";
        return false;
    }
    m_numElements = examineArray(topArray);

    mxDestroyArray(topArray);
    if (!close()) {
        return false;
    }

    if (!foundToplevel && !m_toplevelIsField) {
        LOG(WARNING) << "Error: Could not find " << m_defaultToplevel;
        return false;
    } else {
        return true;
    }
}

///@return number of elements
unsigned long long MatFile::examineArray(const mxArray* array, unsigned int tabs)
{
    std::string tab(tabs, '\t');
    // Look at header
    //bool isGlobal = mxIsFromGlobalWS(array);
    unsigned long long numDims = mxGetNumberOfDimensions(array);
    const mwSize* dims = mxGetDimensions(array);
    unsigned long long numElements = mxGetNumberOfElements(array);
    mxClassID classId = mxGetClassID(array);
    const char* className = mxGetClassName(array);

    // Log information
    //LOG(INFO) << "Was a " << (isGlobal ? "global" : "local") << " variable when saved.";
    LOG(INFO) << tab << "Number of dimensions: " << numDims;
    for (mwSize c = 0; c < numDims; ++c) {
        LOG(INFO) << tab << "Dim [" << c << "]: " << dims[c];
    }
    LOG(INFO) << tab << "Total number of elements: " << numElements;
    LOG(INFO) << tab<< "Matlab class ID: "<< classId << " (" << className << ")";
    if (mxIsSparse(array)) {
        LOG(INFO) << tab<< "Is sparse.";
    }

    // If it's a struct, get some more info
    if (classId == mxSTRUCT_CLASS) {
        int number_of_fields = mxGetNumberOfFields(array);
        LOG(INFO) << tab << "Number of fields in struct: " << number_of_fields;

        LOG(INFO) << "Fields (first element):";
        const char *field_name;
        for (int field_index = 0; field_index < number_of_fields; ++field_index) {
            field_name = mxGetFieldNameByNumber(array, field_index);
            LOG(INFO) << "\t" << tab << field_name;
            mxArray* fieldArray = mxGetFieldByNumber(array, 0, field_index);
            if (fieldArray == nullptr) {
                LOG(WARNING) << "\t" << tab << "Error reading array.";
            } else {
                if (field_name == m_defaultToplevel) {
                    numElements = mxGetNumberOfElements(fieldArray);
                    m_toplevelIsField = true;
                    LOG(INFO) << tab << "Updated total number of elements: " << numElements;
                }
                examineArray(fieldArray, 2);
            }
            //NOTE: "Do NOT call mxDestroyArray on an mxArray returned by the mxGetFieldByNumber function."
        }
    }
    return numElements;
}


bool MatFile::loadRois()
{
    // Matlab API requires closing and reopening between some of these functions
    if (!close()) {
        return false;
    }
    if (!open()) {
        return false;
    }

    LOG(INFO) << "Reading data from " << m_topArrayName;
    mxArray* metaArray = nullptr;
    mxArray* topArray = nullptr;

    if (m_toplevelIsField) {
        LOG(INFO) << "Getting " << m_defaultToplevel << " out of " << m_topArrayName;
        metaArray = matGetVariable(m_mf, m_topArrayName.c_str());
        if (metaArray == nullptr) {
            LOG(WARNING) << "Error reading top level field array.";
            return false;
        }
        topArray = mxGetField(metaArray, 0, m_defaultToplevel.c_str());
    } else {
        topArray = matGetVariable(m_mf, m_topArrayName.c_str());
    }
    if (topArray == nullptr) {
        LOG(WARNING) << "Error reading array.";
        return false;
    }

    LOG(INFO) << "Reading each element...";
    std::shared_ptr<RoiFields> newRoi(nullptr);
    mxArray* fieldArray;
    mwIndex index = 0;
    for (; index < m_numElements; index++)
    {
        newRoi = std::make_shared<RoiFields>();

        fieldArray = mxGetField(topArray, index, "name");
        newRoi->name = QString::fromStdString(loadString(fieldArray));
        LOG(DBUG) << "name: " << newRoi->name.toStdString();

        fieldArray = mxGetField(topArray, index, "linked_name");
        newRoi->linkedName = QString::fromStdString(loadString(fieldArray));
        //LOG(DBUG) << "linked_name: " << newRoi->linkedName.toStdString();

        fieldArray = mxGetField(topArray, index, "linked_session");
        newRoi->linkedSession = QString::fromStdString(loadString(fieldArray));
        //LOG(DBUG) << "linked_session:" << newRoi->linkedSession.toStdString();

        fieldArray = mxGetField(topArray, index, "centroid");
        newRoi->centroid = loadCoordinate(fieldArray);
        //LOG(DBUG) << "centroid: " << newRoi->centroid.x << "," << newRoi->centroid.y << "," << newRoi->centroid.z;

        fieldArray = mxGetField(topArray, index, "roi");
        loadMask(fieldArray, newRoi->mask);
        //LOG(DBUG) << "roi vector size:" << newRoi->mask.size();

        // optional field
        fieldArray = mxGetField(topArray, index, "flag");
        if (fieldArray != nullptr) {
            bool* flag = mxGetLogicals(fieldArray);
            if (flag) {
                newRoi->flagged = *flag;
            }
        }

        // Insert it into the application's data manager
        m_roiData.addNewRoi(newRoi->name, newRoi);
    }
    LOG(INFO) << "Loaded " << index << " elements";

    if (metaArray) {
        mxDestroyArray(metaArray);
    } else {
        mxDestroyArray(topArray);
    }

    if (!close()) {
        return false;
    }
    return true;
}

/// Tries to get/convert a string out of the given field
/// @returns A string which is empty if the field was empty (or there was an error)
std::string MatFile::loadString(const mxArray* array)
{
    if (array == nullptr) {
        LOG(WARNING) << "Error reading array.";
        return "";
    }
    std::string ret;
    mxClassID classId = mxGetClassID(array);
    //LOG(DBUG) << "class:" << mxGetClassName(array);
    switch (classId) {
        case mxCHAR_CLASS:
            {
                // Allocate enough memory to hold the converted string
                mwSize buflen = mxGetNumberOfElements(array) + 1;
                char* buf = static_cast<char*>(mxCalloc(buflen, sizeof(char)));

                // Copy the string data from array and place it into buf
                if (mxGetString(array, buf, buflen) != 0) {
                    LOG(WARNING) << "Could not convert string data.";
                } else {
                    ret = std::string(buf);
                }
            }
            break;
        case mxDOUBLE_CLASS:
            {
                double* numericName = mxGetDoubles(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxSINGLE_CLASS:
            {
                float* numericName = mxGetSingles(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT8_CLASS:
            {
                mxInt8* numericName = mxGetInt8s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT8_CLASS:
            {
                mxUint8* numericName = mxGetUint8s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT16_CLASS:
            {
                mxInt16* numericName = mxGetInt16s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT16_CLASS:
            {
                mxUint16* numericName = mxGetUint16s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT32_CLASS:
            {
                mxInt32* numericName = mxGetInt32s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT32_CLASS:
            {
                mxUint32* numericName = mxGetUint32s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT64_CLASS:
        {
            mxInt64* numericName = mxGetInt64s(array);
            if (numericName) {
                ret = QString::number(*numericName).toStdString();
            }
        }
        break;
        case mxUINT64_CLASS:
            {
                mxUint64* numericName = mxGetUint64s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        default:
            LOG(WARNING) << "Unknown data type.";
            break;
    }
    return ret;
}

VoxelCoordinate MatFile::loadCoordinate(const mxArray* array)
{
    VoxelCoordinate v;
    if ((array != nullptr) && (mxGetNumberOfElements(array) >= 3))
    {
        mxClassID classId = mxGetClassID(array);
        int x = 0, y = 0, z = 0;
        switch (classId) {
            case mxINT16_CLASS:
                {
                    mxInt16* numPtr = mxGetInt16s(array);
                    x = static_cast<int>(numPtr[0]);
                    y = static_cast<int>(numPtr[1]);
                    z = static_cast<int>(numPtr[2]);
                }
                break;
            case mxUINT16_CLASS:
                {
                    mxUint16* numPtr = mxGetUint16s(array);
                    x = static_cast<int>(numPtr[0]);
                    y = static_cast<int>(numPtr[1]);
                    z = static_cast<int>(numPtr[2]);
                }
                break;
            case mxSINGLE_CLASS:
                {
                    float* realPtr = mxGetSingles(array);
                    x = static_cast<int>(realPtr[0]);
                    y = static_cast<int>(realPtr[1]);
                    z = static_cast<int>(realPtr[2]);
                }
                break;
            case mxDOUBLE_CLASS:
                {
                    double* realPtr = mxGetDoubles(array);
                    x = static_cast<int>(realPtr[0]);
                    y = static_cast<int>(realPtr[1]);
                    z = static_cast<int>(realPtr[2]);
                }
                break;
            default: LOG(WARNING) << "Wrong number type."; break;
        }

        // For some reason y is first and x is second in the data
        v = VoxelCoordinate(y, x, z);
    } else {
        LOG(WARNING) << "Not enough elements.";
    }
    return v;
}

void MatFile::loadMask(const mxArray* array, QVector<VoxelCoordinate>& mask)
{
    if (array != nullptr)
    {
        const mwSize* dims = mxGetDimensions(array);
        unsigned long long numDims = mxGetNumberOfDimensions(array);

        bool dimsOk = true;
        if (numDims != 2) {
            LOG(WARNING) << "Wrong dimensions.";
            bool dimsOk = false;
        }
        if ((dims[1] != 3) || (mxGetNumberOfElements(array) != (dims[0] * dims[1]))) {
            LOG(WARNING) << "Wrong dimension size.";
            bool dimsOk = false;
        }
        if (dims[0] == 0) {
            LOG(INFO) << "Mask is empty";
            bool dimsOk = false;
        }
        if (!dimsOk) {
            return;
        }

        // for each coordinate
        mxClassID classId = mxGetClassID(array);
        double* realPtr = mxGetPr(array);
        mxUint16* u16Ptr;
        int x = 0, y = 0, z = 0; // signed because we allow negative coords
        for (mwSize i = 0; i < dims[0]; ++i)
        {
            switch (classId) {
                case mxUINT16_CLASS:
                    u16Ptr = mxGetUint16s(array);
                    x = static_cast<unsigned int>(u16Ptr[i]);
                    y = static_cast<unsigned int>(u16Ptr[dims[0] + i]);
                    z = static_cast<unsigned int>(u16Ptr[dims[0] * 2 + i]);
                    break;
                case mxDOUBLE_CLASS:
                    realPtr = mxGetDoubles(array);
                    x = static_cast<int>(realPtr[i]);
                    y = static_cast<int>(realPtr[dims[0] + i]);
                    z = static_cast<int>(realPtr[dims[0] * 2 + i]);
//                    if (x < 0 || y < 0 || z < 0) {
//                        LOG(DBUG) << "Negative coordinate: (" << x << "," << y << "," << z << ")";
//                    }
                    break;
                default: LOG(WARNING) << "Wrong number type."; break;
            }

            // For some reason y is first and x is second in the data
            VoxelCoordinate v(y, x, z);
            mask.push_back(v);
        }   
    }
    else
    {
        LOG(WARNING) << "Array empty.";
    }
    return;
}

bool MatFile::saveRois()
{
    QList<RoiData::roi_t> roiList = m_roiData.roisAtSession(m_sessionKey);
    m_topArrayName = "ROI_list";
    mxArray *topArray;
    mwSize nDims = 2;
    mwSize dims[2];
    dims[0] = 1;
    dims[1] = static_cast<mwSize>(roiList.size());
    const int numFields = 8;
    const char* fieldnames[numFields] = {"roi", "name", "centroid", "flag", "confirmed", "candidates", "linked_name", "linked_session"};
    topArray = mxCreateStructArray(nDims, dims, numFields, fieldnames);
    if (topArray == nullptr) {
        LOG(WARNING) << "Error reading array.";
        return false;
    }

    mxArray* tmp;
    mwSize centroidDims[2];
    centroidDims[0] = 1;
    centroidDims[1] = 3;
    mwSize singleDims[2];
    singleDims[0] = 1;
    singleDims[1] = 1;
    unsigned int index = 0;
    for (; index < dims[1]; index++)
    {
        //LOG(DBUG) << "save name [" << index << "]: " << roiList.at(index)->name.toStdString();

        // the string fields
        mxSetField(topArray, index, "name", mxCreateString(roiList.at(index)->name.toUtf8()));
        mxSetField(topArray, index, "linked_name", mxCreateString(roiList.at(index)->linkedName.toUtf8()));
        mxSetField(topArray, index, "linked_session", mxCreateString(roiList.at(index)->linkedSession.toUtf8()));

        tmp = mxCreateLogicalArray(2, singleDims);
        mxLogical logicalData[1] = {static_cast<mxLogical>(roiList.at(index)->flagged)};
        memcpy(mxGetData(tmp), logicalData, sizeof(logicalData));
        mxSetField(topArray, index, "flag", tmp);

        ///@todo how to use the confirmed field
        //mxSetField(topArray, index, "confirmed", mxCreateString(roiList.at(index)->confirmed));

        // centroid
        // y and x are out of order to match the format we've been getting for import
        double data[3] = {static_cast<double>(roiList.at(index)->centroid.y),
                            static_cast<double>(roiList.at(index)->centroid.x),
                            static_cast<double>(roiList.at(index)->centroid.z)};
        tmp = mxCreateNumericArray(2, centroidDims, mxDOUBLE_CLASS, mxREAL); // note we do not need to free this manually
        memcpy(mxGetData(tmp), data, sizeof(data));
        mxSetField(topArray, index, "centroid", tmp);

        // fill in the masks vector
        QVector<VoxelCoordinate>& coords = roiList.at(index)->mask;
        m_roiData.maskImageToVector(coords, roiList.at(index)->masks);

        // copy that into the matlab array
        mwSize maskDims[2];
        maskDims[0] = static_cast<mwSize>(coords.size());;
        if (coords.size() == 0) {
            LOG(WARNING) << roiList.at(index)->name.toStdString() << ": mask is empty";
        }
        maskDims[1] = 3;
//        tmp = mxCreateNumericArray(2, maskDims, mxUINT16_CLASS, mxREAL); // note we do not need to free this manually
//        mxUint16* tmpData = mxGetUint16s(tmp);
//        int si = 0;
//        for (mwSize i = 0; i < maskDims[0]; ++i)
//        {
//            si = static_cast<int>(i);
//            // y and x are out of order to match the format we've been getting for import
//            tmpData[i] = static_cast<mxUint16>(coords[si].y);
//            tmpData[maskDims[0] + i] = static_cast<mxUint16>(coords[si].x);
//            tmpData[maskDims[0] * 2 + i] = static_cast<mxUint16>(coords[si].z);
//        }
        tmp = mxCreateNumericArray(2, maskDims, mxDOUBLE_CLASS, mxREAL); // note we do not need to free this manually
        double* tmpData = mxGetDoubles(tmp);
        int si = 0;
        for (mwSize i = 0; i < maskDims[0]; ++i)
        {
            si = static_cast<int>(i);
            // y and x are out of order to match the format we've been getting for import
            tmpData[i] = static_cast<double>(coords[si].y);
            tmpData[maskDims[0] + i] = static_cast<double>(coords[si].x);
            tmpData[maskDims[0] * 2 + i] = static_cast<double>(coords[si].z);
        }
        mxSetField(topArray, index, "roi", tmp);

    }
    LOG(INFO) << "Created " << index << " elements";

    int status = matPutVariable(m_mf, "ROI_list", topArray);
    if (status != 0) {
        LOG(WARNING) << "Put failed.";
        return false;
    }

    LOG(DBUG) << "Destroying array...";
    mxDestroyArray(topArray);

    if (!close()) {
        return false;
    }
    return true;
}
