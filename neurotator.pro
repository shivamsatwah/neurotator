QT += qml quick
CONFIG += c++11
QT += quickcontrols2

QMAKE_CXXFLAGS_RELEASE  += -O2
QMAKE_CXXFLAGS_DEBUG  += -D_DEBUG

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Neurotator specific defines
DEFINES += MATLAB_SUPPORT

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/gui

HEADERS += \
    third-party/libmat/mat.h \
    third-party/libmat/matrix.h \
    third-party/libmat/tmwtypes.h \
    include/MatFile.hpp \
    include/PersistenceInterface.hpp \
    include/Project.hpp \
    include/AppSettings.hpp \
    include/HyperstackTiff.hpp \
    include/Hyperstack.hpp \
    include/RoiDataModel.hpp \
    include/RoiData.hpp \
    include/ImagePipeline.hpp \
    include/WatershedImagePipeline.hpp \
    include/ImageAdjustments.hpp \
    include/SearchResults.hpp \
    gui/ApplicationData.hpp \
    gui/SliceImage.hpp \
    gui/SliceOverlay.hpp

SOURCES += \
    gui/main.cpp \
    src/Project.cpp \
    gui/ApplicationData.cpp \
    gui/SliceImage.cpp \
    src/HyperstackTiff.cpp \
    src/Hyperstack.cpp \
    src/AppSettings.cpp \
    src/RoiDataModel.cpp \
    src/RoiData.cpp \
    gui/SliceOverlay.cpp \
    src/WatershedImagePipeline.cpp \
    src/ImageAdjustments.cpp

contains(DEFINES, MATLAB_SUPPORT) {
    SOURCES += src/MatFile.cpp
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += $$PWD/third-party/libmat
DEPENDPATH += $$PWD/third-party/libmat

contains(DEFINES, MATLAB_SUPPORT) {
    LIBS += -L$$PWD/third-party/libmat/ -llibmat
    LIBS += -L$$PWD/third-party/libmat/ -llibmx

    #win32:CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/libmat/ -llibmat
    #else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/libmat/ -llibmatd
    #win32:CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/libmat/ -llibmx
    #Commented out because Matlab does not appear to provide the debug version of their lib
    #else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/libmat/ -llibmxd
}

#message(Qt is installed in $$[QT_INSTALL_PREFIX])
#INCLUDEPATH += $$[QT_INSTALL_PREFIX]/../Src/qtimageformats/src/3rdparty/libtiff/libtiff
#DEPENDPATH += $$[QT_INSTALL_PREFIX]/../Src/qtimageformats/src/3rdparty/libtiff/libtiff
#CONFIG(release, debug|release): LIBS += -L$$[QT_INSTALL_PREFIX]/plugins/imageformats -lqtiff
#else:CONFIG(debug, debug|release): LIBS += -L$$[QT_INSTALL_PREFIX]/plugins/imageformats -lqtiffd

INCLUDEPATH += $$PWD/third-party/tiff/libtiff
DEPENDPATH += $$PWD/third-party/tiff/libtiff
CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/tiff/libtiff -llibtiff
else:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/tiff/libtiff -llibtiffd

INCLUDEPATH += $$PWD/third-party/g3log/src
INCLUDEPATH += $$PWD/third-party/g3log/build/include
DEPENDPATH += $$PWD/third-party/g3log/src
DEPENDPATH += $$PWD/third-party/g3log/build/include
CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/g3log/build -lg3logger
else:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/g3log/build -lg3loggerd

INCLUDEPATH += $$PWD/third-party/opencv/include
DEPENDPATH += $$PWD/third-party/opencv/include
CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/opencv -lopencv_world400
else:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/opencv -lopencv_world400d

DISTFILES += \
    gui/qml/main.qml \
    gui/qml/SliceEdit.qml \
    gui/qml/SliceImageFrame.qml \
    gui/qml/ProjectWizard.qml \
    gui/qml/SmallButton.qml \
    gui/qml/HorDivider.qml \
    gui/qml/RoiTable.qml \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_black_18dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_black_24dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_white_18dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_white_24dp.png \
    gui/qml/RoiTableCell.qml \
    gui/qml/RoiTableHeader.qml \
    gui/qml/TinyColorPicker.qml \
    gui/qml/TinySlider.qml \
    gui/qml/SmallSpinBox.qml \
    gui/qml/SmallMenu.qml \
    gui/qml/SliceWindow.qml \
    gui/qml/ProjectWizard.qml \
    gui/qml/FindRoi.qml \
    gui/qml/ToolStatePanel.qml


