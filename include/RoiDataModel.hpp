/// @author Sam Kenyon <sam@metadevo.com>
#ifndef ROIDATAMODEL_HPP
#define ROIDATAMODEL_HPP

#include <QAbstractListModel>
#include <QStringList>
#include <QDebug>

#include "RoiData.hpp"

enum SortChoice
{
    UNSORTED,
    LINKS,
    NAME
};

/// Represents annotations for a single ROI across sessions
/// Typically this maps visually to a row of cells, and each column is a set of linked ROIs
class RoiColumns
{
public:
    RoiColumns(QStringList);

    QString atColumn(const int col) const;
    QStringList linkedRois() const;
    void setLinkedRois(QStringList roiList);

private:
    QStringList m_linkedRois;
};

///@todo split into two classes?
/// Region of Interest data model to interface ROI data with QML
class RoiDataModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum CellRoles {
        NormalRole = Qt::UserRole + 1,
        SpecialRole
    };
    RoiDataModel() {}
    RoiDataModel(RoiData* roiData, bool linkedColumns = false, QObject *parent = nullptr);
    virtual ~RoiDataModel();

    //void beginReset() { beginResetModel(); }
    //void endReset() { endResetModel(); }
    //void setColumnCount(int colCount) { m_colCount = colCount; }
    void addColumn(QString keyname);
    void removeColumn(QString keyname);
    void clearAllColumns();
    void clearAllRows();
    void addRow(const RoiColumns &row);
    void regenerateRows();
    void regenerateColumns();
    void resortColumnNames(const QString refSessionKey);
    void sortRows();
    void signalReset();
    //void insertRow(const int refIndex);
    void refreshRow(const int refIndex);
    void setSorted(const SortChoice sortChoice);
    void setSortColumn(const int column) { m_sortColumn = column; }

    int roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const;
    int linkedRefIndexAt(const session_key_t sessionKey, RoiData::roi_t roi) const;

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    Q_INVOKABLE QStringList columnNames() const { return m_colNames; }
    Q_INVOKABLE QString columnNameAt(const int index) const;
    Q_INVOKABLE int getColumnIndex(QString sessionKey) const { return m_colNames.indexOf(sessionKey); }
    Q_INVOKABLE QVariant dataAt(const int row, int col) const;
    Q_INVOKABLE QVariant dataAtSingleCol(QString keyname, const int row) const;
    Q_INVOKABLE QVariant rowCountAtCol(QString keyname) const;
    Q_INVOKABLE int refLinkCount(const int row) const;
    Q_INVOKABLE void regen();
    Q_INVOKABLE bool isSelected(const QString& sessionKey, const QString& roiKey) const {
        auto roi = m_roiData->fieldsAt(sessionKey, roiKey);
        if (roi && roi->selected) {
            return true;
        } else {
            return false;
        }
    }
    Q_INVOKABLE bool isLinked(const QString& sessionKey, const QString& roiKey) const {
        auto roi = m_roiData->fieldsAt(sessionKey, roiKey);
        if (roi && (roi->linkedName != ""  || !roi->linkedIn.empty())) {
            return true;
        } else {
            return false;
        }
    }
    Q_INVOKABLE bool isFlagged(const QString& sessionKey, const QString& roiKey) const {
        auto roi = m_roiData->fieldsAt(sessionKey, roiKey);
        if (roi && roi->flagged) {
            return true;
        } else {
            return false;
        }
    }
    Q_INVOKABLE QString getLinkName(const QString& sessionKey, const QString& roiKey) {
        auto roi = m_roiData->fieldsAt(sessionKey, roiKey);
        return (roi) ? roi->linkedName : "";
    }
    Q_INVOKABLE QString getLinkSessionText(const QString& sessionKey, const QString& roiKey) {
        auto roi = m_roiData->fieldsAt(sessionKey, roiKey);
        QString sess = (roi) ? roi->linkedSession : "";
        if (sess != "") {
            if (sess == m_roiData->refSessionName()) {
                sess.prepend("(ref) ");
            } else {
                sess.prepend("(non-ref) ");
            }
        }
        return sess;
    }

protected:
    QHash<int, QByteArray> roleNames() const;
    void regenerateBlankRows(QString keyname);

private:
    QList<RoiColumns> m_rows;
    QStringList m_colNames;
    RoiData* m_roiData;
    bool m_linkedColumns;
    SortChoice m_sorted = UNSORTED;
    bool m_sortReverse = false;
    int m_sortColumn = 0; // 0 means reference session
    QHash<int, int> m_sortedIndexMap;
    QHash<int, int> m_sortedInverseMap;
};

#endif // ROIDATAMODEL_HPP
