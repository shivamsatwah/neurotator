/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICESTACKTIFF_HPP
#define SLICESTACKTIFF_HPP

#include <QImage>
#include <tiffio.h>

#include "Hyperstack.hpp"

struct ImageJMetadata
{
    bool imageJ = false;
    QString version;
    bool hyperstack = false;
    bool modeComposite = false;
    unsigned int images = 0;
    unsigned int slices = 0;
    unsigned int frames = 0;
    unsigned int channels = 0;
};


/// Handle stacks and hyperstacks stored in TIFF files including ImageJ TIFFs.
/// Indices are all zero-based in this class.
class HyperstackTiff : public Hyperstack
{
public:
    HyperstackTiff();
    virtual ~HyperstackTiff();

    virtual bool ready() const { return m_ready; }
    virtual bool load(const QString filePath);
    virtual unsigned int numImages() const { return m_numImages; }
    virtual HyperstackSize size() const { return m_size; }
    virtual QString name() const { return m_name; }

    // state (selected slice) interface
    void loadSlice(const HyperstackIndex index);
    void loadNextZ();
    void loadPrevZ();
    virtual QImage imageBuffer();
    QColor maxColorValue() const { return QColor(m_maxColorValue); }
    HyperstackIndex index() const { return m_index; }
    void addClient() { m_clients++; }
    void removeClient() { m_clients--; }
    unsigned int clients() const { return m_clients; }

private:
    void cleanup();
    static void cleanupBufferHandler(void *info);
    bool loadRasterBuffer(TIFF* tif);
    bool loadMetadata(TIFF* tif);
    bool loadCache();

    static unsigned int countPages(TIFF* tif);
    static ImageJMetadata readImageJMetadata(TIFF* tif);
    static int getMetadataInt(QString name, QStringList metatags);
    static QString getMetadataStr(QString name, QStringList metatags);
    static void isolateChannelRGB(uint32* buffer, const RGBA_Channel channel, const unsigned int width, const unsigned int height);
    static uchar mergeBuffersRGB(uint32** buffers, const unsigned int size);
    static void mergeBuffersRGBQuick(uint32** buffers, const unsigned int size);
    static uchar mergeBuffersRGBA(uint32** buffers, unsigned int pixelSize);

    bool m_initted = false;
    bool m_ready = false;
    unsigned int m_numImages = 0; // aka number of TIFF pages
    bool m_hyperstack = false;
    HyperstackSize m_size;
    TIFF* m_tif = nullptr;
    QString m_name;
    HyperstackIndex m_index;
    QImage m_currImage;
    QRgb m_maxColorValue;
    uchar m_maxColor = 0;
    unsigned int m_maxHeapImages = 1;
    unsigned int m_clients = 0;
};

#endif // SLICESTACKTIFF_HPP
