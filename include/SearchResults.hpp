/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SEARCHRESULTS_HPP
#define SEARCHRESULTS_HPP

#include <QObject>
#include <QString>

class SearchResult : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName CONSTANT)
    Q_PROPERTY(QString session READ getSession CONSTANT)
    Q_PROPERTY(bool hasStack READ getHasStack CONSTANT)

public:
    SearchResult() {}
    SearchResult(QString name, QString session, bool hasStack) :
        name(name), session(session), hasStack(hasStack) {}
    QString getName() { return name; }
    QString getSession() { return session; }
    bool getHasStack() { return hasStack; }

    QString name;
    QString session;
    bool hasStack;
};

#endif // SEARCHRESULTS_HPP
