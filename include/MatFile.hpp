/// @author Sam Kenyon <sam@metadevo.com>
#ifndef MATFILE_HPP
#define MATFILE_HPP

#include <string>
#include "mat.h"

#include "PersistenceInterface.hpp"
#include "RoiData.hpp"

/// Load/Save Matlab data files
class MatFile : public PersistenceInterface
{
public:
    explicit MatFile(RoiData& roiData);
    virtual ~MatFile();
    virtual bool save(const QString& sessionKey, const std::string& filepath);
    virtual bool load(const std::string& filepath);

private:
    bool open();
    bool close();
    bool examine();
    unsigned long long examineArray(const mxArray* array, const unsigned int tabs = 0);
    bool loadRois();
    bool saveRois();
    std::string loadString(const mxArray* array);
    VoxelCoordinate loadCoordinate(const mxArray* array);
    void loadMask(const mxArray* array, QVector<VoxelCoordinate>& mask);

    RoiData& m_roiData;
    QString m_sessionKey;
    std::string m_filepath;
    MATFile* m_mf = nullptr;
    std::string m_topArrayName;
    std::string m_defaultToplevel = "ROI_list";
    unsigned long long m_numElements = 0;
    bool m_toplevelIsField = false;
};

#endif // MATFILE_HPP

