/// @author Sam Kenyon <sam@metadevo.com>
#ifndef IMAGEADJUSTMENTS_HPP
#define IMAGEADJUSTMENTS_HPP

#include <QImage>
#include <QVector>

class ImageAdjustments
{
public:
    ImageAdjustments() = delete;

    static QImage equalizeHistogram(QImage input);

private:
};

#endif // IMAGEADJUSTMENTS_HPP
