/// @author Sam Kenyon <sam@metadevo.com>
#ifndef PROJECT_HPP
#define PROJECT_HPP

#include <memory>
#include <vector>
#include <QHash>
#include <QQmlEngine>
#include <QObject>
#include <QJsonObject>
#include <QImage>
#include <QString>
#include <QUrl>
#include <QVector>

#include "Hyperstack.hpp"
#include "RoiData.hpp"
#include "RoiDataModel.hpp"

class HyperstackTiff; ///@todo use base class
class QFile;

using stack_ptr_t = HyperstackTiff*;
using stack_index_t = unsigned long long;

struct StackWindow {
    stack_index_t stackIndex = 0;
    HyperstackIndex coords;
    int zoomFactor = 100;
    float offsetX = 0.0;
    float offsetY = 0.0;
    float width = 0;
    int windowX = 200;
    int windowY = 10;
    bool visible = true;
};

struct RoiFile {
    QString savePath;
    QString sessionKey;
};

class Project
{
public:
    Project();
    explicit Project(const QUrl fileUrl);
    ~Project();

    void saveProjectAs(const QUrl fileUrl);
    void loadStack(const QUrl fileUrl);
    void loadStack(const QString& pathStr);
    void unloadStack(const QString& name);
    void loadRoiData(const QUrl fileUrl);
    void loadRoiData(const QString& pathStr);
    void saveAllRoiFiles();
    bool saveConfig() const;

    void saveStackZ(const stack_index_t stack, const unsigned int z);
    void saveStackZoom(const stack_index_t stack, const int zoomFactor);
    void saveStackOffset(const stack_index_t stack, const float offsetX, const float offsetY);
    void saveStackWidth(const stack_index_t stack, const float width);
    void saveStackWindowPosition(const stack_index_t stack, const int x, const int y);
    //void saveStackWindowHidden(const stack_index_t windowIndex);

    QString name() const { return m_name; }
    void setName(const QString name);
    stack_ptr_t currentStack();
    bool findStack(const QString& filePath);
    RoiData* roiData() { return &m_roiData; }
    RoiDataModel* confirmedModel() { return &m_confirmedModel; }
    RoiDataModel* unconfirmedModel(const int index);
    std::shared_ptr<RoiDataModel> unconfirmedModelByName(const QString sessionName) { return m_unconfirmedModelNameMap[sessionName]; }
    RoiDataModel* lastUnconfirmedModel() { return m_unconfirmedModels.last().get(); }
    void resortModels(const QString refSessionKey);
    void resetModels(bool regenConfirmed = true);
    void resortConfirmedModel(const SortChoice sort);
    void resortConfirmedModelByName(const int column);
    void setCurrentStackWindow(const stack_index_t index);
    unsigned long long currentStackWindowIndex() { return m_currentStackWindow; }
    const StackWindow& getStackWindowConfig(stack_index_t stack) const { return m_stackWindows.at(stack); }
    QStringList stackNames();
    QList<QUrl> stackUrls();
    QList<QUrl> stackWindowUrls();
    QString referenceName() { return m_referenceName; }

private:
    bool loadConfig();
    void readMisc(const QJsonObject &json);
    void readStacks(const QJsonObject &json);
    void readWindowConfigs(const QJsonObject &json);
    void readRoiFiles(const QJsonObject &json);
    void write() const;
    void writeStacks() const;
    void writeWindowConfig() const;
    //void updateConfig() const;
    void updateStacksConfig() const;
    bool loadMatlabRoiFile(QString pathStr, QString keyName);
    bool loadCsvRoiFile(QString pathStr, QString keyName);
    void updateUnconfirmedNameMap();

    QUrl m_filePath;
    QString m_name;
    QVector<RoiFile> m_roiFiles;
    //unsigned int m_nextStackId = 1;
    unsigned int m_maxClientsPerStack = 1;
    std::vector<stack_ptr_t> m_stack;
    std::vector<StackWindow> m_stackWindows;
    stack_index_t m_currentStack = 0;
    stack_index_t m_currentStackWindow = 0;
    stack_index_t m_findResult = 0;
    RoiData m_roiData;
    RoiDataModel m_confirmedModel;
    QList<std::shared_ptr<RoiDataModel>> m_unconfirmedModels;
    QHash<QString, std::shared_ptr<RoiDataModel>> m_unconfirmedModelNameMap;
    //mutable QFile* m_saveFile = nullptr;
    mutable QJsonObject m_jsonObject;
    bool m_openingProject = false;
    QString m_referenceName;
};

#endif // PROJECT_HPP
