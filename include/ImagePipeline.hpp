/// @author Sam Kenyon <sam@metadevo.com>
#ifndef IMAGEPIPELINE_HPP
#define IMAGEPIPELINE_HPP

#include <QImage>
#include <QVector>

#include "Hyperstack.hpp"

struct Intermediate
{
    QImage image;
    QString label;
};

class ImagePipeline
{
public:
    using segments_t = QVector<QVector<VoxelCoordinate>>;

    ImagePipeline() {}
    virtual ~ImagePipeline() {}

    virtual segments_t run(const QImage& input) = 0;
    virtual QVector<Intermediate> getIntermediates() { return QVector<Intermediate>(); }
};

#endif // IMAGEPIPELINE_HPP
