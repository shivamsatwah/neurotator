/// @author Sam Kenyon <sam@metadevo.com>
#ifndef APPSETTINGS_HPP
#define APPSETTINGS_HPP

#include <QList>
#include <QMap>
#include <QSettings>
#include <QStringList>
#include <QUrl>
#include <QVariant>

//enum AppSettingName
//{
//    DEFAULT_IMPORT_DIR,
//    DEFAULT_PROJECT,
//    WINDOW_SIZE_X,
//    WINDOW_SIZE_Y,
//    STACK_CACHE_SIZE,
//    MAX_HEAP
//};

class AppSettings
{
public:
    AppSettings(AppSettings&) = delete;
    AppSettings& operator=(AppSettings&) = delete;

    /// Singleton
    static AppSettings& instance() {
        static AppSettings instance;
        return instance;
    }

    bool find(const QString name) const { return m_settings.contains(name); }
    QVariant get(const QString name) const { return m_settings.value(name); }
    int maxRecentProjects() const;
    int numRecentProjects() const;
    QList<QUrl> recentProjects() const;
    QStringList recentProjectNames() const;
    QUrl defaultProjectDirectory();

    void set(const QString name, const QVariant value);
    void setRecentProject(const QString projectName, const QString projectPath);
    void updateRecentProjectName(const QString projectName, const QString projectPath);
    void clearRecentProjects();

private:
    AppSettings();
    bool recentProjectsContains(const QString projectPath);

    //QMap<QString, QVariant> m_settings;
    QSettings m_settings;
};

#endif // APPSETTINGS_HPP
