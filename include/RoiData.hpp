/// @author Sam Kenyon <sam@metadevo.com>
#ifndef ROIDATA_HPP
#define ROIDATA_HPP

#include <memory>
#include <vector>
#include <QDebug>
#include <QHash>
#include <QString>
#include <QVector>

#include "Hyperstack.hpp"

using session_key_t = QString;
using roi_key_t = QString;
using z_key_t = int;

class SearchResult;

struct RoiAddress
{
    QString sessionKey;
    QString roiKey;
};

struct RoiMask
{
    QRect extent;
    QImage mask;
    QImage maskOutline;
};

/// Data for a single 3D region of interest
struct RoiFields
{
    QString name;
    QString linkedName;
    QString linkedSession;
    QStringList candidates;
    bool confirmed = false;
    bool flagged = false;
    VoxelCoordinate centroid;
    QVector<VoxelCoordinate> mask;  // temporary, cleared after loading

    QHash<z_key_t, RoiMask> masks;
    QHash<session_key_t, std::weak_ptr<RoiFields>> linkedIn;
    bool fullyLinked = false; // for ref session, are all other sessions linked in
    bool selected = false;
    bool filtered = false;
};

struct LinkCount
{
    LinkCount(int i, int c) : index(i), count(c) {}
    int index = 0;
    int count = 0;
};

/// Manages regions of interest for multiple sessions
class RoiData
{
public:
    using roi_t = std::shared_ptr<RoiFields>;
    using session_t = QHash<roi_key_t, roi_t>;
    using maskTable_t = QHash<z_key_t, QList<roi_t>>;

    struct IndexedRoi
    {
        int index = 0;
        RoiData::roi_t roi;
    };

    RoiData();
    ~RoiData();
    bool addNewSession(session_key_t name);
    bool removeSession(session_key_t name);
    bool addNewRoi(roi_key_t name, roi_t fields);
    bool addNewRoi(session_key_t session, roi_key_t name, roi_t fields);
    bool renameRoi(session_key_t session, roi_key_t name, roi_key_t newName);
    bool deleteRoi(session_key_t session, roi_key_t roiKey);
    unsigned int deleteFlagged(const QString& sessionKey);
    unsigned int deleteFlagged();
    void setRefSession(const session_key_t& sessionKey);
    void clearRefSession();
    int modifyRoiLink(RoiAddress& src, RoiAddress& dest);
    bool removeRoiLink(RoiAddress& src);
    void refreshRefLinks();
    void setSelected(roi_t roi);
    bool modifyMask(RoiMask& mask, const int x, const int y);
    void mergeToFirst(session_key_t session, const QList<RoiData::roi_t>& rois);
    void split(session_key_t session, roi_t src, roi_t dest);

    unsigned int filterHighPassRadius(const QString& sessionKey, const int minRadius);
    unsigned int filterHighPassVolume(const QString& sessionKey, const int minVolume);
    unsigned int flagFiltered(const QString& sessionKey);
    unsigned int unflagAll(const QString& sessionKey);
    void setFlag(const QString& sessionKey, const QString& roiKey, bool flag);
    bool toggleFlag(const QString& sessionKey, const QString& roiKey);

    int sessionCount() const { return m_sessions.size(); }
    QList<QString> sessionNames() { return m_sessions.keys(); }
    session_key_t refSessionName() const { return m_refSession; }
    QString sessionNameAt(int index) { return m_sessions.keys().at(index); }
    int refSessionSize() const;
    bool refSessionExists() const { return m_refSession != ""; }
    QStringList traverseRefNames(const int rowIndex, const QStringList colNames) const;
    roi_t fieldsAt(session_key_t sessionKey, roi_key_t roiKey) const;
    QList<roi_t> roisAtSession(session_key_t sessionKey) const;
    QList<roi_t> roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const;
    int roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const;
    int linkedRefIndexAt(const session_key_t sessionKey, roi_t roi) const;
    QList<QString> columnNamesUnordered();
    std::vector<LinkCount> refLinkCounts();
    std::vector<IndexedRoi> indexedRoisAtSession(session_key_t sessionKey) const;
    std::vector<std::shared_ptr<SearchResult>> searchRois(const QString& sessionKey, QString namePattern);

    void createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks);
    void createMaskOutlines(QHash<z_key_t, RoiMask>& masks);
    void maskImageToVector(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks, bool append = false);
    void refreshRoi(session_key_t session, roi_t roi);

    static int radius(const QRect& rect) { return maxDim(rect) / 2; }
    static int maxDim(const QRect& rect) { return (rect.width() > rect.height()) ? rect.width() : rect.height(); }

private:    
    void removeRoiFromMaskTables(session_key_t session, roi_t roi);
    void refreshFullyLinked(roi_t roi);
    bool removeOldLink(const roi_key_t refRoiName, const session_key_t sessionName);

    session_key_t m_currSession;
    session_key_t m_refSession;
    roi_t m_prevSelection = nullptr;

    // the main container of ROI data for the whole application
    QHash<session_key_t, session_t> m_sessions;

    // for quick lookup of masks
    QHash<session_key_t, maskTable_t> m_maskTables;

    // settings that affect data interpretation
    bool m_originTopLeft = true;
};

#endif // ROIDATA_HPP
