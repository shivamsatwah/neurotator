/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QPainter>
#include <QUrl>
#include <g3log/g3log.hpp>

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "RoiData.hpp"
#include "HyperstackTiff.hpp"
#include "WatershedImagePipeline.hpp"

ApplicationData::ApplicationData()
{
    if (AppSettings::instance().find("ibase")) {
        m_iBase = static_cast<uchar>(AppSettings::instance().get("ibase").toInt());
    } else {
        AppSettings::instance().set("ibase", m_iBase);
    }
    LOG(INFO) << "index base: " << m_iBase;

    if (AppSettings::instance().find("command_history")) {
        m_maxHistory = AppSettings::instance().get("command_history").toInt();
    } else {
        AppSettings::instance().set("command_history", m_maxHistory);
    }

    //for test purposes

//    m_searchResults.append(new SearchResult("42", "foobar", false));
//    m_searchResults.append(new SearchResult("422", "foobar2", false));
//    m_searchResults.append(new SearchResult("4222", "foobar3", false));
}

ApplicationData::~ApplicationData()
{
}

//void ApplicationData::deleteSearchResults()
//{
//    foreach(SearchResult* s, m_searchResults) {
//        delete s;
//        s = nullptr;
//    }
//    m_searchResults.clear();
//}

void ApplicationData::setWindowTitle(const QString& title)
{
    m_windowTitle = title;
    emit windowTitleChanged(title);
}

void ApplicationData::setProjectName(const QString& name)
{
    m_project->setName(name);
    setWindowTitle(formatWindowTitle(name));
    commandIssued(QStringList{"Changed Project Name to", name}, false);
    emit projectNameChanged();
}

void ApplicationData::setProject(std::shared_ptr<Project> project)
{
    m_project = project;
    //engine.rootContext()->setContextProperty("confirmedModel", blankProject->confirmedModel());
    setWindowTitle(formatWindowTitle(project->name()));
    emit projectNameChanged();
}

void ApplicationData::setRefSession(const QString& sessionName)
{
    if (sessionName != refSession()) {
        LOG(INFO) << "Setting ref session.";
        m_project->roiData()->setRefSession(sessionName);
        m_project->roiData()->refreshRefLinks();

        m_project->confirmedModel()->resortColumnNames(sessionName);
        m_project->resortModels(sessionName);
        m_project->resetModels(true);

        emit refExistsChanged();
        emit refSessionChanged();

        emit roiModified();

        commandIssued(QStringList{"Set Ref Session.", sessionName}, false);
    }
}

void ApplicationData::sortByLink()
{
    m_project->resortConfirmedModel(SortChoice::LINKS);
    emit linkedRoiSelectedIndex(0);

    emit roiModified();
    commandIssued(QStringList{"Sort by link"}, false);
}

void ApplicationData::sortByName(const int column)
{
    m_project->resortConfirmedModelByName(column);
    emit linkedRoiSelectedIndex(0);

    emit roiModified();
    commandIssued(QStringList{"Sort by ROI name"}, false);
}

/// SliceWindow and SliceOverlay objects referring to the current project should be destroyed from QML,
/// preferably before calling this.
void ApplicationData::loadProject(QUrl url)
{
    try
    {
        LOG(DBUG) << "Loading project...";
        emit loadingNewProject();

        // this informs QML that these stack names are no longer valid
        QStringList names = m_project->stackNames();
        for (auto name : names) {
            emit removedSession(name);
        }

        // new project object (old one will self-destruct thus closing any open stack files)
        std::shared_ptr<Project> project = std::make_shared<Project>(url);
        setProject(project);

        if (project->referenceName() != "") {
            setRefSession(project->referenceName());
        }

        emit newModel();
        emit searchableSourcesChanged();

        // in case it hasn't been added yet to recents list in app settings
        AppSettings::instance().setRecentProject(project->name(), url.toLocalFile());

        commandIssued(QStringList{"Loaded Project", url.fileName()}, false);
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        commandFailure("Load project (" + url.toLocalFile() + ") failed");
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        commandFailure("Load project (" + url.toLocalFile() + ") failed");
    }
}

void ApplicationData::saveProjectAs(QUrl url)
{
    m_project->saveProjectAs(url);
    commandIssued(QStringList{"Saved Project", url.fileName()}, false);
}

void ApplicationData::loadStack(QUrl url)
{
    m_project->loadStack(url);
    emit searchableSourcesChanged();
    commandIssued(QStringList{"Loaded Stack", url.fileName()}, false);
}

/// Unloads a stack as well as associated ROI data
void ApplicationData::unloadStack(const QString& name)
{
    clearLinkState();

    if (name == refSession()) {
        m_project->roiData()->clearRefSession();
        m_project->confirmedModel()->resortColumnNames(name);
        m_project->resortModels(name);
        m_project->resetModels(true);
        emit refExistsChanged();
    }

    m_project->unloadStack(name);
    m_project->roiData()->refreshRefLinks();

    emit roiModified(); //signal overlays to update
    emit removedSession(name);
    emit searchableSourcesChanged();

    commandIssued(QStringList{"Unloaded Session", name}, false);
}

void ApplicationData::loadRoiData(QUrl url)
{
    m_project->loadRoiData(url);
    emit searchableSourcesChanged();
    commandIssued(QStringList{"Loaded ROI file", url.fileName()}, false);
}

QStringList ApplicationData::getSearchableSources()
{
    QStringList sources = m_project->confirmedModel()->columnNames();
    sources.push_front("All");
    return sources;
}

void ApplicationData::searchRois(const QString& sessionKey, QString namePattern)
{
    try
    {
        //m_searchResults.clear();
        std::vector<std::shared_ptr<SearchResult>> temp = m_searchResults;
        m_searchResults = m_project->roiData()->searchRois(sessionKey, namePattern);

        QStringList stackNames = m_project->stackNames();
        for (auto it : m_searchResults) {
            if (stackNames.contains(it->session)) {
                it->hasStack = true;
            }
        }

        emit searchResultsChanged();
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

/// @param selectInConfirmed: whether to auto select this in the confirmed table
bool ApplicationData::selectRoi(const QString& sessionKey, const QString& roiKey, bool selectInConfirmed)
{
    try
    {
        if (!m_project) {
            LOG(WARNING) << "Project is null!";
        }
        if (roiKey == "?") {
            return false;
        }

        auto roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi) {
            m_project->roiData()->setSelected(roi);

            if (selectInConfirmed && roi->linkedName != "") {
                //int index = m_project->roiData()->linkedRefIndexAt(sessionKey, roi);
                int index = m_project->confirmedModel()->linkedRefIndexAt(sessionKey, roi);
                if (index != -1) {
                    emit roiConfirmedSelected(index);
                }
            }

            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);

            // notify the slice widget(s) if any
            emit roiSelected(sessionKey, roiKey);

            return true;
        } else {
            return false;
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return false;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return false;
    }
}

/// Like selectRoi() but skips the widget signal
void ApplicationData::selectRoiFromSlice(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (!m_project) {
            LOG(WARNING) << "Project is null!";
        }
        bool emitted = false;
        auto roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi)
        {
            m_project->roiData()->setSelected(roi);

            if (sessionKey == m_project->roiData()->refSessionName()) {
                LOG(INFO) << "Searching confirmed model : roiIndexAt (" << roiKey.toStdString() << ")";
                int index = m_project->confirmedModel()->roiIndexAt(sessionKey, roiKey);
                if (index != -1) {
                    emit roiConfirmedSelected(index); // for confirmed table
                    emitted = true;
                }
            } else if (roi->linkedName != "") {
                LOG(INFO) << "Searching confirmed model : linkedRefIndexAt (" << roiKey.toStdString() << ") roi->linkedSession: " << roi->linkedSession.toStdString();
                int index = m_project->confirmedModel()->linkedRefIndexAt(sessionKey, roi);
                if (index != -1) {
                    emit roiConfirmedSelected(index); // for confirmed table
                    emitted = true;
                }
            }

            if (!emitted) {
                // try unconfirmed table
                LOG(INFO) << "Searching unconfirmed model (" << roiKey.toStdString() << ")";
                int index = m_project->unconfirmedModelByName(sessionKey)->roiIndexAt(sessionKey, roiKey);
                if (index != -1) {
                    emit roiSelectedIndex(sessionKey, index);
                    emitted = true;
                }
            }
        }
        if (!emitted) {
            commandFailure("Unknown key");
        } else {
            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return;
    }
}

void ApplicationData::selectRoiInSlice(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        auto roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi) {
            m_project->roiData()->setSelected(roi);

            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);

            // notify the slice widget(s) if any
            emit roiSelected(sessionKey, roiKey);
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

void ApplicationData::linkRoi(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (m_currLinkSource.roiKey == "") {
            linkRoiSrc(sessionKey, roiKey);
        } else {
            linkRoiDest(sessionKey, roiKey);
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return;
    }
}

void ApplicationData::linkRoiSrc(const QString& sessionKey, const QString& roiKey)
{
    LOG(INFO) << "Link ROI src: " << sessionKey.toStdString() << " : " << roiKey.toStdString();
    m_currLinkSource.sessionKey = sessionKey;
    m_currLinkSource.roiKey = roiKey;
    emit linkInProgressChanged();
}

void ApplicationData::linkRoiDest(const QString& sessionKey, const QString& roiKey)
{
    LOG(INFO) << "Link ROI dest: " << sessionKey.toStdString() << " : " << roiKey.toStdString();

    if (m_currLinkSource.roiKey == "") {
        LOG(WARNING) << "Link source is undefined.";
        clearLinkState();
        emit linkDone();
        commandFailure("Error: Link source is undefined.");
        return;
    }
    if (m_currLinkSource.sessionKey == sessionKey) {
        clearLinkState();
        emit linkDone();
        commandFailure("Cannot link to self.");
        return;
    }
    if (m_project->roiData()->refSessionName() == "") {
        clearLinkState();
        emit linkDone();
        commandFailure("Cannot link without reference session.");
        return;
    }

    m_currLinkDest.sessionKey = sessionKey;
    m_currLinkDest.roiKey = roiKey;

    int ret = m_project->roiData()->modifyRoiLink(m_currLinkSource, m_currLinkDest);
    if (ret == 1) { // change link
        m_project->resetModels(true);
        auto roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        int index = m_project->confirmedModel()->linkedRefIndexAt(sessionKey, roi); // use model in case it's been sorted
        if (index < 0) {
            LOG(WARNING) << "Index DNE!";
        } else {
            emit linkedRoiSelectedIndex(index);
        }
        emit roiModified();
        commandIssued(QStringList{"Changed Link", m_currLinkSource.sessionKey, m_currLinkSource.roiKey});
    } else if (ret == 0) { // new link
        m_project->resetModels(true);
        auto roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        int index = m_project->confirmedModel()->linkedRefIndexAt(sessionKey, roi); // use model in case it's been sorted
        if (index < 0) {
            LOG(WARNING) << "Index DNE!";
        } else {
            emit linkedRoiSelectedIndex(index);
        }
        emit roiModified();
        commandIssued(QStringList{"Linked", m_currLinkSource.sessionKey, m_currLinkSource.roiKey});
    } else {
        commandFailure("Could not link.");
    }
    emit linkDone();
    clearLinkState();
}

void ApplicationData::unlinkRoi(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        LOG(DBUG) << "Unlink ROI: " << sessionKey.toStdString() << " : " << roiKey.toStdString();

        if (m_project->roiData()->refSessionName() == sessionKey) {
            clearLinkState();
            emit linkDone();
            return;
        }

        m_currLinkSource.sessionKey = sessionKey;
        m_currLinkSource.roiKey = roiKey;
        if (m_project->roiData()->removeRoiLink(m_currLinkSource))
        {
            m_project->resetModels(true);
            emit roiModified();
            emit linkDone();
            clearLinkState();
            commandIssued(QStringList{"Unlinked", sessionKey, roiKey});
        } else {
            commandFailure("Nothing to unlink.");
        }
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
        return;
    }
}

void ApplicationData::clearLinkState()
{
    m_currLinkSource.sessionKey = "";
    m_currLinkSource.roiKey = "";
    m_currLinkDest.sessionKey = "";
    m_currLinkDest.roiKey = "";
    emit linkDone();
    emit linkInProgressChanged();
}

bool ApplicationData::createNewRoi(const QString& sessionKey, const QString& roiKey)
{
    std::shared_ptr<RoiFields> newRoi = std::make_shared<RoiFields>();
    newRoi->name = roiKey;
    if (m_project->roiData()->addNewRoi(sessionKey, newRoi->name, newRoi)) {
        emit roiConfirmedDirty();
        emit roiModified();
        commandIssued(QStringList{"Created new ROI", sessionKey, roiKey}, true);
        return true;
    } else {
        return false;
    }
}

/// @return false if the new name is already used
bool ApplicationData::renameRoi(const QString& sessionKey, const QString& roiKey, const QString& newName)
{
    if (roiKey != "")
    {
        auto roi = m_project->roiData()->fieldsAt(sessionKey, newName);
        if (roi) {
            commandFailure("Name already used.");
            return false;
        }

        LOG(DBUG) << "Rename ROI";
        clearLinkState();
        if (m_project->roiData()->renameRoi(sessionKey, roiKey, newName)) {
            m_project->resetModels(true); // update model and notify users, namely ROI tables
            emit roiConfirmedDirty(); // force confirmed table to update
            emit roiRenamed();  // notify slice overlay and slice window
            commandIssued(QStringList{"Renamed ROI", sessionKey, roiKey, " to ", newName}, true);

            m_hideNextCommandUpdate = true;
            selectRoi(sessionKey, newName, true);
            m_hideNextCommandUpdate = true;
            selectRoiFromSlice(sessionKey, newName);
        } else {
            commandFailure("Rename failed.");
        }
    }
    else
    {
        commandFailure("Empty key.");
    }
    return true;
}

bool ApplicationData::splitRoi(const QString& sessionKey, const QString& roiKey, unsigned int maxZ)
{
    LOG(INFO) << "Splitting...";
    std::shared_ptr<RoiFields> newRoi = std::make_shared<RoiFields>();
    newRoi->name = roiKey + "_split";

    LOG(DBUG) << "Adding new ROI " << newRoi->name.toStdString() << " as part of a Split cmd";
    if (!m_project->roiData()->addNewRoi(sessionKey, newRoi->name, newRoi)) {
        LOG(WARNING) << "Split ROI error: Create new ROI failed.";
        commandFailure("Split ROI error: Create new ROI failed.");
        return false;
    }

    auto oldRoi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
    if (oldRoi) {
        QVector<QVector<VoxelCoordinate>> newMaskCoords;
        newMaskCoords.push_back(QVector<VoxelCoordinate>());
        newMaskCoords.push_back(QVector<VoxelCoordinate>());

        WatershedImagePipeline pipeline;
        ///@todo go through all z
        unsigned int currZ = 0;
        for (; currZ < maxZ; ++currZ)
        {
            QImage& src = oldRoi->masks[currZ].mask;
            const int pad = 10;
            const int doublePad = pad * 2;
            QImage padded(src.width() + doublePad, src.height() + doublePad, QImage::Format_RGB32);
            padded.fill(0);
            QPainter p(&padded);
            p.drawImage(QPoint(pad, pad), src);
            p.end();

            ImagePipeline::segments_t segments = pipeline.run(padded);

            if (segments.size() == 0) {
                QString msg = "Zero segments found at z " + QString::number(currZ);
                LOG(WARNING) << msg.toStdString();
            } else {
                if (segments.size() == 1) {
                    QString msg = "Only 1 segment. Duplicating.";
                    LOG(WARNING) << msg.toStdString();
                    // duplicate segment
                    segments.push_back(segments[0]);
                }
                for (int i = 0; i < 2; ++i) {
                    for (auto v : segments[i]) {
                        VoxelCoordinate vUpdated;
                        // these coordinates are relative to the input image so we need to make them slice-relative
                        vUpdated.x = v.x + oldRoi->masks[currZ].extent.x() - pad;
                        vUpdated.y = v.y + oldRoi->masks[currZ].extent.y() - pad;
                        vUpdated.z = currZ;
                        newMaskCoords[i].push_back(vUpdated);
                        qDebug() << "vUpdated:" << vUpdated.x << ", " << vUpdated.y << "," << vUpdated.z;
                    }
                }
            }
        }

        // clear old masks
        oldRoi->masks.clear();
        /// @todo could this get confused going assuming that the old is at 1 and the new is at 0?
        newRoi->mask = newMaskCoords[0];
        oldRoi->mask = newMaskCoords[1];

        // This will [re]generate the extents and QImage masks from the coordinate data
        m_project->roiData()->refreshRoi(sessionKey, newRoi);
        m_project->roiData()->refreshRoi(sessionKey, oldRoi);

    } else {
        // should be impossible
        LOG(WARNING) << "Old ROI not found.";
        commandFailure("Split ROI error: Old ROI not found.");
        return false;
    }

    m_project->resetModels(true);
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Split ROI", sessionKey, roiKey}, true);
    return true;
}


bool ApplicationData::mergeRois(const QString& sessionKey, const QList<RoiData::roi_t>& rois)
{
    LOG(INFO) << "Merging...";
    m_project->roiData()->mergeToFirst(sessionKey, rois);

    // delete all but the first roi
    for (auto roi : rois.mid(1)) {
        m_project->roiData()->deleteRoi(sessionKey, roi->name);
    }

    m_project->resetModels(true);
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Merge ROIs"}, true);
    return true;
}

bool ApplicationData::deleteRoi(const QString& sessionKey, const QString& roiKey)
{
    if (roiKey == "") {
        return false;
    }
    LOG(DBUG) << "Delete ROI";
    clearLinkState();
    if (m_project->roiData()->deleteRoi(sessionKey, roiKey)) {
        m_project->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ROI", sessionKey, roiKey}, true);
        return true;
    } else {
        return false;
    }
}

void ApplicationData::deleteFlaggedRois(const QString& sessionKey)
{
    LOG(DBUG) << "Delete Flagged ROIs in Session";
    clearLinkState();
    unsigned int delCount = m_project->roiData()->deleteFlagged(sessionKey);
    if (delCount > 0)
    {
        m_project->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ", QString::number(delCount) + " ROIs", sessionKey}, true);
    }
}

void ApplicationData::deleteAllFlaggedRois()
{
    LOG(DBUG) << "Delete Flagged ROIs in All Sessions";
    clearLinkState();
    unsigned int delCount = m_project->roiData()->deleteFlagged();
    if (delCount > 0)
    {
        m_project->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ", QString::number(delCount) + " ROIs"}, true);
    }
}

bool ApplicationData::modifyRoi(const QString& sessionKey, const QString& roiKey)
{
    commandIssued(QStringList{"Modified ROI", roiKey}, true);
    ///@todo
    return true;
}

void ApplicationData::filterHighPassRadius(const QString& sessionKey, const int minRadius)
{
    unsigned int filteredOut = m_project->roiData()->filterHighPassRadius(sessionKey, minRadius);
    m_project->resetModels();
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Filtered out",  QString::number(filteredOut) + " ROIs"}, false);
}


void ApplicationData::filterHighPassVolume(const QString& sessionKey, const int minVolume)
{
    unsigned int filteredOut = m_project->roiData()->filterHighPassVolume(sessionKey, minVolume);
    m_project->resetModels();
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Filtered out",  QString::number(filteredOut) + " ROIs"}, false);
}

void ApplicationData::flagFiltered(const QString& sessionKey)
{
    unsigned int flagged = m_project->roiData()->flagFiltered(sessionKey);
    m_project->resetModels();
    emit roiModified();
    emit roiConfirmedDirty();
    emit flagsChanged();
    commandIssued(QStringList{"Flagged",  QString::number(flagged) + " ROIs"}, true);
}

void ApplicationData::unflagAll(const QString& sessionKey)
{
    unsigned int unflagged = m_project->roiData()->unflagAll(sessionKey);
    emit roiConfirmedDirty();
    emit flagsChanged();
    commandIssued(QStringList{"Unflagged",  QString::number(unflagged) + " ROIs"}, false);
}

void ApplicationData::toggleFlag(const QString& sessionKey, const QString& roiKey)
{
    if (roiKey != "") {
        bool state = m_project->roiData()->toggleFlag(sessionKey, roiKey);
        m_project->resetModels();
        emit linkedRoiSelectedIndex(0);
        emit roiModified();
        emit flagsChanged();
        commandIssued(QStringList{state ? "Flagged" : "Unflagged", sessionKey, roiKey}, false);
    }
}

void ApplicationData::commandIssued(QStringList details, bool isModification /*=true*/)
{   
    if (m_hideNextCommandUpdate) {
        m_hideNextCommandUpdate = false;
        return;
    }
    m_commandHistory.push_back(details);

    if (m_commandHistory.size() > m_maxHistory) {
        m_commandHistory.pop_front();
    }
    if (details.size() == 0) {
        return;
    }
    m_lastCmd = details[0];
    if (details.size() == 2) {
        m_lastCmd += " " + details[1];
    }
    if (details.size() >= 3) {
        m_lastCmd += " " + details[1] + "[" + details[2] + "]";
        for (int i = 3; i < details.size(); ++i) {
            m_lastCmd += details[i];
        }
    }
    //for (QString s : details) {
    //    m_lastCmd = m_lastCmd + " " + s;
    //}
    LOG(INFO) << "User cmd: " << m_lastCmd.toStdString();
    emit lastCmdChanged();
    m_dirty = m_dirty || isModification; // flag indicates if we need to save to disk
    if (isModification) {
        emit dirtyChanged(m_dirty);
    }
    ///@todo record in command system so we can support undo/redo
    LOG(DBUG) << "End of cmd processing";
}

void ApplicationData::commandFailure(QString msg)
{
    LOG(INFO) << msg.toStdString();
    m_lastCmd = msg;
    emit lastCmdChanged();
}


void ApplicationData::saveAllRoiFiles()
{
    m_project->saveAllRoiFiles();
    commandIssued(QStringList{"Saved ROI files."}, false);
    m_dirty = false;
    emit dirtyChanged(m_dirty);
}

QQmlListProperty<SearchResult> ApplicationData::getSearchResults()
{
    return QQmlListProperty<SearchResult>(this, this,
                                          &ApplicationData::searchResultsCount,
                                          &ApplicationData::searchResultAt);
}

int ApplicationData::searchResultsCount() const
{
    //LOG(DBUG) << "m_searchResults.size(): " << m_searchResults.size();
    return m_searchResults.size();
}

SearchResult* ApplicationData::searchResultAt(int index) const
{
    //LOG(DBUG) << "m_searchResults.at(" << index << ") (size " << m_searchResults.size() << ")";
    if (index < m_searchResults.size()) {
        return m_searchResults.at(index).get();
    } else {
        return nullptr;
    }
}

int ApplicationData::searchResultsCount(QQmlListProperty<SearchResult>* list)
{
    return reinterpret_cast<ApplicationData*>(list->data)->searchResultsCount();
}

SearchResult* ApplicationData::searchResultAt(QQmlListProperty<SearchResult>* list, int i)
{
    return reinterpret_cast<ApplicationData*>(list->data)->searchResultAt(i);
}

