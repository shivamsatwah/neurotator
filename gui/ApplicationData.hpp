/// @author Sam Kenyon <sam@metadevo.com>
#ifndef APPLICATIONDATA_HPP
#define APPLICATIONDATA_HPP
#include <memory>
#include <utility>
#include <QCoreApplication>
#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QQmlListProperty>

#include "AppSettings.hpp"
#include "Project.hpp"
#include "SearchResults.hpp"

/// Wrapper for QML access to current project data and app settings
class ApplicationData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString windowTitle READ getWindowTitle WRITE setWindowTitle NOTIFY windowTitleChanged)
    Q_PROPERTY(QString projectName READ getProjectName WRITE setProjectName NOTIFY projectNameChanged)
    Q_PROPERTY(QString refSession READ refSession WRITE setRefSession NOTIFY refSessionChanged)
    Q_PROPERTY(QString dirty READ getDirty NOTIFY dirtyChanged)
    Q_PROPERTY(QString cmdText READ getLastCmd NOTIFY lastCmdChanged)
    Q_PROPERTY(bool linkInProgress READ isLinking NOTIFY linkInProgressChanged)
    Q_PROPERTY(bool refExists READ refExists NOTIFY refExistsChanged)
    Q_PROPERTY(RoiDataModel* confirmedModel READ getConfirmedModel NOTIFY newModel)
    Q_PROPERTY(QStringList searchableSources READ getSearchableSources NOTIFY searchableSourcesChanged)
    Q_PROPERTY(QQmlListProperty<SearchResult> searchResults READ getSearchResults NOTIFY searchResultsChanged)

public:
    ApplicationData(ApplicationData&) = delete;
    ApplicationData& operator=(ApplicationData&) = delete;

    /// Singleton
    static ApplicationData& instance() {
        static ApplicationData instance;
        return instance;
    }

    Q_INVOKABLE QVariant getSetting(QString name) const { return AppSettings::instance().get(name); }
    Q_INVOKABLE QString getSettingString(QString name) const { return AppSettings::instance().get(name).toString(); }
    Q_INVOKABLE void loadProject(QUrl url);
    Q_INVOKABLE void saveProjectAs(QUrl url);
    Q_INVOKABLE void saveAllRoiFiles();
    Q_INVOKABLE void loadStack(QUrl url);
    Q_INVOKABLE void unloadStack(const QString& name);
    Q_INVOKABLE void loadRoiData(QUrl url);
    Q_INVOKABLE void setRecentProjectDir(QUrl url) const { AppSettings::instance().set("defaultprojectdir", url.toString()); }
    Q_INVOKABLE QUrl getRecentProjectDir() const { return AppSettings::instance().defaultProjectDirectory(); }
    Q_INVOKABLE QUrl getRecentImportDir() const { return QUrl::fromLocalFile(AppSettings::instance().get("default_import_dir").toString()); }
    Q_INVOKABLE void setRecentImportDir(QUrl url) { AppSettings::instance().set("default_import_dir", url.toString()); }
    Q_INVOKABLE int getNumOfSessions() const { return m_project->roiData()->sessionCount(); }    
    Q_INVOKABLE bool selectRoi(const QString& sessionKey, const QString& roiKey, bool selectInConfirmed = false);
    //Q_INVOKABLE RoiDataModel* getLastUnconfirmedModel() const { return m_project->lastUnconfirmedModel(); }
    Q_INVOKABLE RoiDataModel* getUnconfirmedModel(const int index) const { return m_project->unconfirmedModel(index); }
    Q_INVOKABLE QList<QUrl> getRecentProjects() { return AppSettings::instance().recentProjects(); }
    Q_INVOKABLE QStringList getRecentProjectNames() { return AppSettings::instance().recentProjectNames(); }
    Q_INVOKABLE void clearRecentProjects() { AppSettings::instance().clearRecentProjects(); }
    Q_INVOKABLE int getCurrentStackWindow() { return m_project->currentStackWindowIndex(); }
    Q_INVOKABLE void setCurrentStackWindow(const int index) { m_project->setCurrentStackWindow(index); }
    Q_INVOKABLE QList<QUrl> getStackWindowUrls() { return m_project->stackWindowUrls(); }
    Q_INVOKABLE void searchRois(const QString& sessionKey, QString namePattern);

    QString getWindowTitle() const { return m_windowTitle; }
    QString getProjectName() const { return m_project->name(); }
    bool isLinking() const { return (m_currLinkSource.roiKey != ""); }
    QString refSession() { return m_project->roiData()->refSessionName(); }
    bool getDirty() const { return m_dirty; }
    QString getLastCmd() { return m_lastCmd; }
    bool refExists() const { return m_project->roiData()->refSessionExists(); }
    RoiDataModel* getConfirmedModel() { return m_project->confirmedModel(); }
    QStringList getSearchableSources();
    QQmlListProperty<SearchResult> getSearchResults();
    int searchResultsCount() const;
    SearchResult* searchResultAt(int) const;

    void setWindowTitle(const QString& title);
    void setProjectName(const QString& name);
    void setProject(std::shared_ptr<Project> project);
    std::shared_ptr<Project> getProject() { return m_project; }
    void setRefSession(const QString& sessionName);
    void broadcastSyncChange(int z, int t, int zoom, float offsetX, float offsetY) {
        emit syncNavChanged(z, t, zoom, offsetX, offsetY);
    }
    uchar getIbase() { return m_iBase; }

// Most of these slots will be considered "commands" logged in the history
public slots:
    void selectRoiFromSlice(const QString& sessionKey, const QString& roiKey);
    void selectRoiInSlice(const QString& sessionKey, const QString& roiKey);
    void linkRoi(const QString& sessionKey, const QString& roiKey);
    void linkRoiSrc(const QString& sessionKey, const QString& roiKey);
    void linkRoiDest(const QString& sessionKey, const QString& roiKey);
    void unlinkRoi(const QString& sessionKey, const QString& roiKey);
    void clearLinkState();
    bool createNewRoi(const QString& sessionKey, const QString& roiKey);
    bool renameRoi(const QString& sessionKey, const QString& roiKey, const QString& newName);
    bool modifyRoi(const QString& sessionKey, const QString& roiKey);
    bool splitRoi(const QString& sessionKey, const QString& roiKey, unsigned int maxZ);
    bool mergeRois(const QString& sessionKey, const QList<RoiData::roi_t>&);
    bool deleteRoi(const QString& sessionKey, const QString& roiKey);
    void deleteFlaggedRois(const QString& sessionKey);
    void deleteAllFlaggedRois();
    void filterHighPassRadius(const QString& sessionKey, const int minRadius);
    void filterHighPassVolume(const QString& sessionKey, const int minVolume);
    void flagFiltered(const QString& sessionKey);
    void unflagAll(const QString& sessionKey);
    void toggleFlag(const QString& sessionKey, const QString& roiKey);
    void sortByLink();
    void sortByName(const int column);

signals:
    void windowTitleChanged(QString title);
    void refSessionChanged();
    void dirtyChanged(bool flag);
    void lastCmdChanged();
    void linkInProgressChanged();
    void refExistsChanged();
    void roiSelected(QString sessionKey, QString roiKey);
    void roiSelectedIndex(QString sessionKey, int roiIndex);
    void linkedRoiSelectedIndex(int roiIndex);
    void roiConfirmedSelected(int roiIndex);
    void roiConfirmedDirty();
    void roiModified();
    void roiRenamed();
    void linkDone();
    void syncNavChanged(int z, int t, int zoom, float offsetX, float offsetY);
    void recreateTable();
    void removedSession(QString name);
    void flagsChanged();
    void roiDeleted();
    void projectNameChanged();
    void loadingNewProject();
    void newModel();
    void searchableSourcesChanged();
    void searchResultsChanged();

private:
    ApplicationData();
    ~ApplicationData();

    void deleteSearchResults();
    void commandIssued(QStringList details, bool isModification = true);
    void commandFailure(QString msg);

    static int searchResultsCount(QQmlListProperty<SearchResult>*);
    static SearchResult* searchResultAt(QQmlListProperty<SearchResult>*, int);

    QString formatWindowTitle(const QString& projectName) {
        return projectName.leftRef(100) + " - " + QCoreApplication::applicationName() + " " + QCoreApplication::applicationVersion();
    }

    std::shared_ptr<Project> m_project;

    QString m_windowTitle;
    RoiAddress m_currLinkSource;
    RoiAddress m_currLinkDest;
    QVector<QStringList> m_commandHistory;    
    int m_maxHistory = 100;
    QString m_lastCmd;
    bool m_hideNextCommandUpdate = false;
    bool m_dirty = false;
    uchar m_iBase = 1; // set to 1 for 1-based coordinates instead of 0-based
    std::vector<std::shared_ptr<SearchResult>> m_searchResults;
};

#endif // APPLICATIONDATA_HPP
