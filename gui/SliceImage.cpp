/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QObject>
#include <QSGNode>
#include <QSGImageNode>
#include <QSGSimpleRectNode>
#include <QString>
#include <QQuickWindow>
#include <g3log/g3log.hpp>
#include <stdlib.h>

#include "ApplicationData.hpp"
#include "HyperstackTiff.hpp"
#include "ImageAdjustments.hpp"
#include "SliceImage.hpp"

SliceImage::SliceImage()
{
    try
    {
        m_iBase = ApplicationData::instance().getIbase();
        setFlag(QQuickItem::ItemHasContents);

        m_project = ApplicationData::instance().getProject();
        m_stack = m_project->currentStack();
        if (m_stack) {
            //m_stack->addClient(); // for future stuff
            m_maxColorValue = m_stack->maxColorValue();
            m_name = m_stack->name();
            m_stackWindowIndex = m_project->currentStackWindowIndex();
        } else {
            LOG(WARNING) << "ERROR: stack is null!";
        }

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiSelected(QString, QString)),
                         this, SLOT(roiSelectionRequest(QString, QString)));
        QObject::connect(&ApplicationData::instance(), SIGNAL(refSessionChanged()),
                         this, SLOT(checkIsRef()));
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

SliceImage::~SliceImage()
{
    if (m_stack) {
        m_stack->removeClient();
    }
}

QString SliceImage::getIndexStr() const
{
    QString s;
    if (m_stack)
    {
        HyperstackIndex index = m_stack->index();
        s = QStringLiteral("z: %1  t: %2").arg(index.z + m_iBase).arg(index.time + m_iBase);
    } else {
        s = "???";
    }
    return s;
}

///  Called on the render thread. Main thread is blocked while this function is executed.
QSGNode* SliceImage::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData)
    QSGImageNode* n = static_cast<QSGImageNode*>(oldNode);

    if (!m_stack) {
        LOG(WARNING) << "ERROR: stack is null!";
        setWidth(800);
        setHeight(600);
        QSGSimpleRectNode* emptyNode = new QSGSimpleRectNode();
        emptyNode->setColor(Qt::blue);
        emptyNode->setRect(boundingRect());
        return emptyNode;
    }
    if (!n) {
        n = window()->createImageNode();
        n->setOwnsTexture(true);
        n->setFiltering(QSGTexture::Linear);
        setWidth(m_stack->size().x);
        setHeight(m_stack->size().y);

        // We're loading part of config here because it doesn't work if we do it before the first paint
        m_loadingConfig = true;

        const StackWindow& config = m_project->getStackWindowConfig(m_stackWindowIndex);

        // order of operations matters here
        if (static_cast<int>(width()) != static_cast<int>(config.width)) {
            setScaledWidth(static_cast<int>(config.width));
        }
        if (m_zoomFactor != config.zoomFactor) {
            setZoomFactor(config.zoomFactor);
        }

        if (m_offsetX != config.offsetX || m_offsetY != config.offsetY) {
            setOffsets(config.offsetX, config.offsetY);
        }

        m_loadingConfig = false;
    }

    // Get the latest buffer if it's ready
    QImage image;
    if (m_stack->ready()) {
        //if (m_zoomFactor != 100) {
            //image = m_stack->imageBuffer().scaledToWidth(static_cast<int>(m_scaledWidth)/*, Qt::SmoothTransformation*/);
        //} else {
            image = m_stack->imageBuffer();
        //}
    }

    if (image.isNull()) {
        LOG(WARNING) << "ERROR: image is null!";
        return n;
    }

    if (m_histEqualization) {
        qDebug() << "hist eq";
        image = ImageAdjustments::equalizeHistogram(image);
    }

    // check offsets
    if (m_zoomFactor > m_zoomThresh) {
        float midOffsetX = abs(m_scaledInvWidth - getMaxX()) / 2.0;
        float midOffsetY = abs(m_scaledInvHeight - getMaxY()) / 2.0;

        // make sure texture offset won't go below zero (lined up with upper-left of viewport)
        if (m_offsetX < -midOffsetX) {
            m_offsetX = -midOffsetX;
        }
        if (m_offsetY < -midOffsetY) {
            m_offsetY = -midOffsetY;
        }
        // make sure texture offset won't go higher than lining up with lower-right of the viewport
        if (m_offsetX > midOffsetX) {
            m_offsetX = midOffsetX;
        }
        if (m_offsetY > midOffsetY) {
            m_offsetY = midOffsetY;
        }

        // calc offset into the texture
        m_textureOffsetX = midOffsetX + m_offsetX;
        m_textureOffsetY = midOffsetY + m_offsetY;
        //qDebug() << "texoff: " << m_textureOffsetX << ", " << m_textureOffsetY;
    }
    if (m_offsetChanged) {
        emit scaleChanged(m_scaling, -m_textureOffsetX, -m_textureOffsetY);
        m_offsetChanged = false;
    }

    QSGTexture* texture = window()->createTextureFromImage(image, QQuickWindow::TextureIsOpaque);
    //m_textureWidth = texture->textureSize().width();
    //m_textureHeight = texture->textureSize().height();

    if (!texture)
    {
        LOG(WARNING) << "ERROR: Texture null!";
        return n;
    }
    n->setTexture(texture);
    //n->setRect(0, 0, m_textureWidth, m_textureHeight);
    n->setRect(0, 0, width(), height());
    n->setSourceRect(m_textureOffsetX, m_textureOffsetY, m_scaledInvWidth, m_scaledInvHeight);
    //n->setSourceRect(m_textureOffsetX, m_textureOffsetY, 200, 200);
    return n;
}

void SliceImage::windowPositionChanged(const int x, const int y)
{
    m_project->saveStackWindowPosition(m_stackWindowIndex, x, y);
}

//void SliceImage::windowHidden()
//{
//    m_project->saveStackWindowHidden(m_stackWindowIndex);
//}

void SliceImage::adjustOffset(const int x, const int y)
{
    if (m_zoomFactor <= m_zoomThresh) {
        return;
    }
    m_offsetX -= x;
    m_offsetY -= y;

    m_offsetChanged = true;
    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY);
    }
    if (!m_loadingConfig) {
        m_project->saveStackOffset(m_stackWindowIndex, m_offsetX, m_offsetY);
    }
}

void SliceImage::setOffsets(const float x, const float y)
{
    if (x != m_offsetX) {
        m_offsetX = x;
    }
    if (y != m_offsetY) {
        m_offsetY = y;
    }

    m_offsetChanged = true;

    if (!m_loadingConfig) {
        m_project->saveStackOffset(m_stackWindowIndex, m_offsetX, m_offsetY);
    }
}

void SliceImage::setZoomFactor(const int zoomFactor)
{
//    qDebug() << "Width: " << width() << ", getMaxX: " << getMaxX();
//    qDebug() << "zoom input: " << zoomFactor;
//    qDebug() << "zoomFactor: " << m_zoomFactor;
//    qDebug() << "m_zoomThresh: " << m_zoomThresh;
    m_zoomFactor = zoomFactor;
    if (m_zoomFactor <= m_zoomThresh) {
        m_zoomFactor = m_zoomThresh;
        m_textureOffsetX = 0;
        m_textureOffsetY = 0;
        m_offsetX = 0;
        m_offsetY = 0;
    }

    if (m_zoomFactor < 1) {
        m_zoomFactor = 1;
    }
    if (m_zoomFactor > 2000) {
        m_zoomFactor = 2000;
    }
    if (m_zoomFactor < m_zoomThresh) {
        m_zoomFactor = m_zoomThresh;
    }
    //qDebug() << "new m_zoomFactorh: " << m_zoomFactor;

    m_scaling = static_cast<float>(m_zoomFactor) / 100.0f;
    m_scaledWidth = getMaxX() * m_scaling;
    m_scaledHeight = getMaxY() * m_scaling;
    //qDebug() << "Z scaled: " << m_scaledWidth  << ", " << m_scaledHeight;
    m_scaledInvWidth = width() / m_scaling;
    m_scaledInvHeight = height() / m_scaling;

//    qDebug() << "scaled: " << m_scaledWidth << ", " << m_scaledHeight;
//    qDebug() << "invscaled: " << m_scaledInvWidth << ", " << m_scaledInvHeight;
    emit zoomFactorChanged(m_zoomFactor);

    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
    }

    m_offsetChanged = true;
    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY);
    }
}

/// Setting this overwrites whatever the zoomfactor was
void SliceImage::setScaledWidth(const int w)
{
    m_scaledWidth = w;
    if (m_scaledWidth < 1) {
        m_scaledWidth = 1;
    }
    m_scaling = static_cast<float>(m_scaledWidth) / getMaxX();
    m_scaledHeight = getMaxY() * m_scaling;

    setWidth(m_scaledWidth);
    setHeight(m_scaledHeight);
    m_scaledInvWidth = m_scaledWidth / m_scaling;
    m_scaledInvHeight = m_scaledHeight / m_scaling;

    m_textureOffsetX = 0;
    m_textureOffsetY = 0;
    m_offsetX = 0;
    m_offsetY = 0;

    updateZoomThresh();
    m_zoomFactor = static_cast<int>(m_scaling * 100.0f);
    emit zoomFactorChanged(m_zoomFactor);
    emit scaleChanged(m_scaling, -m_textureOffsetX, -m_textureOffsetY);
    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
        m_project->saveStackWidth(m_stackWindowIndex, m_scaledWidth); // only saving width since aspect ratio is maintained
    }
}

/// Setting this overwrites whatever the zoomfactor was
void SliceImage::setScaledHeight(const int h)
{
    m_scaledHeight = h;
    if (m_scaledHeight < 1) {
        m_scaledHeight = 1;
    }
    m_scaling = static_cast<float>(m_scaledHeight) / getMaxY();
    m_scaledWidth = getMaxX() * m_scaling;
    setWidth(m_scaledWidth);
    setHeight(m_scaledHeight);
    m_scaledInvWidth = m_scaledWidth / m_scaling;
    m_scaledInvHeight = m_scaledHeight / m_scaling;

    m_textureOffsetX = 0;
    m_textureOffsetY = 0;
    m_offsetX = 0;
    m_offsetY = 0;

    updateZoomThresh();
    m_zoomFactor = static_cast<int>(m_scaling * 100.0f);
    emit zoomFactorChanged(m_zoomFactor);
    emit scaleChanged(m_scaling, -m_textureOffsetX, -m_textureOffsetY);

    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
        m_project->saveStackWidth(m_stackWindowIndex, m_scaledWidth); // only saving width since aspect ratio is maintained
    }
}

void SliceImage::resetZoom()
{
    setZoomFactor(100);
}

/// This adjusts the viewing configuration from a saved project
void SliceImage::loadWindowConfig()
{
    m_loadingConfig = true;

    const StackWindow& config = m_project->getStackWindowConfig(m_stackWindowIndex);

    if (getIndexZ() != static_cast<int>(config.coords.z)) {
        gotoZ(static_cast<int>(config.coords.z + m_iBase));
    }

    // width, zoom and offsets are loaded in updatePaintNode() when it first creates the node

    emit changeStackWindowPosition(config.windowX, config.windowY);

    m_loadingConfig = false;
}

///@param z: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceImage::gotoZ(const int z)
{
    if (z < 0 || !m_stack) {
        return;
    }
    HyperstackIndex index;
    index.z = static_cast<unsigned int>(z - m_iBase); // ensure it's a zero-based index
    m_stack->loadSlice(index);

    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY);
    }
    if (!m_loadingConfig) {
        m_project->saveStackZ(m_stackWindowIndex, index.z);
    }
}

///@param t: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceImage::gotoTime(const int t)
{
    if (t < 0 || !m_stack) {
        return;
    }
    HyperstackIndex index;
    index.time = static_cast<unsigned int>(t - m_iBase); // ensure it's a zero-based index
//    m_stack->loadSlice(index);

//    if (m_synced) {
//        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime());
//    }
//    if (!m_loadingConfig) {
//        m_project->saveStackTime(m_stackWindowIndex, index.time);
//    }
}

void SliceImage::nextZ() {
    if (m_stack) {
        m_stack->loadNextZ();
        if (m_synced) {
            ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY);
        }
        if (!m_loadingConfig) {
            m_project->saveStackZ(m_stackWindowIndex, getIndexZ());
        }
    }
}

void SliceImage::prevZ() {
    if (m_stack) {
        m_stack->loadPrevZ();
        if (m_synced) {
            ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY);
        }
        if (!m_loadingConfig) {
            m_project->saveStackZ(m_stackWindowIndex, getIndexZ());
        }
    }
}

/// If this stack is the specified session then navigate to the correct slice
/// and select the ROI.
void SliceImage::roiSelectionRequest(QString sessionKey, QString roiKey)
{
    if (sessionKey == m_name) {
        // get the data structure
        RoiData::roi_t roi = m_project->roiData()->fieldsAt(sessionKey, roiKey);
        if (!roi)
        {
            LOG(WARNING) << "ROI " << roiKey.toStdString() << " does not exist in this session.";
            return;
        }

        //LOG(DBUG) << "navigate to slice";
        gotoZ(roi->centroid.z + m_iBase);

        emit navChanged();
    }
}

bool SliceImage::isRef()
{
    return (m_name == m_project->roiData()->refSessionName());
}
