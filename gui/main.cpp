#include <memory>
#include <string>
#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QQmlContext>
#include <QQuickStyle>
#include <g3log/g3log.hpp>
#include <g3log/logworker.hpp>

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "Project.hpp"
#include "RoiDataModel.hpp"
#include "SliceImage.hpp"
#include "SliceOverlay.hpp"

int main(int argc, char *argv[])
{
    QString versionStr("1.95");
    QString appNameStr = "Neurotator";
#ifndef _DEBUG
    auto worker = g3::LogWorker::createLogWorker();
    std::string logname = "neurotator";
    std::string logFilePath = "./";
    std::string logid = "run";
    auto handle = worker->addDefaultLogger(logname, logFilePath);
    g3::initializeLogging(worker.get());
    LOG(INFO) << "Starting " << appNameStr.toStdString();
#endif

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QCoreApplication::setApplicationName(appNameStr);
    QCoreApplication::setOrganizationName(QStringLiteral("Boston University"));
    QCoreApplication::setApplicationVersion(versionStr);

    QQuickStyle::setStyle("Material");

    // register qml types (custom widgets)
    qmlRegisterType<SliceImage>("com.bu", 1, 0, "SliceImage");
    qmlRegisterType<SliceOverlay>("com.bu", 1, 0, "SliceOverlay");
    qmlRegisterType<RoiDataModel>("com.bu", 1, 0, "RoiDataModel");
    qmlRegisterType<SearchResult>("com.bu", 1, 0, "SearchResult");

    std::shared_ptr<Project> blankProject = std::make_shared<Project>();
    ApplicationData::instance().setProject(blankProject);
    ApplicationData& appdata = ApplicationData::instance();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("appdata", &appdata);
    engine.load(QUrl(QStringLiteral("qrc:/gui/qml/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    try
    {
        app.exec();
    }
    catch (const std::bad_alloc &e)
    {
        LOG(FATAL) << "Ran out of memory!" << e.what();
    }
    catch (const std::exception &e)
    {
        LOG(FATAL) << "Exception:" << e.what();
    }
    catch (...)
    {
        LOG(FATAL) << "Unknown exception";
    }
    return 0;
}
