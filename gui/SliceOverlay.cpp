/// @author Sam Kenyon <sam@metadevo.com>
#include <g3log/g3log.hpp>

#include <QDebug>
#include <QPainter>
#include <QUuid>

#include "ApplicationData.hpp"
#include "RoiData.hpp"
#include "SliceOverlay.hpp"

SliceOverlay::SliceOverlay(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_appdata(ApplicationData::instance()),
    m_confirmedPen(QColor(0, 255, 0, 220), 1),
    m_unconfirmedPen(QColor(255, 0, 0, 220), 1),
    m_confirmedColor(QColor(0, 255, 0).rgba()),
    m_unconfirmedColor(QColor(255, 0, 0).rgba()),
    m_refColor(QColor(255, 255, 255).rgba()),
    m_confirmedCentroidPen(QColor(0, 255, 0, 128), 3),
    m_unconfirmedCentroidPen(QColor(255, 0, 0, 128), 3),
    m_defaultSelectPen(QPen(QColor(255, 255, 255, 220), 2, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin)),
    m_editSelectPen(QPen(QColor(255, 255, 0, 150), 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin)),
    m_selectPen{m_defaultSelectPen},
    m_linkingPen(QPen(QColor(0, 255, 255, 220), 3, Qt::DashDotDotLine, Qt::RoundCap, Qt::RoundJoin)),
    m_lightPen(QColor(255, 255, 255, 150)),
    m_labelPen(QColor(255, 255, 255, 200)),
    m_maskColorTable{QColor(0, 0, 0, 0).rgba(), m_unconfirmedColor},
    m_labelFont(QFont("Sans Serif", 8))
{
    try
    {
        m_iBase = ApplicationData::instance().getIbase();
        m_iBaseF = static_cast<float>(ApplicationData::instance().getIbase());
        m_z = m_iBase;
        m_roiData = m_appdata.getProject()->roiData();

        QFontMetrics fm(m_labelFont);
        m_labelHeight = fm.height() * m_maxLabelLines;

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiSelected(QString, QString)),
                         this, SLOT(roiSelectionRequest(QString, QString)));

        QObject::connect(&ApplicationData::instance(), SIGNAL(linkDone()),
                         this, SLOT(resetLinking()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiModified()),
                         this, SLOT(update()));
        QObject::connect(&ApplicationData::instance(), SIGNAL(roiModified()),
                         this, SIGNAL(roiSelectionChanged()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiRenamed()),
                         this, SLOT(refresh()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiDeleted()),
                         this, SLOT(refresh()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(flagsChanged()),
                         this, SIGNAL(roiSelectionChanged()));
    }
    catch (const std::exception &e)
    {
        LOG(WARNING) << "ERROR: " << e.what();
    }
    catch (...)
    {
        LOG(WARNING) << "Unknown exception";
    }
}

void SliceOverlay::paint(QPainter *painter)
{
    painter->scale(m_scaling, m_scaling);
    painter->translate(-m_iBaseF + m_textureOffsetX, -m_iBaseF + m_textureOffsetY);

    painter->setBrush(Qt::NoBrush);
    painter->setRenderHint(QPainter::Antialiasing);
    //painter->setRenderHint(QPainter::SmoothPixmapTransform);
    if (m_showLabels) {
        painter->setFont(m_labelFont);
    }

    // won't show any masks or indicators if m_visibleRois is empty
    for (QList<RoiData::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        bool hidden = !m_showConfirmed && (*it)->confirmed;
        hidden = hidden || (!m_showFullyLinked && (*it)->fullyLinked);
        hidden = hidden || (!m_showFiltered && (*it)->filtered);
        hidden = hidden || (!m_showFlagged && (*it)->flagged);
        if (m_showMasks && !hidden) {
            if ((*it)->confirmed) {
                m_maskColorTable[1] = m_confirmedColor;
            } else {
                m_maskColorTable[1] = m_unconfirmedColor;
            }

            if (m_showMaskOutlines) {
                (*it)->masks[m_z].maskOutline.setColorTable(m_maskColorTable);
                painter->drawImage((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.y(), (*it)->masks[m_z].maskOutline);
            } else {
                (*it)->masks[m_z].mask.setColorTable(m_maskColorTable);
                painter->drawImage((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.y(), (*it)->masks[m_z].mask);
            }
        }
        if (m_showCentroids) {
//            if ((*it)->confirmed) {
//                painter->setPen(m_confirmedCentroidPen);
//                drawCrosshairs(painter, *it);
//                painter->setPen(m_lightPen);
//                drawCrosshairs(painter, *it);
//            } else {
//                painter->setPen(m_unconfirmedCentroidPen);
//                drawCrosshairs(painter, *it);
//                painter->setPen(m_lightPen);
//                drawCrosshairs(painter, *it);
//            }
            painter->setPen(m_lightPen);
            drawCrosshairs(painter, *it);
        }
    }

    if (m_showRef)
    {
        for (QList<RoiData::roi_t>::iterator it = m_refRois.begin(); it != m_refRois.end(); ++it)
        {
            m_maskColorTable[1] = m_refColor;
            if (m_showMaskOutlines) {
                (*it)->masks[m_z].maskOutline.setColorTable(m_maskColorTable);
                painter->drawImage((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.y(), (*it)->masks[m_z].maskOutline);
            } else {
                (*it)->masks[m_z].mask.setColorTable(m_maskColorTable);
                painter->drawImage((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.y(), (*it)->masks[m_z].mask);
            }
        }
    }

    // overlays that need to stay at constant size
    painter->resetTransform();
    for (QList<RoiData::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        bool hidden = !m_showConfirmed && (*it)->confirmed;
        hidden = hidden || (!m_showFullyLinked && (*it)->fullyLinked);
        hidden = hidden || (!m_showFiltered && (*it)->filtered);
        hidden = hidden || (!m_showFlagged && (*it)->flagged);
        if (m_showMasks && !hidden) {
            if (m_showLabels) {
                painter->setPen(m_selectPen);
                QString labelText((*it)->name + "\nradius: ");
                labelText += QString::number(RoiData::radius((*it)->masks[m_z].extent));
                QPoint ul = worldToViewCoords((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.bottom());
                painter->drawText(QRect(ul.x(), ul.y(), m_maxLabelWidth, m_labelHeight), 0, labelText);
            }
        }
    }
    if (m_linking) {
        painter->setPen(m_linkingPen);
    } else {
        painter->setPen(m_selectPen);
    }
    for (QHash<QString, RoiData::roi_t>::iterator j = m_selectedRois.begin(); j != m_selectedRois.end(); ++j)
    {
        painter->drawRoundedRect(worldToViewCoords((*j)->masks[m_z].extent), 2, 2);
    }
}

void SliceOverlay::paintMask(QPainter *painter, RoiData::roi_t roi)
{
    if (roi->confirmed) {
        painter->setPen(m_confirmedPen);
    } else {
        painter->setPen(m_unconfirmedPen);
    }
    for (auto it : roi->mask) {
        if (it.z == m_z) {
            painter->drawPoint(it.x, it.y);
        }
    }
}

void SliceOverlay::paintMaskOutline(QPainter *painter, RoiData::roi_t roi)
{
    if (roi->confirmed) {
        painter->setPen(m_confirmedPen);
    } else {
        painter->setPen(m_unconfirmedPen);
    }

}

void SliceOverlay::drawEllipse(QPainter *painter, RoiData::roi_t roi)
{
    painter->drawEllipse(roi->masks[m_z].extent);
}

void SliceOverlay::drawCrosshairs(QPainter *painter, RoiData::roi_t roi)
{
    int w = 10; //roi->extents.width();
    int h = 10; //roi->extents.height();
    painter->drawLine(roi->centroid.x - w, roi->centroid.y, roi->centroid.x + w, roi->centroid.y);
    painter->drawLine(roi->centroid.x, roi->centroid.y - h, roi->centroid.x, roi->centroid.y + h);
//    painter->drawLine(roi->extents.x(), roi->centroid.y, roi->extents.right(), roi->centroid.y);
//    painter->drawLine(roi->centroid.x, roi->extents.top(), roi->centroid.x, roi->extents.bottom());
}

/// Like update() but also clears the current selection
void SliceOverlay::refresh()
{
    m_selectedRois.clear();
    emit roiSelectionChanged();

    if (m_showAllRois) {
        showAllRois();
    } else {
        m_visibleRois.clear();
    }
    update();
}

void SliceOverlay::roiSelectionRequest(QString sessionKey, QString roiKey)
{
    if (sessionKey == m_sessionName) {
        // get the data structure
        RoiData::roi_t roi = m_roiData->fieldsAt(sessionKey, roiKey);
        if (!roi)
        {
            LOG(DBUG) << "ROI " << roiKey.toStdString() << " does not exist in this session.";
            return;
        }

        if (m_z != roi->centroid.z) {
            changeSlice(roi->centroid.z);
        }

        m_selectedRois[roiKey] = roi;
        emit roiSelectionChanged();
    }
}

/// Select an ROI that is in the current slice
void SliceOverlay::selectRoiInSlice(QString roiKey)
{
    RoiData::roi_t roi = m_roiData->fieldsAt(m_sessionName, roiKey);
    if (roi && (m_z == roi->centroid.z)) {
        m_selectedRois[roiKey] = roi;
        emit roiSelectionChanged();
    } else {
        LOG(WARNING) << "ROI " << roiKey.toStdString() << " does not exist in this slice.";
    }
}

/// Show all ROI locations in this slice
void SliceOverlay::showAllRois()
{
    m_showAllRois = true;
    m_visibleRois.clear();
    m_visibleRois = m_roiData->roisAtSessionFilterZ(m_sessionName, m_z);
}

void SliceOverlay::hideAllRois()
{
    m_showAllRois = false;
    m_visibleRois.clear();
}

///@return true if the coordinate is inside a region of interest
RoiData::roi_t SliceOverlay::coordInRoi(const float mouseX, const float mouseY)
{
    int x = static_cast<int>(mouseX);
    int y = static_cast<int>(mouseY);
    if (m_scaling != 1.0f) {
        QPoint p = viewToWorldCoords(mouseX, mouseY);
        x = p.x();
        y = p.y();
    }
    for (QList<RoiData::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        int left = (*it)->masks[m_z].extent.x();
        int right = (*it)->masks[m_z].extent.right();
        int top = (*it)->masks[m_z].extent.y();
        int bottom = (*it)->masks[m_z].extent.bottom();
        if (x >= left && x <= right && y >= top && y <= bottom) {
            return (*it);
        }
    }

    return nullptr;
}

/// The action depends on what mode we're in
void SliceOverlay::mouseClick(const float mouseX, const float mouseY)
{
    if (m_mode == EDITMASK_MODE) {
        modifyRoiPixel(mouseX, mouseY);
    } else {
        selectRoiViaMouse(mouseX, mouseY);
    }
}

/**
 * @brief Select an ROI via mouse if there's one at the coords
 * @param x
 * @param y
 * @param action: extra action to perform if we selected an ROI
 */
void SliceOverlay::selectRoiViaMouse(const float mouseX, const float mouseY, const int action)
{
    RoiData::roi_t roi = coordInRoi(mouseX, mouseY);    
    if (roi)
    {
        if (action != MULTISELECT_ACTION) {
            m_selectedRois.clear();
        }
        m_selectedRois[roi->name] = roi;

        ApplicationData::instance().selectRoiFromSlice(m_sessionName, roi->name);
        switch (action) {
            case LINK_ACTION:
                ApplicationData::instance().linkRoi(m_sessionName, roi->name);
                m_linking = ApplicationData::instance().isLinking();
                break;
            case UNLINK_ACTION:
                ApplicationData::instance().unlinkRoi(m_sessionName, roi->name);
                ApplicationData::instance().clearLinkState();
                m_linking = false;
                break;
            default:
                ApplicationData::instance().clearLinkState();
                m_linking = false;
        }
        update();
        emit roiSelectionChanged();
        emit linkingChanged(); // just for the GUI
        return;
    }

    // if no hits, then just unselect
    m_selectedRois.clear();
    emit roiSelectionChanged();
    update();
}

void SliceOverlay::modifyRoiPixel(const float mouseX, const float mouseY, const ModifyRoiAction action)
{
    if (m_selectedRois.size() == 1) {
        ApplicationData::instance().clearLinkState();
        setIsLinking(false);

        ///@todo log as command in appdata
        QPoint p = viewToWorldCoords(mouseX, mouseY);

        // convert to mask-relative coords
        int mx = p.x() - m_selectedRois.values()[0]->masks[m_z].extent.x() + 1;
        int my = p.y() - m_selectedRois.values()[0]->masks[m_z].extent.y() + 1;
        m_roiData->modifyMask(m_selectedRois.values()[0]->masks[m_z], mx, my);
    }

    ///@todo
    //m_appdata.modifyRoi(m_sessionName, roi);
}

void SliceOverlay::newRoi(const float mouseX, const float mouseY, const ModifyRoiAction action)
{
    if (!coordInRoi(mouseX, mouseY)) {
        QString newRoiName = QUuid::createUuid().toString();
        if (!m_appdata.createNewRoi(m_sessionName, newRoiName)) {
            LOG(WARNING) << "Failed to create new ROI.";
            return;
        }
    }
}

QPoint SliceOverlay::viewToWorldCoords(const float x, const float y)
{
    QPoint p;
    p.setX(static_cast<int>(x / m_scaling - m_textureOffsetX));
    p.setY(static_cast<int>(y / m_scaling - m_textureOffsetY));
    return p;
}

QPoint SliceOverlay::worldToViewCoords(const float x, const float y)
{
    QPoint p;
    p.setX(static_cast<int>((x + m_textureOffsetX) * m_scaling));
    p.setY(static_cast<int>((y + m_textureOffsetY) * m_scaling));
    return p;
}

/// This also shifts upper-left by 1 pixel
QRect SliceOverlay::worldToViewCoords(const QRect rect)
{
    QRect outRect;
    outRect.setX(static_cast<int>((rect.x() - 1 + m_textureOffsetX) * m_scaling));
    outRect.setY(static_cast<int>((rect.y() - 1 + m_textureOffsetY) * m_scaling));
    outRect.setRight(static_cast<int>((rect.x() - 1 + rect.width() + m_textureOffsetX) * m_scaling));
    outRect.setBottom(static_cast<int>((rect.y() - 1 + rect.height() + m_textureOffsetY) * m_scaling));
    return outRect;
}

void SliceOverlay::unlinkSelected()
{
    if (m_selectedRois.size() < 1) {
        return;
    }

    ApplicationData::instance().unlinkRoi(m_sessionName,  m_selectedRois.keys().at(0));
}

void SliceOverlay::hoverRoi(const int x, const int y)
{
    for (QList<RoiData::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        int left = (*it)->masks[m_z].extent.x();
        int right = (*it)->masks[m_z].extent.right();
        int top = (*it)->masks[m_z].extent.y();
        int bottom = (*it)->masks[m_z].extent.bottom();
        if (x >= left && x <= right && y >= top && y <= bottom) {
            m_hoveredRoi = (*it);
        }
    }
}

void SliceOverlay::exitHoverRoi()
{
    m_hoveredRoi.reset();
}

///@param z: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceOverlay::changeSlice(const int z)
{
    if (z < 0) {
        return;
    }
    // changing slices, so unselect anything selected in this slice
    m_selectedRois.clear();
    changeMode(SELECT_MODE);

    m_z = static_cast<unsigned int>(z);

    if (m_showAllRois) {
        showAllRois();
    } else {
        m_visibleRois.clear();
    }
}

///@param indexZ: 0-based index
void SliceOverlay::changeSliceIndex(const int indexZ)
{
    changeSlice(indexZ + m_iBase);
}

void SliceOverlay::toggleShowReference()
{
    m_showRef = !m_showRef;
    if (m_showRef) {
        m_refRois = m_roiData->roisAtSession(m_roiData->refSessionName());
    } else {
        m_refRois.clear();
    }
}

void SliceOverlay::setScaleAndOffset(const float scale, const float xOffset, const float yOffset)
{
    m_scaling = scale;
    m_textureOffsetX = xOffset;
    m_textureOffsetY = yOffset;

//    if (m_zoomFactor > 100) {
//        setClip(true);
//    } else {
//        setClip(false);
//    }

    update();
}

void SliceOverlay::toggleEditMode()
{
    (m_mode == EDITMASK_MODE) ? changeMode(SELECT_MODE) : changeMode(EDITMASK_MODE);
    update();
}

void SliceOverlay::changeMode(ActionMode mode)
{
    m_mode = mode;
    if (m_mode == EDITMASK_MODE) {
        m_selectPen = m_editSelectPen;
    } else {
        m_selectPen = m_defaultSelectPen;
    }
}

void SliceOverlay::mergeSelected()
{
    m_appdata.mergeRois(m_sessionName, m_selectedRois.values());
    m_visibleRois.clear();
    m_visibleRois = m_roiData->roisAtSessionFilterZ(m_sessionName, m_z);
    update();
}

