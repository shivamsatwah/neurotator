/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICEOVERLAY_HPP
#define SLICEOVERLAY_HPP

#include <QObject>
#include <QQuickPaintedItem>
#include <QPen>

#include "ApplicationData.hpp"
#include "RoiData.hpp"

enum SelectRoiAction
{
    CLEAR_ACTION = 0,
    LINK_ACTION = 1,
    UNLINK_ACTION = 2,
    MULTISELECT_ACTION = 3
};

enum ModifyRoiAction
{
    TOGGLE_PIXEL = 0,
    SET_PIXEL = 1,
    CLEAR_PIXEL = 2,
};

enum ActionMode
{
    SELECT_MODE = 0,
    EDITMASK_MODE = 1
};

/// A widget to use in QML, intended to be drawn over a SliceImage
class SliceOverlay : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(bool newMask READ isMakingNewMask NOTIFY newMaskChanged)
    Q_PROPERTY(QString roiSelectionPretty READ getRoiSelectionPretty NOTIFY roiSelectionChanged)
    Q_PROPERTY(QString roiSelection READ getRoiSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool selectionExists READ isSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool multiSelectionExists READ isMultiSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool selectionFlagged READ isSelectionFlagged NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool linking READ isLinking NOTIFY linkingChanged)

public:
    explicit SliceOverlay(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);

    Q_INVOKABLE void setSessionName(QString sessionName) { m_sessionName = sessionName; }
    //Q_INVOKABLE RoiData::roi_t roiAtMouse(const int x, const int y);
    Q_INVOKABLE QColor getConfirmedMaskColor() { return m_confirmedPen.color(); }
    Q_INVOKABLE QColor getUnconfirmedMaskColor() { return m_unconfirmedPen.color(); }
    Q_INVOKABLE QColor getRefMaskColor() { return m_refColor; }
    Q_INVOKABLE void initNewMask() { m_makingNewMask = true; }

    bool isMakingNewMask() { return m_makingNewMask; }
    QString getRoiSelectionPretty() { return (isSelection() ? ("[" + m_selectedRois.values()[0]->name + "]") : ""); }
    QString getRoiSelection() { return isSelection() ? m_selectedRois.values()[0]->name : ""; }
    bool isSelection() { return (m_selectedRois.size() == 1); }
    bool isMultiSelection() { return (m_selectedRois.size() > 1); }
    bool isSelectionFlagged() { return (isSelection() ?  m_selectedRois.values()[0]->flagged : false); }
    bool isLinking() const { return m_linking; }

public slots:
    void refresh();
    void roiSelectionRequest(QString sessionKey, QString roiKey);
    void mouseClick(const float mouseX, const float mouseY);
    void selectRoiInSlice(QString roiKey);
    void selectRoiViaMouse(const float mouseX, const float mouseY, const int action = CLEAR_ACTION);
    void modifyRoiPixel(const float mouseX, const float mouseY, const ModifyRoiAction action = SET_PIXEL);
    void newRoi(const float mouseX, const float mouseY, const ModifyRoiAction action);
    void unselectAll() { m_selectedRois.clear(); }
    void hoverRoi(const int x, const int y);
    void exitHoverRoi();
    void showAllRois();
    void hideAllRois();
    void toggleAllRois() { (m_showAllRois) ? hideAllRois() : showAllRois(); }
    void changeSlice(const int z);
    void changeSliceIndex(const int indexZ);
    void toggleShowCentroids() { m_showCentroids = !m_showCentroids; }
    void toggleShowMasks() { m_showMasks = !m_showMasks; }
    void toggleShowMaskOutlines() { m_showMaskOutlines = !m_showMaskOutlines; }
    void toggleShowConfirmed() { m_showConfirmed = !m_showConfirmed; }
    void toggleShowFullyLinked() { m_showFullyLinked = !m_showFullyLinked; }
    void toggleShowReference();
    void toggleShowLabels() { m_showLabels = !m_showLabels; }
    void toggleShowFlagged() { m_showFlagged = !m_showFlagged; }
    void toggleShowFiltered() { m_showFiltered = !m_showFiltered; }
    void resetLinking() { setIsLinking(false); update(); }
    void unlinkSelected();
    void setConfirmedMaskColor(QColor color) {
        m_confirmedPen.setColor(color);
        m_confirmedColor = color.rgba();
    }
    void setUnconfirmedMaskColor(QColor color) {
        m_unconfirmedPen.setColor(color);
        m_unconfirmedColor = color.rgba();
    }
    void setRefMaskColor(QColor color) {
        m_refColor = color.rgba();
    }
    void setScaleAndOffset(const float scale, const float xOffset, const float yOffset);    
    void toggleEditMode();
    void mergeSelected();

signals:
    void newMaskChanged();
    void roiSelectionChanged();
    void linkingChanged();

private:    
    void paintMask(QPainter *painter, RoiData::roi_t roi);
    void paintMaskOutline(QPainter *painter, RoiData::roi_t roi);
    void drawCrosshairs(QPainter *painter, RoiData::roi_t roi);
    void drawEllipse(QPainter *painter, RoiData::roi_t roi);
    QPoint viewToWorldCoords(const float x, const float y);
    QPoint worldToViewCoords(const float x, const float y);
    QRect worldToViewCoords(const QRect rect);
    RoiData::roi_t coordInRoi(const float mouseX, const float mouseY);
    void changeMode(ActionMode mode);
    void setIsLinking(bool flag) {
        if (m_linking != flag) {
            m_linking = flag;
            emit linkingChanged();
        }
    }

    ApplicationData& m_appdata;
    QString m_sessionName;
    QHash<QString, RoiData::roi_t> m_selectedRois;
    QList<RoiData::roi_t> m_visibleRois;
    QList<RoiData::roi_t> m_refRois;
    RoiData::roi_t m_hoveredRoi;
    ActionMode m_mode = SELECT_MODE;
    bool m_showAllRois = true;
    bool m_showCentroids = false;
    bool m_showMasks = true;
    bool m_showMaskOutlines = false;
    bool m_showConfirmed = true;
    bool m_showFullyLinked = true;
    bool m_showLabels = false;
    bool m_showFlagged = true;
    bool m_showFiltered = true;
    bool m_showRef = false;
    bool m_linking = false;
    unsigned int m_z = 0;
    unsigned char m_iBase = 0;
    float m_iBaseF = 0;
    RoiData* m_roiData; ///@todo should be smart pointer
    QPen m_confirmedPen;
    QPen m_unconfirmedPen;
    QRgb m_confirmedColor;
    QRgb m_unconfirmedColor;
    QRgb m_refColor;
    QPen m_confirmedCentroidPen;
    QPen m_unconfirmedCentroidPen;
    QPen m_defaultSelectPen;
    QPen m_editSelectPen;
    QPen m_selectPen;
    QPen m_linkingPen;
    QPen m_lightPen;
    QPen m_labelPen;
    QVector<QRgb> m_maskColorTable;
    QFont m_labelFont;
    float m_scaling = 1.0;
    float m_textureOffsetX = 0;
    float m_textureOffsetY = 0;
    int m_maxLabelWidth = 150;
    int m_labelHeight = 12;
    const int m_maxLabelLines = 2;
    bool m_makingNewMask;
};

#endif // SLICEOVERLAY_HPP

