/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtQuick.Controls 2.4

Rectangle {
    id: tinyColorPicker

    property string title: ""
    property color borderColor: Material.color(Material.Grey, Material.Shade700)
    property color backgroundColor: Material.color(Material.Grey, Material.Shade900)
    property color textColor: Material.color(Material.Grey)
    property real textSize: 4
    property real currRed: 1.0
    property real currGreen: 0.0
    property real currBlue: 0.0
    property real currAlpha: 1.0

//    property alias sizex: width
//    property alias sizey: height

    signal colorChanged(color rgba)

    color: backgroundColor
    border.color: borderColor
    width: 60
    height: (rslider.height + 1) * 4 + titleText.height + 1
    border.width: 1
    clip: true

    Column {
        anchors.fill: parent
        anchors.leftMargin: spaceunit
        anchors.rightMargin: spaceunit
        anchors.topMargin: spaceunit
        anchors.bottomMargin: spaceunit

        Text {
            id: titleText
            text: title
            color: textColor
            font.pointSize: 6
            anchors.bottomMargin: 1
        }
//        Row {
//            Text {
//                text: "R"
//                anchors.leftMargin: spaceunit
//                color: textColor
//                font.pointSize: textSize
//            }

        TinySlider {
            id: rslider
            slideColor: "red"
            value: currRed
            onMoved: {
                //console.log("R slider moved", value);
                currRed = value;
                colorChanged(Qt.rgba(currRed, currGreen, currBlue, currAlpha));
            }
        }
        TinySlider {
            id: gslider
            slideColor: "green"
            value: currGreen
            onMoved: {
                currGreen = value;
                colorChanged(Qt.rgba(currRed, currGreen, currBlue, currAlpha));
            }
        }
        TinySlider {
            id: bslider
            slideColor: "#8888FF"
            value: currBlue
            onMoved: {
                currBlue = value;
                colorChanged(Qt.rgba(currRed, currGreen, currBlue, currAlpha));
            }
        }
        TinySlider {
            id: aslider
            slideColor: "#999999"
            value: currAlpha
            onMoved: {
                currAlpha = value;
                colorChanged(Qt.rgba(currRed, currGreen, currBlue, currAlpha));
            }
        }
    }

}
