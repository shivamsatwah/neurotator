/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import com.bu 1.0

Rectangle {
    id: roiTableCell
    property string name: "?"
    property string sessName: "?"
    property string linkName: ""
    property string linkSessText: ""
    property string pressedIcon: ""
    property bool pressed: mouseArea.pressed
    property color normalColor: "transparent"
    property color hiliteColor: Material.color(Material.Grey, Material.Shade700)
    property color borderColor: Material.color(Material.Grey, Material.Shade600)
    property color borderHiliteColor: Material.color(Material.Grey, Material.Shade400)
    property color textColor: Material.color(Material.Grey, Material.Shade50)
    property color textHiliteColor: "white"
    property color textSelectedColor: "black"

    property int maskX: -1
    property int maskY: -1
    property int maskZ: -1
    property bool confirmed: false
    property bool selected: false
    property bool flagged: false

    signal buttonClicked(real x, real y)
    signal rightButtonClicked(real x, real y)
    signal buttonCtrlClicked(real x, real y)
    signal buttonShiftClicked(real x, real y)

    color: "transparent"
    border.color: borderColor
    width: tableCellWidth
    height: tableCellHeight
    border.width: 1
    anchors.leftMargin: spaceunit
    anchors.rightMargin: spaceunit
    anchors.topMargin: spaceunit
    anchors.bottomMargin: spaceunit

    onSelectedChanged: {
        if (!selected) {
            unselect()
        } else {
            label.color = textSelectedColor;
            buttonPressedEffect.visible = true;
        }
    }

    function unselect() {
        console.log("unselect()");
        if (!selected) {
            innerBG.color = normalColor;
            border.color = borderColor;
            label.color = textColor;
            buttonPressedEffect.visible = false;
        }
    }

    Rectangle {
        id: innerBG
        anchors.fill: parent
        anchors.margins: border.width
        color: normalColor
     }
    RectangularGlow {
        id: buttonPressedEffect
        visible: false
        anchors.fill: roiTableCell
        glowRadius: 8
        spread: 0
        color: "orange"
    }

    Image {
        id: flaggedIcon
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        //anchors.horizontalCenter: parent.horizontalCenter
        visible: flagged
        source: flagIcon
        smooth: true
    }

//    Image {
//        id: buttonIconPressed
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.horizontalCenter: parent.horizontalCenter
//        visible: { pressedIcon != ""; }
//        source: pressedIcon
//    }

    Text {
        id: label
        text: parent.name
        color: textColor
        font.bold: false
        anchors.left: flaggedIcon.right
        anchors.right: parent.right
        anchors.leftMargin: spaceunit //+ smallButtonSize
        anchors.rightMargin: spaceunit
        anchors.verticalCenter: parent.verticalCenter
        clip: true
        font.pointSize: tableCellFontSize
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        propagateComposedEvents: true
        //preventStealing: true
        hoverEnabled: true
        cursorShape: appdata.linkInProgress ? Qt.CrossCursor : Qt.ArrowCursor

        onEntered: {
            if (!selected) {
                innerBG.color = hiliteColor;
                parent.border.color = borderHiliteColor;
                label.color = textHiliteColor;
            }
        }
        onExited: {
            if (!selected) {
                innerBG.color = normalColor;
                parent.border.color = borderColor;
                label.color = textColor;
                buttonPressedEffect.visible = false;
            }
        }
        onPressed: {
            buttonPressedEffect.visible = true;

        }
        onReleased: {
            if (!selected) {
                buttonPressedEffect.visible = false;
            }
        }
        onPressAndHold: mouse.accepted = false
        onClicked: {
            if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ControlModifier)) {
                roiTableCell.buttonCtrlClicked(mouse.x, mouse.y);
            } else if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ShiftModifier)) {
                roiTableCell.buttonShiftClicked(mouse.x, mouse.y);
            } else if (mouse.button === Qt.RightButton) {
                roiTableCell.rightButtonClicked(mouse.x, mouse.y);
            } else if (mouse.button === Qt.LeftButton) {
                roiTableCell.buttonClicked(mouse.x, mouse.y);
            }

            label.color = textSelectedColor;
            buttonPressedEffect.visible = true;
        }
    }
    ToolTip {
        visible: mouseArea.containsMouse
        delay: toolTipDelay
        timeout: toolTipTimeout
        font.pointSize: toolTipFontSize
        text: roiTableCell.name + "\n" + roiTableCell.sessName + "\nLink:" + roiTableCell.linkName + "\n" + roiTableCell.linkSessText
    }
}

