/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

Rectangle {
    id: projectWizard
    //visible: false
    anchors.fill: parent
    //color: Material.color(Material.Grey, Material.Shade700)
    gradient: Gradient {
        GradientStop { position: 1.0; color: Material.color(Material.Grey, Material.Shade500) }
        GradientStop { position: 0.0; color: Material.color(Material.Grey, Material.ShadeA100) }
    }

    signal newProject(url projectUrl, string projectName)
    signal openProject(url projectUrl)

    ListModel {
        id: recentProjectsModel
    }

    Component.onCompleted: {
        var recents = appdata.getRecentProjects();
        var recentNames = appdata.getRecentProjectNames();
        for (var i = 0; i < recents.length; ++i) {
            recentProjectsModel.append({"path": recents[i], "name": recentNames[i].toString()});
        }
    }

    Text {
        text: "Neurotator"
        font.bold: true
        font.pointSize: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        color: lightOrangeColor
    }

    Pane {
        width: 1000
        height: 800

        Material.elevation: 6

        anchors.centerIn: parent

        ColumnLayout {
            anchors.fill: parent
            RowLayout {
                Layout.fillWidth: true

                Button {
                    id: openButton
                    text: qsTr("Open Project...")

                    onClicked: {
                        projectFileDialog.setFolder(appdata.getRecentProjectDir());
                        projectFileDialog.open();
                    }
                }
                Rectangle {
                    //spacer
                    color: "transparent"
                    width: 100
                }
                Row {
                    spacing: 8
                    Column {
                        spacing: 2
                        Label {
                            text: qsTr("New Project")
                            color: lightOrangeColor
                        }
                        Item {
                            id: inputItem
                            width: 500
                            height: 30

                            Rectangle {
                                anchors.fill: parent
                                color: Material.color(Material.Grey, Material.Shade900)
                                border.color: projectNameInput.text === "" ? "red" : Material.color(Material.Grey, Material.Shade700)
                            }

                            TextInput {
                                id: projectNameInput
                                text: "name"
                                width: inputItem.width
                                anchors.margins: 2
                                anchors.verticalCenter: parent.verticalCenter
                                padding: 2
                                color: "white"
                                font.pointSize: 12
                                clip: true
                                selectByMouse: true
                                onEditingFinished: {
                                    projectNameLabel.visible = true;
                                    appdata.projectName = text;
                                    refreshRecentsMenu();
                                }
                            }
                        }
                        Item {
                            //Layout.preferredWidth: 144
                            width: 500
                            height: smallButtonSize
                            property bool userChosen: false

                            Rectangle {
                                anchors.fill: parent
                                anchors.rightMargin: smallButtonSize
                                color: Material.color(Material.Grey, Material.Shade900)
                                border.color: Material.color(Material.Grey, Material.Shade700)
                            }

                            TextInput {
                                id: projectUrlInput
                                text: appdata.getRecentProjectDir() + "/" + projectNameInput.text + ".json"
                                readOnly: true
                                anchors.margins: 2
                                anchors.verticalCenter: parent.verticalCenter
                                padding: 2
                                color: "light gray"
                                font.pointSize: 10
                                clip: true
                                selectByMouse: true
                                onEditingFinished: {
                                    projectNameLabel.visible = true;
                                    appdata.projectName = text;
                                    refreshRecentsMenu();
                                }
                            }
                            SmallButton {
                                id: changePathButton
                                anchors.right: parent.right
                                anchors.rightMargin: 0

                                icon: "qrc:/third-party/material-design-icons-master/file/1x_web/ic_folder_open_white_18dp.png"
                                onButtonClicked: {
                                    projectSaveFileDialog.setFolder(appdata.getRecentProjectDir());
                                    projectSaveFileDialog.open();
                                }
                            }
                        }
                    }
                    Button {
                        id: newButton
                        text: qsTr("Create")
//                        anchors.topMargin: 20
//                        anchors.bottom: parent.bottom
                        anchors.verticalCenter: parent.verticalCenter

                        onClicked: {
                            //fileDialog.open();
                            var projectUrl = "NewProject.json";
                            projectWizard.newProject(projectUrlInput.text, projectNameInput.text)
                        }
                    }
                }

            }
            HorDivider {
                id: divider
                Layout.fillWidth: true
            }
            Label {
                text: qsTr("Recent Projects")
                Layout.fillWidth: true
                color: lightOrangeColor
            }
            ScrollView {
                id: recentsScrollView
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true

                ListView {
                    model: recentProjectsModel
                    delegate: ItemDelegate {
                        width: recentsScrollView.width - 32 //pathText.width + 32
                        height: 66
                        text: model.name
                        font.bold: true
                        onClicked: {
                            projectWizard.openProject(model.path); // signal
                        }
                        Text {
                            id: pathText
                            text: model.path
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 2
                            x: 16
                            color: Material.color(Material.Grey, Material.Shade500)
                        }
                    }
                }
            }
        }
    }

    FileDialog {
        id: projectSaveFileDialog
        title: "New Project"
        selectExisting: false
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            appdata.setRecentProjectDir(projectSaveFileDialog.folder);
            projectUrlInput.text = projectSaveFileDialog.fileUrl;
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    FileDialog {
        id: projectFileDialog
        title: "Open Project"
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            appdata.setRecentProjectDir(projectFileDialog.folder);
            projectWizard.openProject(projectFileDialog.fileUrl); // signal
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }
}
