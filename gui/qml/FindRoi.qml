/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Popup {
    id: findPopup
    modal: false
    padding: 1
    closePolicy: Popup.CloseOnEscape
    height: 500

    ColumnLayout {
        anchors.fill: parent

        Rectangle {
            id: titleBar
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
            Layout.leftMargin: 8

            height: smallButtonSize
            color: "transparent"

            Label {
                text: "Find ROIs"
                anchors.left: titleBar.left
                anchors.verticalCenter: parent.verticalCenter
            }

            SmallButton {
                id: closeButton
                anchors.right: titleBar.right
                anchors.verticalCenter: parent.verticalCenter
                icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_close_white_18dp.png"
                onButtonClicked: {
                    findPopup.close();
                }
            }
        }

        Row {
            Layout.leftMargin: 8

            Label {
                text: "Name:"
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: spaceunit
                font.pointSize: 8
            }

            Item {
                id: inputItem
                width: 200
                height: smallButtonSize

                Rectangle {
                    anchors.fill: parent
                    anchors.rightMargin: smallButtonSize
                    color: Material.color(Material.Grey, Material.Shade900)
                    border.color: Material.color(Material.Grey, Material.Shade700)
                }

                TextInput {
                    id: patternInput
                    anchors.verticalCenter: parent.verticalCenter
                    padding: 2
                    color: "light gray"
                    width: inputItem.width
                    font.pointSize: 10
                    clip: true
                    selectByMouse: true
                    onEditingFinished: {
                        appdata.searchRois(sourceSelect.currentText, patternInput.text);
                    }
                }
                SmallButton {
                    id: findButton
                    anchors.right: parent.right
                    anchors.rightMargin: 0

                    icon: searchIcon
                    onButtonClicked: {
                        appdata.searchRois(sourceSelect.currentText, patternInput.text);
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Search")
                    }
                }
            }
        }
        ComboBox {
            id: sourceSelect
            Layout.minimumWidth: 216
            Layout.margins: 0

            font.pointSize: 8
            model: appdata.searchableSources
            onActivated: {
            }
        }
        HorDivider {
            id: divider
            Layout.fillWidth: true
        }
        ScrollView {
            id: resultsScrollView
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true

            ListView {
                id: resultsListView
                model: appdata.searchResults
                delegate: ItemDelegate {
                    width: resultsScrollView.width - smallButtonSize
                    implicitHeight: 32

                    onClicked: {
                        appdata.selectRoiFromSlice(model.session, model.name);
                    }
                    Text {
                        text: model.name
                        //font.bold: true
                        font.pointSize: 10
                        y: 2
                        x: 8
                        color: lightOrangeColor
                    }
                    Text {
                        id: pathText
                        text: model.session
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 2
                        x: 8
                        color: Material.color(Material.Grey, Material.Shade500)
                    }
                    SmallButton {
                        id: stackButton
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.Center
                        visible: model.hasStack

                        icon: stackIcon
                        onButtonClicked: {
                            appdata.selectRoiInSlice(model.session, model.name);
                        }
                    }
                }
            }
        }

    }
}
