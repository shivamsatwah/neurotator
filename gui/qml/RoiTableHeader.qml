import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

Rectangle {
    property alias sessName: nameText.text
    //color: "transparent"
    color: Material.color(Material.Grey, Material.Shade500)
    border.color: Material.color(Material.Grey, Material.Shade900)
    border.width: 1
    width: tableColWidth
    height: 32
    z: 2

    Text {
        id: nameText
        text: "" //appdata.getSessionName(0) //ListView.view.columnName
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        //anchors.leftMargin: spaceunit
        //width: tableCellWidth
        color: "black"
        clip: true
    }
}

