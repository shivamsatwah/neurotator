/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle {
    id: roiTable
    property string label
    property bool isFocus: false
    property bool isLoading: true
    property bool needsRecreate: false

    property color borderColor: "light gray"
    property color prevColor: borderColor
    property color syncColor: "red"
    property color focusColor: "lightblue"

    property RoiDataModel confirmedModel: appdata.confirmedModel
    property string referenceName: appdata.refSession
    property real headerModel: confirmedModel.columnCount()
    property real numSessionsModel: confirmedModel.columnCount()
    property real numRefRowsModel: confirmedModel.rowCount()
    property var prevSelection: ({})
    property int confirmedLastIndex: 0
    property bool headerActive: true

    property int prevZ: highestZ
    property real gammaVal: 1.0
    property real minWidth: 250
    property real minHeight: 600

    onWidthChanged: if (width < minWidth) width = minWidth
    onHeightChanged: if (height < minWidth) height = minWidth

    width: 400
    height: 710
    z: highestZ
    color: Material.color(Material.Grey, Material.Shade800)
    radius: 3
    // this is for the glow/focus effect
    border.width: 2
    border.color: focusColor

    signal destroying(bool recreate);
    signal minimizing();

    function refreshConfirmedTable() {
        numRefRowsModel = 0;
        numRefRowsModel = confirmedModel.rowCount();
    }

    function updateRef() {
        selectRef.model = confirmedModel.columnNames();
    }

    function updateModels() {
        updateConfirmed();

        headerModel = confirmedModel.columnCount();
        headerActive = false;
        headerActive = true;

        // tell the table on the bottom to refresh since its column order may have changed
        numSessionsModel = 0;
        numSessionsModel = confirmedModel.columnCount();
        unconfirmedRepeater.update();
    }

    function updateConfirmed() {
        //console.log("RoiTable::updateConfirmed");
        numRefRowsModel = 0;
        numRefRowsModel = confirmedModel.rowCount();
        confirmedList.currentIndex = -1;
        confirmedList.currentIndex = confirmedLastIndex;
        confirmedList.update();
    }

    function selectUnconfirmedItem(session, roi) {
        var sessionIndex = confirmedModel.getColumnIndex(session);
        //console.log("RoiTable::selectUnconfirmedItem at session index ", sessionIndex);

        numSessionsModel = 0;
        numSessionsModel = confirmedModel.columnCount();

        unconfirmedRepeater.itemAt(sessionIndex).currentIndex = -1;
        unconfirmedRepeater.itemAt(sessionIndex).currentIndex = roi;

        unconfirmedRepeater.itemAt(sessionIndex).positionViewAtIndex(roi, ListView.Center);

        unconfirmedRepeater.update();
    }

    function selectConfirmedItem(roi) {
        //console.log("RoiTable::selectConItem at ", roi);
        numRefRowsModel = 0;
        numRefRowsModel = confirmedModel.rowCount();
        confirmedList.currentIndex = -1;
        confirmedList.currentIndex = roi;
        confirmedList.positionViewAtIndex(roi, ListView.Center);
        confirmedLastIndex = roi;
        confirmedList.update();
    }

    function selectRefItem(session, roi) {
        if (session === referenceName) {
//            console.log("RoiTable::selectRefItem: is ref ", session);
//            console.log("RoiTable::selectRefItem: roi ", roi);
            confirmedList.currentIndex = 0;
            confirmedList.currentIndex = roi;
            confirmedLastIndex = roi;
        }
    }

    Component.onCompleted: {
        appdata.refSessionChanged.connect(updateModels);
        appdata.removedSession.connect(updateModels);
        appdata.removedSession.connect(updateRef);

        appdata.roiConfirmedSelected.connect(selectConfirmedItem);
        appdata.roiConfirmedDirty.connect(updateConfirmed);

        appdata.linkedRoiSelectedIndex.connect(selectConfirmedItem);

        appdata.roiSelectedIndex.connect(selectUnconfirmedItem);
        //appdata.roiSelectedIndex.connect(selectRefItem);
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        propagateComposedEvents: true

        // don't catch hover over the whole rectangle as Qt doesn't propagate hover down the z stack

        drag.target: roiTable
        drag.axis: Drag.XAndYAxis
        onPressed: {
            prevColor = parent.border.color;
            parent.border.color = focusColor;
            parent.anchors.centerIn = undefined;
            roiTable.z = ++highestZ;
            selectRef.update();
        }
        onReleased: {
            parent.border.color = prevColor;
            parent.isFocus = true;
            parent.focus = true;
        }
        onPressAndHold: mouse.accepted = false
        onClicked: {
            if (mouse.button & Qt.RightButton) {
                tableMenu.popup();
            }
        }
    }
    Menu {
        id: tableMenu
        Menu {
            title: "Flags"
            MenuItem {
                text: "Delete All Flagged"
                onTriggered: {
                    console.log("Del All Flagged");
                    appdata.deleteAllFlaggedRois();
                }
            }
        }
    }

    Rectangle
    {
        id: roiTableContent
        color: parent.color
        border.width: 1
        border.color: Material.color(Material.Grey, Material.Shade900)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.bottomMargin: parent.border.width

        Rectangle {
            id: titlebar
            height: titlebarunit
            color: roiTableContent.color
            anchors.left: parent.left
            anchors.leftMargin: parent.border.width
            anchors.right: parent.right
            anchors.rightMargin: parent.border.width
            anchors.top: parent.top
            anchors.topMargin: parent.border.width

            Text {
                id: titleText
                maximumLineCount: 1
                clip: true
                width: parent.width - closeButton.width - spaceunit
                anchors.left: titlebar.left
                anchors.leftMargin: spaceunit
                anchors.verticalCenter: titlebar.verticalCenter
                text: "ROIs"
                color: "white"
                font.bold: true
                font.pointSize: 10
            }

            SmallButton {
                id: openFindButton
                icon: searchIcon
                anchors.right: closeButton.left
                anchors.centerIn: titlebar.Center
                onButtonClicked: {
                    findRoi.open();
                }
                ToolTip {
                    visible: parent.hovered
                    delay: toolTipDelay
                    timeout: toolTipTimeout
                    font.pointSize: toolTipFontSize
                    text: qsTr("Find...")
                }
            }

            SmallButton {
                id: closeButton
                icon: "qrc:/gui/icons/minimize_white_18dp.png"
                anchors.right: titlebar.right
                anchors.centerIn: titlebar.Center
                onButtonClicked: {
                    roiTable.minimizing();
                    //roiTable.destroy();
                    roiTable.visible = false;
                }
            }
        }
        HorDivider {
            id: hordiv
            anchors.left: roiTableContent.left
            anchors.right: roiTableContent.right
            anchors.top: titlebar.bottom
            anchors.leftMargin: spaceunit
            anchors.rightMargin: spaceunit
        }

        ColumnLayout {
            id: toolArea
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: hordiv.bottom
            anchors.bottom: parent.bottom
            anchors.leftMargin: parent.border.width + spaceunit
            anchors.rightMargin: parent.border.width + spaceunit
            anchors.topMargin: spaceunit
            anchors.bottomMargin: parent.border.width + spaceunit

            ComboBox {
                id: selectRef
                //width: 200
                Layout.minimumWidth: 200

                font.pointSize: 10
                visible: !appdata.refExists
                model: confirmedModel.columnNames()
                displayText: "Set Reference"

                onActivated: {
                    //refIsSet = true;
                    referenceName = currentText;
                    appdata.refSession = referenceName;
                }
            }

            Text {
                id: confirmedLabel
                text: "Confirmed"
                color: Material.color(Material.Grey)
                font.pointSize: labelSize
                clip: true
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: roiTableContent.border.width + spaceunit
            }

            Rectangle {
                id: roiTableView

                color: "transparent"
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 180
                Layout.minimumHeight: 150
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade900)

                ListView {
                    id: confirmedList
                    anchors.fill: parent
                    anchors.margins: 1                    
                    model: numRefRowsModel
                    highlightFollowsCurrentItem: true
                    highlightMoveDuration: 500
                    clip: true
                    contentWidth: confirmedModel.columnCount() * tableCellWidth
                    flickableDirection: Flickable.AutoFlickDirection;
                    keyNavigationEnabled: true
                    focus: true
                    boundsBehavior: Flickable.StopAtBounds
                    ScrollBar.horizontal: ScrollBar { id: hbar; active: vbar.active }
                    ScrollBar.vertical: ScrollBar { id: vbar; active: hbar.active }
                    headerPositioning: ListView.OverlayHeader
                    header: Loader { id: headerLoader; sourceComponent: header1; active: roiTable.headerActive}

                    delegate: Column
                    {
                        id: cc
                        property int i: index

                        Row {
                            Text {
                                id: linksCell
                                color: Material.color(Material.Grey, Material.Shade50)
                                clip: true
                                font.pointSize: tableCellFontSize
                                text: confirmedModel.refLinkCount(cc.i)
                                width: smallButtonSize
                                height: tableCellHeight
                                horizontalAlignment: Text.AlignHCenter
                            }
                            Repeater {
                                model: confirmedModel.columnCount()

                                RoiTableCell {
                                    id: tableCell
                                    name: confirmedModel.dataAt(cc.i, index)
                                    linkName: confirmedModel.getLinkName(sessName, name)
                                    property string columnName: confirmedModel.columnNameAt(index)
                                    //property bool linkInProgress: appdata.linkInProgress
                                    selected: confirmedModel.isSelected(sessName, name)
                                    flagged: confirmedModel.isFlagged(sessName, name)
                                    sessName: columnName
                                    normalColor: { (index % 2 == 0) ? "#222222" : "#303030"}

                                    onButtonClicked: {
                                         if (appdata.selectRoi(columnName, confirmedModel.dataAt(cc.i, index))) {
                                            selectConfirmedItem(cc.i);
                                        } else {
                                            console.log("Blank cell");
                                        }
                                    }
                                    onButtonCtrlClicked: {
                                        appdata.linkRoi(columnName, name);
                                        if (appdata.selectRoi(columnName, confirmedModel.dataAt(cc.i, index))) {
                                            selectConfirmedItem(cc.i);
                                        }
                                    }
                                    onButtonShiftClicked: {
                                        appdata.unlinkRoi(columnName, name);
                                        if (appdata.selectRoi(columnName, confirmedModel.dataAt(cc.i, index))) {
                                            selectConfirmedItem(cc.i);
                                        }
                                    }
                                    onRightButtonClicked: {
                                        console.log("cell right click");
                                        if (appdata.selectRoi(columnName, confirmedModel.dataAt(cc.i, index))) {
                                            //selectConfirmedItem(cc.i);
                                        }
                                        cellContextMenu.popup();

                                        // clear link state in case user was in the middle of a link operation
                                        //appdata.clearLinkState();
                                    }
                                    Menu {
                                        id: cellContextMenu
                                        MenuItem {
                                            text: "Rename"
                                            onTriggered: {
                                                appdata.renameRoi(columnName, name, "testName");
                                            }
                                        }
                                        MenuItem {
                                            text: "Unlink"
                                            onTriggered: {
                                                appdata.unlinkRoi(columnName, name);
                                            }
                                        }
                                        MenuItem {
                                            text: "Flag/Unflag"
                                            onTriggered: {
                                                appdata.toggleFlag(columnName, name);
                                            }
                                        }
                                        MenuItem {
                                            text: "Delete"
                                            onTriggered: {
                                                appdata.deleteRoi(columnName, name);
                                            }
                                        }
                                    }
                                    Shortcut {
                                        sequence: "U"
                                        onActivatedAmbiguously: {
                                            if (activeFocus) {
                                                console.log("Cell key: Attempting to unlink...");
                                                appdata.unlinkRoi(columnName, name);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Component.onCompleted: {
                        confirmedList.currentIndex = 0;
                        headerItem.z = 55; // hack to fix bug of header below cells
                        console.log("Confirmed onCompleted: confirmedList.count == ", confirmedList.count);                        
                    }
                }
                Component {
                    id: header1

                    Rectangle {
                        id: headerContent
                        color: Material.color(Material.Grey, Material.Shade600)
                        border.color: Material.color(Material.Grey, Material.Shade900)
                        border.width: 1
                        width: parent.width
                        height: smallButtonSize

                        Row {                    
                            SmallButton {
                                id: linkSortButton
                                icon: linkIcon
                                alwaysShowBorder: true
                                onButtonClicked: {
                                    console.log("link sort button");
                                    appdata.sortByLink();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Sort")
                                }
                            }
                            Repeater {
                                id: header1repeater
                                model: headerModel
                                Column {
                                    Rectangle {
                                        id: headerCell
                                        width: tableCellWidth
                                        height: headerContent.height
                                        color: Material.color(Material.Grey, Material.Shade500)
                                        border.color: Material.color(Material.Grey, Material.Shade900)
                                        clip: true

                                        Image {
                                            id: sessionFlagsIcon
                                            anchors.verticalCenter: headerCell.verticalCenter
                                            anchors.left: headerCell.left
                                            anchors.leftMargin: 2
                                            source: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_collections_bookmark_black_18dp.png"
                                            smooth: true
                                            visible: {referenceName === sesLabel.text;}
                                        }
                                        Text {
                                            id: sesLabel
                                            text: confirmedModel.headerData(index, Qt.Horizontal)
                                            anchors.verticalCenter: headerCell.verticalCenter
                                            anchors.left: sessionFlagsIcon.right
                                            anchors.leftMargin: spaceunit
                                            width: tableCellWidth

                                            color: "black"
                                            clip: true
                                            font.bold: {referenceName === sesLabel.text;}
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Text {
                id: unconfirmedLabel
                text: "Unconfirmed"
                color: Material.color(Material.Grey)
                font.pointSize: labelSize
                clip: true
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: roiTableContent.border.width + spaceunit
                Layout.topMargin: spaceunit
                Layout.bottomMargin: spaceunit
            }

            Rectangle {
                id: roiTableView2

                color: "transparent"

                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: tableColWidth
                Layout.minimumHeight: 150
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade900)
                clip: true

                Row {
                    clip: true

                    Repeater {
                        id: unconfirmedRepeater
                        model: numSessionsModel
                        clip: true

                        ListView {
                            id: unconfirmedList
                            property int colIndex: index
                            property string columnName: confirmedModel.columnNameAt(colIndex)
                            property RoiDataModel umodel: appdata.getUnconfirmedModel(colIndex)
                            property var prevSel: ({})
                            model: umodel
                            width: tableColWidth
                            height: roiTableView2.height
                            highlightFollowsCurrentItem: true
                            highlightMoveDuration: 500
                            // this might need absolute screen coords? instead using the positionViewAtIndex() func in selectUnconfirmedItem()
//                            highlightRangeMode: ListView.ApplyRange
//                            preferredHighlightBegin: roiTableView2 / 2 - tableCellHeight * 2
//                            preferredHighlightEnd: roiTableView2 / 2 + tableCellHeight * 2
                            clip: true
                            contentWidth: tableCellWidth
                            flickableDirection: Flickable.AutoFlickDirection;
                            keyNavigationEnabled: true
                            focus: true
                            boundsBehavior: Flickable.StopAtBounds
                            ScrollBar.horizontal: ScrollBar { id: uhbar; active: uvbar.active }
                            ScrollBar.vertical: ScrollBar { id: uvbar; active: uhbar.active }
                            headerPositioning: ListView.OverlayHeader
                            header: RoiTableHeader {
                                sessName: columnName
                            }

                            delegate: Column
                            {
                                id: ucc
                                property int i: index
                                property int colIndex: unconfirmedList.colIndex
                                property RoiDataModel umodel: unconfirmedList.umodel
                                property string columnName: unconfirmedList.columnName

                                RoiTableCell {
                                    id: unconfirmedCell
                                    name: umodel.dataAtSingleCol(unconfirmedList.columnName, ucc.i)
                                    sessName: unconfirmedList.columnName
                                    linkName: confirmedModel.getLinkName(sessName, name)
                                    linkSessText: confirmedModel.getLinkSessionText(sessName, name)
                                    selected: confirmedModel.isSelected(sessName, name)
                                    flagged: umodel.isFlagged(sessName, name)
                                    visible: !confirmedModel.isLinked(sessName, name)                                    

                                    //normalColor: { (ucc.i % 2 == 0) ? "#222222" : "#303030"}
                                    normalColor: "#222222"
                                    onButtonClicked: {
                                        appdata.selectRoi(ucc.columnName, name, true);
                                        selectUnconfirmedItem(ucc.columnName, ucc.i);
                                        if (prevSel) {
                                            prevSel.selected = false;
                                        }
                                        prevSel = unconfirmedCell
//                                        selected = true;
                                    }
                                    onButtonCtrlClicked: {
                                        if (prevSel) {
                                            prevSel.selected = false;
                                        }
                                        prevSel = unconfirmedCell
                                        selected = true;
                                        appdata.linkRoi(ucc.columnName, name);
                                    }
                                    onButtonShiftClicked: {
                                        if (prevSel) {
                                            prevSel.selected = false;
                                        }
                                        prevSel = unconfirmedCell;
                                        selected = true;
                                        appdata.unlinkRoi(ucc.columnName, name);
                                    }
                                    onRightButtonClicked: {
                                        console.log("onRightButtonClicked");
                                        if (prevSel) {
                                            prevSel.selected = false;
                                        }
                                        prevSel = unconfirmedCell;
                                        selected = true;
                                        unCellContextMenu.popup();

                                        // clear link state in case user was in the middle of a link operation
                                        appdata.clearLinkState();
                                    }
                                    Menu {
                                        id: unCellContextMenu
                                        MenuItem {
                                            text: "Unlink"
                                            onTriggered: {
                                                if (unconfirmedCell.linkName != "") {
                                                    appdata.unlinkRoi(ucc.columnName, unconfirmedCell.name);
                                                }
                                            }
                                        }
                                        MenuItem {
                                            text: "Flag/Unflag"
                                            onTriggered: {
                                                appdata.toggleFlag(ucc.columnName, unconfirmedCell.name);
                                            }
                                        }
                                        MenuItem {
                                            text: "Delete"
                                            onTriggered: {
                                                appdata.deleteRoi(ucc.columnName, unconfirmedCell.name);
                                            }
                                        }
                                    }
                                }
                            }

                            Component.onCompleted: {
                                unconfirmedList.currentIndex = 0;
                                //console.log("** unconfirmedList columnName:", columnName);
                            }
                        }
                    }
                }

            }
            Rectangle {
                color: Material.color(Material.Grey, Material.ShadeA100)
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade800)
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: 100
                Layout.minimumHeight: 10
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                clip: true

                Text {
                    id: lastCmdText
                    text: appdata.cmdText;
                    color: Material.color(Material.Grey)
                    font.pointSize: labelSize
                    clip: true
                    anchors.left: parent.left
                    anchors.topMargin: spaceunit
                    anchors.leftMargin: roiTableContent.border.width + spaceunit
                }
            }
        }
    }

    FindRoi {
        id: findRoi
        visible: false
        x: parent.width
    }

    BusyIndicator {
        id: busy
        visible: false
        running: isLoading
        anchors.fill: parent
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // resizing grips

    // left side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: -2
        width: spaceunit + 2
        MouseArea {
            id: leftGrip
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = roiTable.width - delta;
                    var newX = roiTable.x + delta;
                    if (newWidth < roiTable.minWidth || newX < 0) {
                        return;
                    }
                    roiTable.x = newX;
                    roiTable.width = newWidth;
                    confirmedList.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    // right side resize grip
    Rectangle {
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: -2
        width: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    //var delta = Math.max(mouseX, mouseY)
                    var delta = mouseX;
                    var newWidth = roiTable.width + delta;
                    if (newWidth < roiTable.minWidth) {
                        return;
                    }
                    roiTable.width = newWidth;
                    confirmedList.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    // top side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = roiTable.height - delta;
                    var newY = roiTable.y + delta;
                    if (newHeight < roiTable.minHeight || newY < 0) {
                        return;
                    }
                    roiTable.y = newY;
                    roiTable.height = newHeight;
                    confirmedList.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    // bottom side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = roiTable.height + delta;
                    if (newHeight < roiTable.minHeight) {
                        return;
                    }
                    roiTable.height = newHeight;
                    confirmedList.update();
                    toolArea.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
}
