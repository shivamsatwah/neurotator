/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle
{
    id: sliceEdit
    width: sliceImage.width + 2;
    height: sliceImage.height + 2;
    border.width: 1
    color: "transparent"
    border.color: Material.color(Material.Grey, Material.Shade700)

    property real gammaVal: 1.0

    property alias sliceImage: sliceImage
    property alias sliceOverlay: sliceOverlay
    property alias overlayMouseArea: overlayMouseArea
    //property alias state: state

    property alias rgbEffect: rgbEffect
    property alias levelsEffect: levelsEffect
    property alias brightnessContrastEffect: brightnessContrastEffect
    property alias colorizeEffect: colorizeEffect

    function redrawSlice() {
        sliceImage.update();
        //sliceOverlay.update();
    }

    function saveToImage(urlNoProtocol) {
        // usually colorizeEffect is not visible, so we'd save from the previous filter
        if (colorizeEffect.visible) {
            colorizeEffect.grabToImage(function(result) {
                                      result.saveToFile(urlNoProtocol);
                                  });
        } else {
            brightnessContrastEffect.grabToImage(function(result) {
                                      result.saveToFile(urlNoProtocol);
                                  });
        }
    }

    function rgbChannelCompensate()
    {
        var count = 3;
        if (rgbEffect.redChannel == 0.0) {
            count--;
        }
        if (rgbEffect.greenChannel == 0.0) {
            count--;
        }
        if (rgbEffect.blueChannel == 0.0) {
            count--;
        }
        var channelValue = 3.0 / count;

        if (rgbEffect.redChannel != 0.0) {
            rgbEffect.redChannel = channelValue;
        }
        if (rgbEffect.greenChannel != 0.0) {
            rgbEffect.greenChannel = channelValue;
        }
        if (rgbEffect.blueChannel != 0.0) {
            rgbEffect.blueChannel = channelValue;
        }
    }

    /// SliceImage is the access into the hyperstack
    SliceImage
    {
        id: sliceImage
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 1
        anchors.topMargin: 1
        visible: false
    }
//        Desaturate {
//            id: desaturateEffect
//            anchors.fill: sliceImage
//            source: sliceImage
//            visible: true
//            desaturation: 0.0
//        }
    ShaderEffectSource {
        id: shaderSource
        sourceItem: sliceImage
    }
    ShaderEffect {
        id: rgbEffect
        anchors.fill: sliceImage
        property variant source: shaderSource
        property real redChannel: 1.0
        property real greenChannel: 1.0
        property real blueChannel: 1.0
        property real greyAmount: 1.0
        visible: true
        fragmentShader: "
            varying highp vec2 qt_TexCoord0;
            uniform sampler2D source;
            uniform lowp float qt_Opacity;
            uniform lowp float redChannel;
            uniform lowp float greenChannel;
            uniform lowp float blueChannel;
            uniform lowp float greyAmount;
            vec4 sample = texture2D(source, qt_TexCoord0);
            float grey = 0.333 * sample.r * redChannel + 0.333 * sample.g * greenChannel + 0.333 * sample.b * blueChannel;
            float rs = sample.r * redChannel;
            float gs = sample.g * greenChannel;
            float bs = sample.b * blueChannel;
            void main() {
                gl_FragColor = vec4(rs * greyAmount + grey * (1.0 - greyAmount), gs * greyAmount + grey * (1.0 - greyAmount), bs * greyAmount + grey * (1.0 - greyAmount), 1.0) * qt_Opacity;
            }"
    }
    LevelAdjust {
        id: levelsEffect
        anchors.fill: sliceImage
        source: rgbEffect
        maximumInput: sliceImage.getMaxColorValue()
        visible: true
    }
    BrightnessContrast {
        id: brightnessContrastEffect
        anchors.fill: sliceImage
        source: levelsEffect
        visible: true
    }
    Colorize {
        id: colorizeEffect
        anchors.fill: sliceImage
        source: brightnessContrastEffect
        visible: false
        hue: 0.0
    }

    SliceOverlay
    {
        id: sliceOverlay
        anchors.fill: sliceImage
    }


    ToolStatePanel
    {
        id: toolStatePanel
        visible: false
        anchors.left: sliceImage.left
        anchors.right: sliceImage.right
        anchors.bottom: sliceImage.bottom
    }

    MouseArea {
        id: overlayMouseArea
        anchors.fill: sliceOverlay
        propagateComposedEvents: true
        acceptedButtons: Qt.AllButtons
        cursorShape: appdata.linkInProgress ? Qt.CrossCursor : Qt.ArrowCursor

        property real px: 0
        property real py: 0
        property bool middleButton: false
        property bool rightButton: false

        onPositionChanged: {
            if (middleButton || rightButton) {
                if (px == 0) {
                    px = mouseX;
                    py = mouseY;
                }

                sliceImage.adjustOffset((mouseX - px) * 0.8, (mouseY - py) * 0.8);
                redrawSlice();
                px = mouseX;
                py = mouseY;
            }
        }
        onPressed: {
            if (mouse.button & Qt.MiddleButton) {
                overlayMouseArea.middleButton = true;
            } else {
                overlayMouseArea.middleButton = false;
            }
            if (mouse.button & Qt.RightButton) {
                overlayMouseArea.rightButton = true;
            } else {
                overlayMouseArea.rightButton = false;
            }
        }
        onReleased: {
            px = 0;
            py = 0;
        }
        onClicked: {
            // Ctrl+click
            if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ControlModifier)) {
                sliceOverlay.selectRoiViaMouse(mouse.x, mouse.y, 1);
            // Shift+click
            } else if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ShiftModifier)) {
                sliceOverlay.selectRoiViaMouse(mouse.x, mouse.y, 2);
            // Alt+click
            } else if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.AltModifier)) {
                sliceOverlay.selectRoiViaMouse(mouse.x, mouse.y, 3);
            // Normal click
            } else if (mouse.button === Qt.LeftButton)  {
                sliceOverlay.mouseClick(mouse.x, mouse.y);
            }
        }
        onWheel: {
            if (wheel.modifiers & Qt.ControlModifier) {
                sliceImage.zoomFactor += wheel.angleDelta.y / 40;
                redrawSlice();
            } else {
                var newZ = zNavSlider.value + wheel.angleDelta.y / 120;
                if (newZ < 1) {
                    newZ = 1;
                }
                if (newZ > sliceImage.getMaxZ()) {
                    newZ = sliceImage.getMaxZ();
                }

                sliceImage.gotoZ(newZ);
                editButton.clearEditMode();
                redrawNav();
            }
        }
    }

    states: [
        State {
            name: "select"
            PropertyChanges {
            }
        },
        State {
            name: "link"
            when: sliceOverlay.linking
            PropertyChanges {
            }
        },
        State {
            name: "autosplit"
            PropertyChanges {
                target: toolStatePanel
                visible: true
            }
        },
        State {
            name: "split"
            PropertyChanges {
                target: toolStatePanel
                visible: true
            }
            PropertyChanges {
                target: sliceEdit
                border.color: "orange";
            }
        },
        State {
            name: "splitMaskEdit"
            PropertyChanges {
            }
        },
        State {
            name: "maskEdit"
            PropertyChanges {
            }
        }
    ]
}
