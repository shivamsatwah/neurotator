/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle {
    id: toolStatePanel
    border.width: 1
    color: Material.color(Material.Grey, Material.Shade800)
    border.color: Material.color(Material.Grey, Material.Shade700)
    height: 50
}
