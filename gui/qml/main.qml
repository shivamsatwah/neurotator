/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import QtQml 2.11
import com.bu 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    Material.theme: Material.Dark
    Material.accent: Material.Orange

    width: 1920
    height: 1080
    title: appdata.windowTitle

    // using a few vars as much as possible for pixel dimensions/spacing
    property real spaceunit: 4
    property real barsizeunit: 32
    property real barunit2: 38
    property real titlebarunit: 24
    property real smallButtonSize: 24
    property real labelSize: 8
    property real tableCellWidth: 125
    property real tableColWidth: 125
    property real tableCellHeight: 24
    property real tableCellFontSize: 8
    property int defaultRoiTableX: 10
    property int defaultRoiTableY: 220
    property int oldX: defaultRoiTableX
    property int oldY: defaultRoiTableY
    property int highestZ: 0
    property int toolTipDelay: 500
    property int toolTipTimeout: 5000
    property int toolTipFontSize: 8
    property int nextStackX: 200
    property int nextStackY: 10

    // common colors
    property color lightOrangeColor: "#FFCC80"

    // common icons
    property string stackIcon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_filter_none_white_18dp.png"
    property string refStackIcon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_collections_bookmark_white_18dp.png"
    property string linkIcon: "qrc:/third-party/material-design-icons-master/editor/1x_web/ic_insert_link_white_18dp.png"
    property string confirmedIcon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_check_white_18dp.png"
    property string flagIcon: "qrc:/third-party/material-design-icons-master/content/1x_web/ic_flag_white_18dp.png"
    property string filterIcon: "qrc:/third-party/material-design-icons-master/content/1x_web/ic_filter_list_white_18dp.png"
    property string searchIcon: "qrc:/third-party/material-design-icons-master/action/ios/ic_search_white_18pt.imageset/ic_search_white_18pt.png"

    //property var lastFocusObj
    property var stackComponent: null
    property var roiObject: null
    property var roiComponent: null
    property bool secondScreenOn: false
    property bool thirdScreenOn: false
    property bool fourthScreenOn: false
    property var menuMap: null

    // global app state variables
    //property bool refIsSet: false
    property bool linkOperation: false

    ListModel {
        id: recentProjectsModel
    }

    //onActiveFocusItemChanged: console.log("activeFocusItem", activeFocusItem)

    function handleDirty(flag) {
        if (flag) {
            saveButton.text = "Save ROIs*"
        } else {
            saveButton.text = "Save ROIs"
        }
        saveButton.font.bold = flag;
    }

    Component.onCompleted: {
        appdata.onDirtyChanged.connect(handleDirty);
        appdata.removedSession.connect(removeMenuItem);
        refreshRecentsMenu();
    }

    Rectangle {
        id: window1Content
        anchors.fill: parent
        color: "transparent"
    }

    Popup {
        id: projectPopup
        modal: false
        visible: false
        ColumnLayout {
            Row {
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true

                SmallButton {
                    id: projectMenuButton
                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_menu_white_18dp.png"
                    onButtonClicked: {
                        projectPopupMenu.popup();
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Project Menu")
                    }
                    Menu {
                        id: projectPopupMenu
                        MenuItem {
                            text: "Open Project..."
                            onTriggered: {
                                projectFileDialog.setFolder(appdata.getRecentProjectDir());
                                projectFileDialog.open();
                            }
                        }
                        MenuItem {
                            text: "Save Project As..."
                            onTriggered: {
                                projectSaveFileDialog.setFolder(appdata.getRecentProjectDir());
                                projectSaveFileDialog.open();
                            }
                        }
                        Menu {
                            id: recentsMenu
                            title: "Recent Projects"

                            Instantiator {
                                model: recentProjectsModel
                                MenuItem {
                                    text: model.name

                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: model.path
                                    }
                                    onTriggered: {
                                        if (appdata.dirty) {
                                            appdata.saveAllRoiFiles();
                                        }

                                        if (roiObject) {
                                            roiObject.destroy();
                                        }

                                        // change the project to the selected one
                                        // appdata will send a signal to close each slicewindow
                                        recreateRoiTable();

                                        appdata.loadProject(model.path);
                                        roiObject.updateModels();
                                        roiObject.updateRef();
                                        roiObject.isLoading = false;
                                        roiObject.visible = true;
                                        saveButton.visible = true;

                                        recreateSliceWindows();

                                        refreshRecentsMenu();
                                        recentsMenu.dismiss();
                                    }
                                }
                                onObjectAdded: recentsMenu.insertItem(index, object)
                                onObjectRemoved: recentsMenu.removeItem(object)
                            }
                            MenuSeparator { }
                            MenuItem {
                                text: "Clear Recents"
                                onTriggered: {
                                    appdata.clearRecentProjects();
                                    refreshRecentsMenu();
                                }
                            }
                        }
                        MenuSeparator { }
                        MenuItem {
                            text: "ROI Table"
                            onTriggered: {
                                if (!roiObject) {
                                    recreateRoiTable();
                                } else {
                                    roiObject.x = defaultRoiTableX;
                                    roiObject.y = defaultRoiTableY;
                                    roiObject.z = ++highestZ;
                                    roiObject.visible = true;
                                }
                                showRoiTableButton.visible = false;
                            }
                        }
                    }
                }
                Rectangle {
                    //spacer
                    width: smallButtonSize
                    height: smallButtonSize
                    color: "transparent"
                }
                SmallButton {
                    id: exitFullScreenButton
                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_fullscreen_exit_white_18dp.png"
                    greyedOut: true
                    onButtonClicked: {
                        appWindow.visibility = Window.Maximized;
                        exitFullScreenButton.greyedOut = true;
                        fullScreenButton.greyedOut = false;
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Exit Fullscreen")
                    }
                }
                SmallButton {
                    id: fullScreenButton

                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_fullscreen_white_18dp.png"
                    onButtonClicked: {
                        appWindow.visibility = Window.FullScreen;
                        exitFullScreenButton.greyedOut = false;
                        fullScreenButton.greyedOut = true;
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Fullscreen (F11)")
                    }
                    Shortcut {
                        sequence: "F11"
                        onActivated: {
                            appWindow.visibility = Window.FullScreen;
                            exitFullScreenButton.greyedOut = false;
                            fullScreenButton.greyedOut = true;
                        }
                    }
                }
                SmallButton {
                    id: secondScreenButton
                    icon: "qrc:/third-party/material-design-icons-master/hardware/1x_web/ic_desktop_windows_white_18dp.png"
                    greyedOut: false
                    onButtonClicked: {
                        var screens = Qt.application.screens;
                        console.log("screens.length:", screens.length);
                        for (var i = 0; i < screens.length; ++i) {
                            console.log("screen " + screens[i].name + " has geometry " +
                                        screens[i].virtualX + ", " + screens[i].virtualY + " " +
                                        screens[i].width + "x" + screens[i].height);
                        }
                        if (screens.length > 1) {
                            if (!secondScreenOn) {
                                secondWindow.screen = Qt.application.screens[1];
                                secondWindow.visibility = Window.FullScreen;
                                secondScreenOn = true;                              
                            } else {
                                secondWindow.visible = false;
                                secondScreenOn = false;
                            }
                        }
                        if (screens.length > 2) {
                            if (!thirdScreenOn) {
                                thirdWindow.screen = Qt.application.screens[2];
                                thirdWindow.visibility = Window.FullScreen;
                                thirdScreenOn = true;
                            } else {
                                thirdWindow.visible = false;
                                thirdScreenOn = false;
                            }
                        }
                        if (screens.length > 3) {
                            if (!fourthScreenOn) {
                                fourthWindow.screen = Qt.application.screens[3];
                                fourthWindow.visibility = Window.FullScreen;
                                fourthScreenOn = true;
                            } else {
                                fourthWindow.visible = false;
                                fourthScreenOn = false;
                            }
                        }
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Extend to all screens (F12)")
                    }
                    Shortcut {
                        sequence: "F12"
                        onActivated: secondScreenButton.buttonClicked(0, 0);
                    }
                }
                SmallButton {
                    id: quitButton
                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_close_white_18dp.png"
                    onButtonClicked: {
                        quitDialog.open();
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Exit application")
                    }

                    MessageDialog {
                        id: quitDialog
                        title: "Exit Application"
                        text: "Are you sure you want to quit?"
                        icon: StandardIcon.Question
                        visible: false
                        standardButtons: StandardButton.Yes | StandardButton.No
                        onYes: {
                            console.log("User exited application.")
                            Qt.quit()
                        }
                        Component.onCompleted: visible = false
                    }
                }
            }

            HorDivider {
                id: divider
                Layout.fillWidth: true
            }

            SmallButton {
                id: projectNameLabel
                customWidth: 140
                text: appdata.projectName
                textLeftAligned: true
                onButtonClicked: {
                    projectNameLabel.visible = false;
                }
                ToolTip {
                    visible: parent.hovered
                    delay: toolTipDelay
                    timeout: toolTipTimeout
                    font.pointSize: toolTipFontSize
                    text: appdata.projectName
                }
            }
            Item {
                id: projectNameInput
                visible: !projectNameLabel.visible
                Layout.preferredWidth: 144
                height: smallButtonSize

                Rectangle {
                    anchors.fill: parent
                    color: Material.color(Material.Grey, Material.Shade900)
                    border.color: Material.color(Material.Grey, Material.Shade700)
                }

                TextInput {
                    text: appdata.projectName
                    anchors.fill: parent
                    anchors.margins: 2
                    color: "white"
                    font.pointSize: 10
                    clip: true
                    selectByMouse: true
                    onEditingFinished: {
                        projectNameLabel.visible = true;
                        appdata.projectName = text;
                        refreshRecentsMenu();
                    }
                }
            }
            anchors.fill: parent
            Button {
                text: qsTr("Import Stack")
                onClicked: {
                    importSession();
                }
                Shortcut {
                    sequence: "F2"
                    onActivated: importSession();
                }
            }
            Button {
                text: qsTr("Import ROIs")
                onClicked: {
                    roiFileDialog.setFolder(appdata.getRecentImportDir());
                    roiFileDialog.setNameFilters(["ROI files (*.mat *.csv)", "All files (*)"]);
                    roiFileDialog.open();
                }
                Shortcut {
                    sequence: "F4"
                    onActivated: {
                        roiFileDialog.setFolder(appdata.getRecentImportDir());
                        roiFileDialog.setNameFilters(["ROI files (*.mat *.csv)", "All files (*)"]);
                        roiFileDialog.open();
                    }
                }
            }
            Button {
                id: saveButton
                text: qsTr("Save ROIs")
                visible: false
                onClicked: {
                    appdata.saveAllRoiFiles();
                    console.log("Done saving!");
                }

                Shortcut {
                    sequence: "Ctrl+S"
                    onActivated: buttonClicked();
                }
            }
            Button {
                id: showRoiTableButton
                text: qsTr("Show ROI Table")
                visible: false

                onClicked: {
                    if (!roiObject) {
                        recreateRoiTable();
                    } else {
                        roiObject.x = defaultRoiTableX;
                        roiObject.y = defaultRoiTableY;
                        roiObject.z = ++highestZ;
                        roiObject.visible = true;
                    }
                    showRoiTableButton.visible = false;
                }
            }
        }
        closePolicy: Popup.NoAutoClose
    }

    FileDialog {
        id: stackFileDialog
        title: "Import Stack"
        nameFilters: [ "Image stack files (*.tiff *.tif)", "All files (*)" ]
        onAccepted: {
            appdata.loadStack(stackFileDialog.fileUrl);
            createStackWindow(stackFileDialog.fileUrl);
            appdata.setRecentImportDir(stackFileDialog.folder)
        }
        onRejected: {
            console.log("Stack file selection canceled")
        }
    }

    FileDialog {
        id: roiFileDialog
        title: "Import ROI File"
        nameFilters: [ "Matlab files (*.mat)", "CSV files (*.csv)", "All files (*)" ]
        onAccepted: {
            if (roiObject) {
                //roiObject.visible = false; // don't allow UI during backend updating
                roiObject.isLoading = true;
            }
            appdata.loadRoiData(roiFileDialog.fileUrl);
            if (!roiObject) {
                recreateRoiTable();
            }
            roiObject.updateModels();
            roiObject.updateRef();
            roiObject.isLoading = false;
            roiObject.visible = true;
            saveButton.visible = true;
            appdata.setRecentImportDir(roiFileDialog.folder)
        }
        onRejected: {
            console.log("ROI file selection canceled")
        }
    }

    FileDialog {
        id: saveFileDialog
        title: "Save ROI File As"
        selectExisting: false
        nameFilters: [ "Matlab files (*.mat)", "All files (*)" ]
        onAccepted: {
            console.log("saveFileDialog")
        }
        onRejected: {
            console.log("ROI file selection canceled")
        }
    }

    FileDialog {
        id: projectFileDialog
        title: "Open Project"
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            if (appdata.dirty) {
                appdata.saveAllRoiFiles();
            }

            if (roiObject) {
                roiObject.destroy();
            }

            recreateRoiTable();

            appdata.loadProject(projectFileDialog.fileUrl);
            roiObject.updateModels();
            roiObject.updateRef();
            roiObject.isLoading = false;
            roiObject.visible = true;
            saveButton.visible = true;

            recreateSliceWindows();

            refreshRecentsMenu();
            recentsMenu.dismiss();

            appdata.setRecentProjectDir(projectFileDialog.folder);
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    FileDialog {
        id: projectSaveFileDialog
        title: "Save Project As"
        selectExisting: false
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            appdata.saveProjectAs(projectSaveFileDialog.fileUrl);
            refreshRecentsMenu();
            appdata.setRecentProjectDir(projectSaveFileDialog.folder);
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    function importSession() {
        stackFileDialog.setFolder(appdata.getRecentImportDir());
        stackFileDialog.open();
    }

    function removeMenuItem(name) {
        console.log("removeMenuItem:", name);
        projectPopupMenu.removeItem(menuMap[name]);
    }

    function handleRoiDestroy(recreate) {
        console.log("handleRoiDestroy");
        oldX = roiObject.x;
        oldY = roiObject.y;
        roiObject.destroy();
        if (recreate) {
            recreateRoiTable();
            roiObject.visible = true;
        }
    }

    function handleMinimizeRoi() {
        showRoiTableButton.visible = true;
    }

    function recreateRoiTable() {
        console.log("[Re]creating ROI object");
        if (roiObject) {
            oldX = roiObject.x;
            oldY = roiObject.y;
            roiObject.destroy();
        }
        //if (!roiComponent) {
        roiComponent = Qt.createComponent("RoiTable.qml", appWindow);
        console.log("ROI component created.");
        //}
        if (roiComponent.status === Component.Ready) {
            roiObject = roiComponent.createObject(appWindow, {"x": oldX, "y": oldY});

            // connecting of signals:
            roiObject.minimizing.connect(handleMinimizeRoi);

            roiObject.isLoading = false;           
            console.log("ROI object constructed");
        }
    }

    function createStackWindow(stackUrl) {
        console.log("Creating SliceWindow object:", stackUrl);

        if (!stackComponent) {
            console.log("making stack component");
            stackComponent = Qt.createComponent("SliceWindow.qml");
        }
        if (stackComponent.status === Component.Ready) {
            var stackObject = stackComponent.createObject(appWindow, {"x": 200, "y": 10});
            stackObject.screen = 1;
            // connecting of signals:
            stackObject.moveToScreen.connect(moveToOtherScreen);

            // add to main context menu
            var qmlStr = 'import QtQuick.Controls 2.4; MenuItem {
                              text: "' + stackObject.sessionName + '";
                              onTriggered: { reshowStackWindow("'+ appdata.getCurrentStackWindow().toString() +'"); }
                          }'
            var newMenuItem = Qt.createQmlObject(qmlStr, appWindow, "newMenuItem");
            projectPopupMenu.addItem(newMenuItem);
            if (!menuMap) {
                menuMap = {};
            }
            menuMap[stackObject.sessionName] = newMenuItem;
        }
    }

    function reshowStackWindow(stackId) {
        appdata.setCurrentStackWindow(stackId);

        if (!stackComponent) {
            console.log("making stack component");
            stackComponent = Qt.createComponent("SliceWindow.qml");
        }
        if (stackComponent.status === Component.Ready) {
            // the initial location doesn't matter, it will move itself to where the user last had it
            var stackObject = stackComponent.createObject(appWindow, {"x": 200, "y": 10});
            stackObject.screen = 1;
            // connecting of signals:
            stackObject.moveToScreen.connect(moveToOtherScreen);
        }
    }

    function recreateSliceWindows()
    {
        // recreate the slicewindows
        var stackUrls = appdata.getStackWindowUrls();
        ///@todo get coordinates
        for (var i = 0; i < stackUrls.length; ++i) {
            appdata.setCurrentStackWindow(i);
            createStackWindow(stackUrls[i]);
        }
    }

    function refreshRecentsMenu() {
        recentProjectsModel.clear();
        var recents = appdata.getRecentProjects();
        var recentNames = appdata.getRecentProjectNames();
        var end = recents.length;
        if (recents.length > 15) {
            end = 15;
        }

        for (var i = 0; i < end; ++i) {
            recentProjectsModel.append({"path": recents[i], "name": recentNames[i].toString()});
        }
    }

    function moveToOtherScreen(obj) {
        if (obj.screen === 1) {
            obj.parent = window2Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 2;
        } else if (obj.screen === 2 && Qt.application.screens.length > 2) {
            obj.parent = window3Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 3;
        } else if (obj.screen === 3 && Qt.application.screens.length > 3) {
            obj.parent = window4Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 4;
        } else {
            obj.parent = window1Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 1;
        }
    }

    property var secondWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        //property var seObject: null
        Rectangle {
            id: window2Content
            anchors.fill: parent
            color: "transparent"
        }
    }
    property var thirdWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        Rectangle {
            id: window3Content
            anchors.fill: parent
            color: "transparent"
        }
    }
    property var fourthWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        Rectangle {
            id: window4Content
            anchors.fill: parent
            color: "transparent"
        }
    }

    Shortcut {
        sequence: "F1"
        onActivated: {
            helpDialog.open();
        }
    }
    MessageDialog {
        id: helpDialog
        title: "Keyboard Shortcuts"
        text: appdata.getSettingString("shortcuts")
        icon: StandardIcon.Information
        visible: false
        standardButtons: StandardButton.Close
        onRejected: {
            close();
        }
        Component.onCompleted: visible = false
    }

//    MessageDialog {
//        id: saveCurrentProjectDialog
//        title: "Unsaved Changes"
//        text: "Save current project first?"
//        icon: StandardIcon.Question
//        visible: false
//        standardButtons: StandardButton.Yes | StandardButton.No
//        onYes: {
//            console.log("User wants to save current project.")
//            appdata.saveAllRoiFiles();
//        }
//        Component.onCompleted: visible = false
//    }

    ProjectWizard {
        id:  projectWizard;
        onNewProject: {
            projectWizard.visible = false;
            projectPopup.visible = true;

            appdata.projectName = projectName;
            appdata.saveProjectAs(projectUrl);
            refreshRecentsMenu();
        }
        onOpenProject: {
            projectWizard.visible = false;
            projectPopup.visible = true;

            recreateRoiTable();

            appdata.loadProject(projectUrl);
            roiObject.updateModels();
            roiObject.updateRef();
            roiObject.isLoading = false;
            roiObject.visible = true;
            saveButton.visible = true;

            recreateSliceWindows();

            refreshRecentsMenu();
        }
    }
}

