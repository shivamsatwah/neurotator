/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICEIMAGE_HPP
#define SLICEIMAGE_HPP

#include <QQuickItem>
#include <QImage> // for testing

#include "Project.hpp"
#include "RoiData.hpp"
#include "HyperstackTiff.hpp"

/// A widget to use in QML for showing slices of hyperstacks
class SliceImage : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int zoomFactor READ getZoomFactor WRITE setZoomFactor NOTIFY zoomFactorChanged)
    Q_PROPERTY(int minWidth READ getMinWidth)
    Q_PROPERTY(int minHeight READ getMinHeight)
    Q_PROPERTY(bool isRef READ isRef NOTIFY isRefChanged)
    Q_PROPERTY(bool histEq READ getHistEq WRITE setHistEq)

public:
    SliceImage();
    virtual ~SliceImage() override;

    Q_INVOKABLE QString getName() const { return m_name; }
    Q_INVOKABLE int getNumChannels() const { return m_numChannels; }
    Q_INVOKABLE int getMaxX() const { return (m_stack) ? m_stack->size().x : 0; }
    Q_INVOKABLE int getMaxY() const { return (m_stack) ? m_stack->size().y : 0; }
    Q_INVOKABLE int getMaxZ() const { return (m_stack) ? m_stack->size().z : 0; }
    Q_INVOKABLE int getMaxT() const { return (m_stack) ? m_stack->size().t : 0; }
    Q_INVOKABLE int getIndexZ() const { return (m_stack) ? m_stack->index().z : 0; }
    Q_INVOKABLE int getIndexTime() const { return (m_stack) ? m_stack->index().time : 0; }
    Q_INVOKABLE QColor getMaxColorValue() const { return m_maxColorValue; }
    Q_INVOKABLE double getMaxColorValueF() const { return m_maxColorValue.redF(); }
    Q_INVOKABLE QString getIndexStr() const;
    Q_INVOKABLE bool getSynced() const { return m_synced; }
    Q_INVOKABLE bool atMaxZ() const { return (m_stack) ? ((m_stack->index().z + 1) >= m_stack->size().z) : false; }

    Q_INVOKABLE void setScaledWidth(const int w);
    Q_INVOKABLE void setScaledHeight(const int h);
    Q_INVOKABLE void adjustOffset(const int x, const int y);
    Q_INVOKABLE void setOffsets(const float x, const float y);
    Q_INVOKABLE void resetZoom();
    Q_INVOKABLE void loadWindowConfig();

    int getZoomFactor() { return m_zoomFactor; }
    void setZoomFactor(const int zoomFactor);
    int getMinWidth() { return 460; }
    int getMinHeight() { return 420; }
    bool isRef();
    bool getHistEq() { return m_histEqualization; }
    void setHistEq(bool flag) { m_histEqualization = flag; }

protected:
    virtual QSGNode* updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData) override;
    void updateZoomThresh() { m_zoomThresh = static_cast<int>(width() / static_cast<float>(getMaxX()) * 100.0f); }

signals:
    void navChanged();
    void zoomFactorChanged(int zoomFactor);
    void scaleChanged(float zoomFactor, float xOffset, float yOffset);
    void isRefChanged();
    void changeStackWindowPosition(int x, int y);

public slots:
    void nextTime() {}
    void nextZ();
    void prevTime() {}
    void prevZ();
    void gotoZ(const int z);
    void gotoTime(const int t);
    void roiSelectionRequest(QString sessionKey, QString roiKey);
    void setSynced(bool flag) { m_synced = flag; }
    void checkIsRef() { emit isRefChanged(); }
    void windowPositionChanged(const int x, const int y);
    //void windowHidden();

private:
    std::shared_ptr<Project> m_project;
    QString m_name = "empty stack";
    stack_index_t m_stackWindowIndex = 0;
    QColor m_maxColorValue = QColor(qRgba(0xFF, 0xFF, 0xFF, 0xFF));
    int m_numChannels = 0;
    HyperstackTiff* m_stack = nullptr;
    float m_scaling = 1.0;
    int m_zoomFactor = 100;
    int m_zoomThresh = 100;
    float m_scaledWidth = 0;
    float m_scaledHeight = 0;
    float m_scaledInvWidth = 0;
    float m_scaledInvHeight = 0;
    float m_offsetX = 0;
    float m_offsetY = 0;
    float m_textureOffsetX = 0;
    float m_textureOffsetY = 0;
    //int m_textureWidth = 0;
    //int m_textureHeight = 0;
    bool m_offsetChanged = false;
    uchar m_iBase;
    bool m_synced = false;
    bool m_histEqualization = false;
    bool m_loadingConfig = false;
    //QHash<QString, RoiData::roi_t> m_visibleRois;
};

#endif // SLICEIMAGE_HPP
